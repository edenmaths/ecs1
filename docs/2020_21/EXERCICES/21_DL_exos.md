
{{ chapitre(21, "Développements limités : exercices", 10)}}


## Comparaisons de fonctions


!!! {{exercice()}}

	Donner un équivalent simple des fonctions ci-dessous au voisinage de $x_0$.
	
	1. $f(x)=\sin(x^2)\cos(x^3)\ln(1+2x)\quad x_0=0$
	1. $f(x) = \left({\rm e}^x - \cos\frac1x\right)\quad x_0=+\infty$
	1. $f(x) = \ln(x^2+{\rm e}^{x^2})\quad x_0=+\infty$
	1. $f(x) = \ln(x) + \cos(x)\quad x_0=+\infty$
	1. $f(x)= x^2\sin\left(\frac1{\sqrt{x}}\right)\quad x_0=+\infty$





!!! {{ exercice()}}
	
	Calculez 

	1. $ \displaystyle\lim_{x\to 0}\dfrac{\tan(x)\ln(1+x)}{1-\cos(x)} $
	2. $ \displaystyle\lim_{x\to 0}\dfrac{\left(\sqrt[3]{1+x}-1\right)\sin(x)}{\ln(1-x^2)}$


!!! {{ exercice()}}

    === "Énoncé"

        Déterminer  un équivalent  en  $a$ et  la limite  en  $a$ des  fonctions
        suivantes (les ordres demandés peuvent être rabaissés pour les moins aventuriers).
		
		1. $\sqrt{1-x^2}$ en $a=1$
		2. $x^n-1$ en $a=1$
		3. $\ln(\cos(x))$ en $a=0$.
		4. $\ln(\sin(x))$ en $a=0$.
		5. $\sin(πx)$ en $a=1$.
		6. $ \dfrac{(1-{\rm e}^x)(1-\cos(x^2))}{x^5}$ en $a=0$.
		7. $(x^2+x-2)\tan \left(\dfrac{\pi x}{2} \right)$ en $a=2$.
		8. $(\ln(x))^2\left(\sin\left(\dfrac{1}{\ln(x)}\right)-\sin\left(\dfrac{1}{\ln(x+1)} \right)\right)$ en $a=+\infty$.

	=== "Indications"

         1. 0
		 2. 0
		 3. $\cos(x) = 1 + (\cos(x) - 1)$
		 4. Attention... On pourra écrire $\sin x = \dfrac{\sin x}{x} \times x$.
		 5. $\pi x = \pi  + \pi (x-1)$.
		 6. $-1/2$
		 7. $\tan(x + \pi /2) =...$ On trouve comme équivalent $-6/\pi $.
		 8. $\sin p - \sin q = {\rm Im}( {\rm e}^{i p } - {\rm e}^{ i q} )$
			On trouve $1/x$.

!!! {{exercice()}}

	Soit  $x_0$ un  réel et  $f$  une fonction  définie sur  un voisinage  $\cal
	V_{x_0}$ de $x_0$. 
	
	On suppose que $ \displaystyle\lim_{x\to x_0}f(x)=1$.
	
	1. Démontrer que $\ln\left(f(x)\right)  \underset{x \to x_0}{\sim} f(x)-1$ 
	1.  Déterminer  un  équivalent   de  $\ln\left(\dfrac{1+x}{1-x}  \right)$  au
	   voisinage de 0.
	   
	   
!!! {{exercice() }}

	Déterminez  une  condition   suffisante  sur  deux  fonctions   $f$  et  $g$
	équivalentes au voisinage  d'un point $x_0$ pour pouvoir  composer par $\ln$
	et conserver cependant l'équivalence.
	





## DL


!!! {{ exercice("Interro de cours")}}

    === "Énoncé"

        1. Déterminer (et justifier) si les énoncés suivants sont vrais ou faux :
		    1. Si $u_{n} \sim v_{n}$, $u_{n} + 1 \sim v_{n} + 1$.
		    1. Si $f(x) = \underset{x \rightarrow a}{o}(g(x))$, $\dfrac{f}{g}$ est bornée.
		    1. Si $f$ est dérivable en $a$, et si $f'(a) \neq 0$, $f(x) - f(a) \underset{x \rightarrow a}{\sim} f'(a)(x-a)$.
		    1. Si $f(x) \underset{x \rightarrow a}{\sim} g(x)$, $\operatorname{e}^{f(x)} \underset{x \rightarrow a}{\sim} \operatorname{e}^{g(x)}$.
		
		1.  Déterminer la limite de $\dfrac{(\sqrt[3]{1+x}-1)\sin{x}}{\ln(1-x^{2})}$ quand $x \rightarrow 0$.
		1. Déterminer le développement limité en $0$ de $\operatorname{e}^{x} \ln(1+x)$ à l'ordre $3$.
		1. Déterminer le développement limité de $\sqrt{\cos{x}}$ à l'ordre $4$ en $0$.
		1. Déterminer le développement limité de $\operatorname{th} = \dfrac{\operatorname{sh}}{\operatorname{ch}}$ à l'ordre $3$ en $0$.
		1. Déterminer si les énoncés suivants sont vrais ou faux à partir de votre cours :
	 
		    1. Une fonction dérivable en $a$ admet un développement limité à l'ordre $1$ en $a$.
		    1. Une fonction deux fois dérivable admet un développement limité à l'ordre $2$ en $a$.
		    1. Une fonction $\mathscr{C}^{2}$ admet un développement limité à l'ordre $2$ en $a$.
		    1. Une fonction qui admet un développement limité à l'ordre $2$ en $a$ est deux fois dérivable en $a$.

		1. Donner un équivalent en $0$ de $\cos{x} + \operatorname{ch}{x} - 2$.

	=== "Indications"

         1. F F V F
		 2. $-\dfrac{1}{3}$
		 3. $x + \dfrac{x^{2}}{2} + \dfrac{x^{3}}{3} + {\Large\scr o}(x^{3})$
		 4. $ 1 - \dfrac{x^{2}}{4} - \dfrac{x^{4}}{96} {\Large\scr o}(x^{4})$
		 5. $x - \dfrac{x^{3}}{3} + {\Large\scr o}(x^{3})$
		 6. V V V F
		 7. $\cos{x} + \operatorname{ch}{x} - 2 \underset{x \rightarrow 0}{\sim} \dfrac{x^{4}}{24}$



!!! {{ exercice()}}

    === "Énoncé"

        Donner le développement limité de $f$ à l'ordre $n$ en $x_{0}$ :
		
		$
		\begin{array}{ll}
		\text{ 1. } f : x \mapsto \cos{x} \ln(1+x), n = 4 \text{ et } x_{0} = 0 & \qquad \text{ 2. } f : x \mapsto \dfrac{\operatorname{e}^{x} - \sqrt{1+2x}}{x^{2}} , n = 3 \text{ et } x_{0} = 0 \\
		\text{ 3. } f : x \mapsto \operatorname{e}^{\sin{x}} , n = 4 \text{ et } x_{0} = \dfrac{\pi}{2} & \qquad \text{ 4. } f : x \mapsto  \ln \left ( \dfrac{\sin{x}}{x} \right ) , n = 4 \text{ et } x_{0} = 0 \\
		\text{ 5. } f : x \mapsto \sqrt{\sin{x}} , n = 3 \text{ et } x_{0} = \dfrac{\pi}{2} & \qquad \text{ 6. } f : x \mapsto  \left ( 1 + \dfrac{1}{x} \right )^{x} , n = 3 \text{ et } x_{0} = +\infty \\
		\text{ 7. } f : x \mapsto \ln(2\sin{x}) , n = 3 \text{ et } x_{0} = \dfrac{\pi}{6} & \qquad \text{ 8. } f : x \mapsto \dfrac{1}{x} \sqrt[3]{x^{3}+x^{2}} , n = 3 \text{ et } x_{0} = +\infty \\
		\text{ 9. } f : x \mapsto \dfrac{\sqrt[3]{1-x^{2}}}{\cos{x}} , n = 4 \text{ et } x_{0} = 0 & \qquad \text{ 10. } f: x \mapsto \dfrac{\ln(\operatorname{ch}{x})}{\cos{x}} , n = 5 \text{ et } x_{0} = 0
		\end{array}
		$

	=== "Indications"

	    1. On a
		
		   $
		   \begin{align*}
		   f(x) & \underset{x \to 0}{=} \left (1 - \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24}  + {\Large \scr o}(x^{4}) \right ) \left (x - \dfrac{x^{2}}{2} + \dfrac{x^{3}}{3} - \dfrac{x^{4}}{4}  + {\Large \scr o}(x^{4}) \right ) \\
		   & \underset{x \to 0}{=} x - \dfrac{x^{2}}{2} + \dfrac{x^{3}}{3} - \dfrac{x^{4}}{4} - \dfrac{x^{3}}{2} + \dfrac{x^{4}}{4} + {\Large \scr o}(x^{4}) \underset{x \to 0}{=} x - \dfrac{1}{2} x^{2} - \dfrac{1}{6} x^{3} + {\Large \scr o}(x^{4})
		   \end{align*}
		   $
	    2. Attention le dénominateur réduit de $2$ l'ordre du DL du
		   numérateur, il faut donc faire un DL du numérateur à l'ordre $5$ :
		   
		   $
		   \begin{align*}
		   f(x) & \underset{x \to 0}{=} \dfrac{1}{x^{2}} \left (1 + x + \dfrac{x^{2}}{2} + \dfrac{x^{3}}{6} + \dfrac{x^{4}}{24} + \dfrac{x^{5}}{120} \right . \\
		   & \hskip 9mm \left . - 1 - \dfrac{2x}{2} + \dfrac{(2x)^{2}}{8} - \dfrac{(2x)^{3}}{16} + \dfrac{5 (2x)^{4}}{128} - \dfrac{7 (2x)^{5}}{256} + {\Large \scr o}(x^{5}) \right ) \\
		   & \underset{x \to 0}{=} \dfrac{1}{x^{2}} \left (\dfrac{x^{2}}{2} + \dfrac{x^{3}}{6} + \dfrac{x^{4}}{24} + \dfrac{x^{5}}{120} + \dfrac{x^{2}}{2} - \dfrac{x^{3}}{2} + \dfrac{5 x^{4}}{8} - \dfrac{7 x^{5}}{8} + {\Large \scr o}(x^{5}) \right ) \\
		   & \underset{x \to 0}{=} 1 - \dfrac{1}{3} x + \dfrac{2}{3} x^{2} - \dfrac{13}{15} x^{3} + {\Large \scr o}(x^{3})
		   \end{align*}
		   $
		   
		   *Avant  de   calculer  un  DL,   bien  anticiper  s'il  y   aura  une
		   multiplication  par $x$  (ou  $x^{2}$, $x^{3}$,  etc) qui  augmentera
		   l'ordre  du DL  final,  ou  au contraire  une  division  par $x$  (ou
		   $x^{2}$, $x^{3}$, etc) qui diminuera l'ordre du DL final.* 
		
		3. On pose $u = x - \dfrac{\pi}{2}$ de sorte que
		  
		  $
		  \begin{align*}
		   f(x) & = \exp \left (\sin \left (u + \dfrac{\pi}{2} \right ) \right ) = \exp(\cos{u}) = \exp  \left ( 1 - \dfrac{u^{2}}{2} + \dfrac{u^{4}}{24} + \underset{u \rightarrow 0}{\Large\scr o}(u^{4}) \right ) \\
		   &= \operatorname{e} \exp \left (- \dfrac{u^{2}}{2} + \dfrac{u^{4}}{24} + \underset{u \rightarrow 0}{\Large\scr o}(u^{4}) \right ) = \operatorname{e} \left ( 1 + h + \dfrac{h^{2}}{2} + \dfrac{h^{3}}{6} + \dfrac{h^{4}}{24} + \underset{h \rightarrow 0}{\Large\scr o}(h^{4}) \right ) \text{ où } h = -\dfrac{u^{2}}{2} + \dfrac{u^{4}}{24} \\
		   &= \operatorname{e} \left ( 1 + \left (-\dfrac{u^{2}}{2} + \dfrac{u^{4}}{24} \right ) + \dfrac{1}{2} \left (-\dfrac{u^{2}}{2} + \dfrac{u^{4}}{24} \right )^{2} \right ) + \underset{u \rightarrow 0}{\Large\scr o}(u^{4}) = \operatorname{e} \left (1 - \dfrac{u^{2}}{2} + \dfrac{u^{4}}{24} + \dfrac{u^{4}}{8} \right ) + \underset{u \rightarrow 0}{\Large\scr o}(u^{4}) \\
		   &= \operatorname{e} - \dfrac{\operatorname{e} u^{2}}{2} + \dfrac{\operatorname{e} u^{4}}{6} + \underset{u \rightarrow 0}{\Large\scr o}(u^{4}) \underset{x \to 0}{=} \operatorname{e} - \dfrac{\operatorname{e}}{2} \left (x-\dfrac{\pi}{2} \right )^{2} + \dfrac{\operatorname{e}}{6} \left (x-\dfrac{\pi}{2} \right )^{4} + \underset{x \rightarrow \dfrac{\pi}{2}}{\Large\scr o} \left ( \left (x-\dfrac{\pi}{2} \right )^{4} \right )
		   \end{align*}
		   $

		   **Attention !**
		   *Attention   à  ne   pas  poser   $h   =  1   -  \dfrac{u^{2}}{2}   +
		   \dfrac{u^{4}}{24}$ qui tend  vers $1$ quand $u$ tend vers  $0$. On ne
		   pourrait alors pas utiliser le DL d'exponentielle en $0$ !*
		   
		4.  On a  (attention à  bien faire  un DL  de $\sin$  à l'ordre  $5$, le
		   dénominateur réduisant d'un ordre le DL) : 
			
		   $	
		   \begin{align*}
		   f(x) & \underset{x \to 0}{=} \ln \left (\dfrac{1}{x} \left (x - \dfrac{x^{3}}{6} + \dfrac{x^{5}}{120} + {\Large \scr o}(x^{5}) \right) \right ) \underset{x \to 0}{=} \ln \left (1 - \dfrac{x^{2}}{6} + \dfrac{x^{4}}{120} + {\Large \scr o}(x^{4}) \right ) \\
		   &= h - \dfrac{h^{2}}{2} + \dfrac{h^{3}}{3} - \dfrac{h^{4}}{4} + \underset{h \rightarrow 0}{\Large\scr o}(h^{4}) \text{ où } h = - \dfrac{x^{2}}{6} + \dfrac{x^{4}}{120} \\
		   & \underset{x \to 0}{=} \left (- \dfrac{x^{2}}{6} + \dfrac{x^{4}}{120} \right ) - \dfrac{1}{2} \left (- \dfrac{x^{2}}{6} + \dfrac{x^{4}}{120} \right )^{2} + {\Large \scr o}(x^{4}) \\
		   & \underset{x \to 0}{=} -\dfrac{x^{2}}{6} + \dfrac{x^{4}}{120} - \dfrac{x^{4}}{72} + {\Large \scr o}(x^{4}) \underset{x \to 0}{=} -\dfrac{1}{6} x^{2} - \dfrac{1}{180} x^{4} + {\Large \scr o}(x^{4})
		   \end{align*}
		   $
		
		5. On pose $u = x-\dfrac{\pi}{2}$ et on a :
		   
		   $
		   \begin{align*}
		   f(x) & = \sqrt{\sin \left (u + \dfrac{\pi}{2} \right)} = \sqrt{\cos{u}} = \sqrt{1 - \dfrac{u^{2}}{2} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3})} \\
		   &= 1 + \dfrac{h}{2} - \dfrac{h^{2}}{8} + \dfrac{h^{3}}{16} + \underset{h \rightarrow 0}{\Large\scr o}(h^{3}) \text{ où } h = -\dfrac{u^{2}}{2} \\
		   &= 1 + \dfrac{1}{2} \left (- \dfrac{u^{2}}{2} \right ) + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) = 1 - \dfrac{1}{4} \left (x-\dfrac{\pi}{2} \right )^{2} + \underset{x \rightarrow \dfrac{\pi}{2}}{\Large\scr o} \left ( \left (x-\dfrac{\pi}{2} \right )^{3} \right )
		   \end{align*}
		   $
		
		6. On pose $x = \dfrac{1}{u}$. Comme il va y avoir un $\dfrac{1}{u}$ (cf
		   plus bas)  qui va diminuer  d'un l'ordre du DL,  on effectue le  DL à
		   l'ordre 4 :
		   
		   $
		   \begin{align*}
		   f(x) & \underset{x \to 0}{=} \exp \left (x \ln \left (1 + \dfrac{1}{x} \right ) \right ) = \exp \left (\dfrac{1}{u} \ln(1+u) \right ) = \exp \left (\dfrac{1}{u} \left (u - \dfrac{u^{2}}{2} + \dfrac{u^{3}}{3} - \dfrac{u^{4}}{4} + \underset{u \rightarrow 0}{\Large\scr o}(u^{4}) \right ) \right ) \\
		   & = \exp \left (1 - \dfrac{u}{2} + \dfrac{u^{2}}{3} - \dfrac{u^{3}}{4} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \right ) = \operatorname{e} \operatorname{e}xp \left (- \dfrac{u}{2} + \dfrac{u^{2}}{3} - \dfrac{u^{3}}{4} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \right ) \\
		   &= \operatorname{e} \left ( 1 + h + \dfrac{h^{2}}{2} + \dfrac{h^{3}}{6} + \underset{h \rightarrow 0}{\Large\scr o}(h^{3}) \right ) \text{ où } h = - \dfrac{u}{2} + \dfrac{u^{2}}{3} - \dfrac{u^{3}}{4} \\
		   & = \operatorname{e} \left ( 1 + \left (- \dfrac{u}{2} + \dfrac{u^{2}}{3} - \dfrac{u^{3}}{4} \right ) + \dfrac{1}{2} \left (- \dfrac{u}{2} + \dfrac{u^{2}}{3} - \dfrac{u^{3}}{4} \right )^{2} + \dfrac{1}{6} \left (- \dfrac{u}{2} + \dfrac{u^{2}}{3} - \dfrac{u^{3}}{4} \right )^{3} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \right ) \\
		   & = \operatorname{e} \left ( 1 - \dfrac{u}{2} + \dfrac{u^{2}}{3} - \dfrac{u^{3}}{4} + \dfrac{u^{2}}{8} - \dfrac{u^{3}}{6} - \dfrac{u^{3}}{48} \right ) + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) = \operatorname{e} - \dfrac{\operatorname{e} u}{2} + \dfrac{11\operatorname{e} u^{2}}{24} - \dfrac{7\operatorname{e} u^{3}}{16} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \\
		   & \underset{x \to 0}{=} \operatorname{e} - \dfrac{\operatorname{e}}{2x} + \dfrac{11\operatorname{e}}{24x^{2}} - \dfrac{7\operatorname{e}}{16x^{3}} + \underset{x \rightarrow +\infty}{\Large\scr o} \left (\dfrac{1}{x^{3}} \right )
		   \end{align*}
		   $
		
		7. On pose $u = x - \dfrac{\pi}{6}$ et on a :
		
		   $	
		   \begin{align*}
		   f(x) & \underset{x \to 0}{=} \ln \left (2\sin \left (u + \dfrac{\pi}{6} \right ) \right ) = \ln(\sqrt{3}\sin{u} + \cos{u}) = \ln \left (\sqrt{3} u - \dfrac{\sqrt{3} u^{3}}{6} + 1 - \dfrac{u^{2}}{2} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \right ) \\
		   & = \ln \left (1 + \sqrt{3} u - \dfrac{u^{2}}{2} - \dfrac{\sqrt{3} u^{3}}{6} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \right) \\
		   &= h - \dfrac{h^{2}}{2} + \dfrac{h^{3}}{3} + \underset{h \rightarrow 0}{\Large\scr o}(h^{3}) \text{ où } h = \sqrt{3} u - \dfrac{u^{2}}{2} - \dfrac{\sqrt{3} u^{3}}{6} \\
		   & = \left (\sqrt{3} u - \dfrac{u^{2}}{2} - \dfrac{\sqrt{3} u^{3}}{6} \right ) - \dfrac{1}{2} \left (\sqrt{3} u - \dfrac{u^{2}}{2} - \dfrac{\sqrt{3} u^{3}}{6} \right )^{2} + \dfrac{1}{3} \left (\sqrt{3} u - \dfrac{u^{2}}{2} - \dfrac{\sqrt{3} u^{3}}{6} \right )^{3} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \\
		   & = \sqrt{3} u - \dfrac{u^{2}}{2} - \dfrac{\sqrt{3} u^{3}}{6} - \dfrac{3u^{2}}{2} + \dfrac{\sqrt{3} u^{3}}{2} + \dfrac{3 \sqrt{3} u^{3}}{3} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \\
		   & = \sqrt{3} u -2u^{2} + \dfrac{4\sqrt{3}}{3} u^{3} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \\
		   &= \sqrt{3} \left (x-\dfrac{\pi}{6} \right ) - 2 \left (x-\dfrac{\pi}{6} \right )^{2} + \dfrac{4\sqrt{3}}{3} \left (x-\dfrac{\pi}{6} \right )^{3} + \underset{x \rightarrow \dfrac{\pi}{6}}{\Large\scr o}\left(\left(x-\dfrac{\pi}{6}\right)^{3} \right)
		   \end{align*}
		   $
		
		8. On commence par mettre les $x$ en facteurs et on pose $u = \dfrac{1}{x}$ :
		   
		   $
		   \begin{align*}
		   f(x) & \underset{x \to 0}{=} \sqrt[3]{1 + \dfrac{1}{x}} = \sqrt[3]{1+u} = 1 + \dfrac{u}{3} - \dfrac{u^{2}}{9} + \dfrac{5u^{3}}{81} + \underset{u \rightarrow 0}{\Large\scr o}(u^{3}) \\
		   & = 1 + \dfrac{1}{3x} - \dfrac{1}{9x^{2}} + \dfrac{5}{81x^{3}} + \underset{x \rightarrow +\infty}{\Large\scr o} \left (\dfrac{1}{x^{3}} \right )
		   \end{align*}
		   $
		
		9. On a :
		   
		   $
		   \begin{align*}
		   f(x) &\underset{x \to 0}{=} \dfrac{\sqrt[3]{1-x^{2}}}{1 - \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} + {\Large \scr o}(x^{4})} = \sqrt[3]{1-x^{2}} \left ( 1 - h + h^{2} - h^{3} + h^{4} + \underset{h \rightarrow 0}{\Large\scr o}(h^{3}) \right ) \text{ où } h = - \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} \\
		   & \underset{x \to 0}{=} \sqrt[3]{1-x^{2}} \left (1 - \left (- \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} \right) + \left (- \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} \right )^{2} \right ) + {\Large \scr o}(x^{4}) \\
		   & \underset{x \to 0}{=} \sqrt[3]{1-x^{2}} \left (1 + \dfrac{x^{2}}{2} - \dfrac{x^{4}}{24} + \dfrac{x^{4}}{4} \right ) + {\Large \scr o}(x^{4}) \underset{x \to 0}{=} \left (1 - \dfrac{x^{2}}{3} - \dfrac{x^{4}}{9} \right ) \left (1 + \dfrac{x^{2}}{2} + \dfrac{5x^{4}}{24} \right ) + {\Large \scr o}(x^{4}) \\
		   & \underset{x \to 0}{=} 1 + \dfrac{x^{2}}{2} + \dfrac{5x^{4}}{24} - \dfrac{x^{2}}{3} - \dfrac{x^{4}}{6} - \dfrac{x^{4}}{9} + {\Large \scr o}(x^{4}) \underset{x \to 0}{=} 1 + \dfrac{1}{6} x^{2} - \dfrac{5}{72} x^{4} + {\Large \scr o}(x^{4})
		   \end{align*}
		   $
		
		10. On a déjà 		
		   $
		   \dfrac{1}{\cos{x}}  \underset{x \to 0}{=}  1  +   \dfrac{x^{2}}{2}  +  \dfrac{5x^{4}}{24}  +
		   {\Large \scr o}(x^{5})
		   $	   
		   (c'est une fonction paire et on en a fait le DL à l'ordre $4$ juste au-dessus). De plus 
		   $
		   \begin{align*}
		   \ln(\operatorname{ch}{x}) & \underset{x \to 0}{=} \ln \left (1 + \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} + {\Large \scr o}(x^{5}) \right ) = h - \dfrac{h^{2}}{2} + \dfrac{h^{3}}{3} - \dfrac{h^{4}}{4} + \dfrac{h^{5}}{5} + \underset{h \rightarrow 0}{\Large\scr o}(h^{5}) \text{ où } h = \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} \\
		   & \underset{x \to 0}{=} \left (\dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} \right ) - \dfrac{1}{2} \left (\dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} \right )^{2} + {\Large \scr o}(x^{5}) \underset{x \to 0}{=} \dfrac{x^{2}}{2} + \dfrac{x^{4}}{24} - \dfrac{x^{4}}{8} + {\Large \scr o}(x^{5}) \\
		   &\underset{x \to 0}{=} \dfrac{x^{2}}{2} - \dfrac{x^{4}}{12} + {\Large \scr o}(x^{5})
		   \end{align*}
		   $
		   
		   Il ne reste plus qu'à faire le produit
		   
		   $
		   \begin{align*}
		   f(x) &\underset{x \to 0}{=} \left (1 + \dfrac{x^{2}}{2} + \dfrac{5x^{4}}{24} \right ) \left (\dfrac{x^{2}}{2} - \dfrac{x^{4}}{12} \right ) + {\Large \scr o}(x^{5}) \underset{x \to 0}{=} \dfrac{x^{2}}{2} + \dfrac{x^{4}}{4} - \dfrac{x^{4}}{12} + {\Large \scr o}(x^{5})\\
		   &\underset{x \to 0}{=} \dfrac{1}{2} x^{2} + \dfrac{1}{6} x^{4} + {\Large \scr o}(x^{5}).
		   \end{align*}
		   $




!!! {{exercice() }}

	=== "Énoncé"
	 
         Donner le développement limité en $0$ des fonctions $x\mapsto $:

		 1.  $\cos x \cdot \exp x$  à l'ordre $3$

		 1.  $\left( \ln (1+x) \right)^2$  à l'ordre $4$

		 1.  $\displaystyle{\dfrac{\operatorname{sh} x-x}{x^3}}$  à l'ordre $6$

		 1.  $\exp\big(\sin(x)\big)$  à l'ordre $4$

		 1.   $\sin^6(x)$  à l'ordre $9$

		 1.  $\ln \big(\cos(x)\big)$  à l'ordre $6$

		 1.  $\displaystyle{\dfrac{1}{\cos x}}$  à l'ordre $4$

		 1.  $\tan x$  à l'ordre $5$ (ou $7$ pour les plus courageux)

		 1.  $(1+x)^{\dfrac{1}{1+x}}$  à l'ordre $3$

	=== "Indications"
	
	       1.  $\cos x \cdot \exp x$ (à l'ordre $3$).
			   Le DL de $\cos x$ à l'ordre $3$ est
		   
			   $
			   \cos x = 1 - \dfrac{1}{2!} x^2 + ε_1(x)x^3.
			   $

	           Le DL de $\exp x$ à l'ordre $3$ est
		   
			   $
			   \exp x =1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3.
			   $

		       Par convention  toutes nos fonctions  $ε_i(x)$ vérifierons
		       $ε_i(x)\to 0$ lorsque $x\to0$. 


			   On multiplie ces deux expressions 
			    $\begin{align*}
			     \cos x \times \exp x 
			     & = \Big(1 - \dfrac{1}{2} x^2 + ε_1(x)x^3\Big) \times \Big( 1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3\Big) \\
			    & = 1 \cdot \Big(1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3 \Big) \quad \text{on développe la ligne du dessus}\\ 
			    & \qquad - \dfrac{1}{2} x^2 \cdot \Big( 1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3 \Big) \\
			    & \qquad + ε_1(x)x^3 \cdot \Big(1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3 \Big) \\
			    \end{align*}$

			 On va développer chacun de ces produits, par exemple pour le deuxième produit :

			 $
			 - \dfrac{1}{2!} x^2 \cdot \Big(  1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 +  ε_2(x)x^3\Big)
			 = -  \dfrac{1}{2} x^2  - \dfrac{1}{2}  x^3 -  \dfrac14x^4 -\dfrac1{12}x^5
			 -\dfrac12x^2\cdot ε_2(x)x^3.
			 $

			 Mais on cherche un DL à l'ordre $3$ donc tout terme en $x^4$, $x^5$ ou plus se met dans $ε_3(x)x^3$,
			 y compris $x^2 \cdot ε_2(x)x^3$ qui est un bien de la forme $ε(x)x^3$.
			 Donc $- \dfrac{1}{2} x^2 \cdot \Big(1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3\Big)
			 = - \dfrac{1}{2} x^2 - \dfrac{1}{2} x^3  + ε_3(x)x^3.$

			 Pour le troisième produit on a

			 $
			 ε_1(x)x^3 \cdot \Big(1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3\Big) 
			 = ε_1(x)x^3+xε_1(x)x^3+\cdots = ε_4(x)x^3
			 $

			 On en arrive à :
			 $\begin{align*}
			 \cos x \cdot \exp x 
			   & =  \Big(1 - \dfrac{1}{2} x^2 + ε_1(x)x^3 \Big) \times \Big( 1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 + ε_2(x)x^3\Big) \\
			   & = 1+x+\dfrac1{2!}x^2+\dfrac1{3!}x^3 +  ε_1(x)x^3\\
			   & \qquad - \dfrac{1}{2} x^2- \dfrac{1}{2} x^3  + ε_3(x)x^3 \\
			   & \qquad  + ε_4(x)x^3 \qquad \text{il ne reste plus qu'à regrouper les termes :}  \\    
			   & =  1 + x + (\dfrac12-\dfrac12) x^2 + (\dfrac{1}{6}- \dfrac{1}{2})x^3 + ε_5(x)x^3 \\
			   & =  1 + x - \dfrac13 x^3 + ε_5(x)x^3 \\
			 \end{align*}$

			 Ainsi le DL de $\cos x \cdot \exp x$ en $0$ à l'ordre $3$ est :
			 $\cos x \cdot \exp x = 1 + x - \dfrac13 x^3 + ε_5(x)x^3.$


		   1.  $\left( \ln (1+x) \right)^2$ (à l'ordre $4$).

		       Il s'agit juste de multiplier le DL de $\ln(1+x)$ par lui-même.
		       En fait si l'on réfléchit un peu on s'aperçoit qu'un DL à l'ordre
		       $3$ sera suffisant (car le terme constant est nul) : 
		   
	    	   $
		       \ln(1+x)=x-\dfrac12x^2+\dfrac13x^3+ ε(x)x^3
		       $
		
			   $ε_5(x)\to 0$ lorsque $x\to0$.

			  $\begin{align*}
			  \left( \ln (1+x) \right)^2 
				& = \ln (1+x)  \times \ln (1+x)  \\
				& = \left(x-\dfrac12x^2+\dfrac13x^3+ ε(x)x^3\right) \times \left( x-\dfrac12x^2+\dfrac13x^3+ ε(x)x^3\right) \\
				& = x \times \left( x-\dfrac12x^2+\dfrac13x^3+ ε(x)x^3\right) \\
				& \qquad  -\dfrac12x^2\times \left( x-\dfrac12x^2+\dfrac13x^3+ ε(x)x^3\right) \\
				& \qquad +\dfrac13x^3\times \left( x-\dfrac12x^2+\dfrac13x^3+ ε(x)x^3\right) \\
				& \qquad + ε(x)x^3\times \left( x-\dfrac12x^2+\dfrac13x^3+ ε(x)x^3\right) \\
				& =  x^2-\dfrac12x^3+\dfrac13x^4+ ε(x)x^4 \\
				& \qquad -\dfrac12x^3+\dfrac14x^4+ ε_1(x)x^4 \\
				& \qquad +\dfrac13x^4 + ε_2(x)x^4 \\
				& \qquad + ε_3(x)x^4 \\  
				& =  x^2-x^3+\dfrac{11}{12}x^4+ ε_4(x)x^4 \\
			  \end{align*}$


		   1.  $\displaystyle{\dfrac{\operatorname{sh} x-x}{x^3}}$ (à l'ordre $6$).

       		   Pour le DL de $\displaystyle{\dfrac{\operatorname{sh} x-x}{x^3}}$ on commence par faire un DL du numérateur.
	    	   Tout d'abord :
		   
     		   $
			   \operatorname{sh}                                x                                =
			   x+\dfrac{1}{3!}x^3+\dfrac{1}{5!}x^5+\dfrac{1}{7!}x^7+\dfrac{1}{9!}x^9
			   +ε(x) x^9
			   $

			   donc 

			   $
			   \operatorname{sh}                x               -                x               =
			   \dfrac{1}{3!}x^3+\dfrac{1}{5!}x^5+\dfrac{1}{7!}x^7+\dfrac{1}{9!}x^9
			   +ε(x) x^9.
			   $

			   Il ne reste plus qu'à diviser par $x^3$ :

			   $
			   \dfrac{\operatorname{sh} x-x}{x^3} = \dfrac{\dfrac{1}{3!}x^3+\dfrac{1}{5!}x^5+\dfrac{1}{7!}x^7+\dfrac{1}{9!}x^9 +ε(x) x^9 }{x^3} 
			   =        \dfrac{1}{3!}+\dfrac{1}{5!}x^2+\dfrac{1}{7!}x^4+\dfrac{1}{9!}x^6
			   +ε(x) x^6
			   $

			   Remarquez que nous avons commencé par calculer un DL du numérateur à l'ordre $9$,
			   pour obtenir après division un DL à l'ordre $6$.


		   1.  $\exp\big(\sin(x)\big)$ (à l'ordre $4$).

    		   On sait $\sin x= x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)$
			   et $\exp(u)=1+u+\dfrac1{2!} u^2+\dfrac{1}{3!}u^3+\dfrac{1}{4!}u^4+o(u^4)$.


			   On note désormais toute fonction $ε(x)x^n$ (où $ε(x)\to 0$ lorsque $x\to0$) par ${\Large \scr o}(x^n)$.
			   Cela évite les multiples expressions $ε_i(x)x^n$.


			   On substitue $u=\sin(x)$, il faut donc calculer $u, u^2, u^3$ et $u^4$ : 

			   $
			   u = \sin x= x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)
			   $

			   $
			   u^2 =  \big( x -\dfrac{1}{3!}x^3  + {\Large \scr o}(x^4)\big)^2 = x^2-\dfrac13  x^4 +
			   {\Large \scr o}(x^4)
			   $

			   $
			   u^3 = \big( x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)\big)^3 = x^3 + {\Large \scr o}(x^4)
			   $

			   $
			   u^3 = x^4 + {\Large \scr o}(x^4) \quad \text{ et } \quad o(u^4)={\Large \scr o}(x^4)
			   $

			   Pour obtenir :
			   $\begin{align*}
				 \exp(\sin(x)) 
				   & =  1+ x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)\\
				   &  \qquad   + \dfrac1{2!}\big(x^2-\dfrac13 x^4 + {\Large \scr o}(x^4)\big) \\
				   &  \qquad   + \dfrac1{3!}\big(x^3 + {\Large \scr o}(x^4)\big) \\
				   &  \qquad   + \dfrac1{4!}\big(x^4 + {\Large \scr o}(x^4)\big) \\    
				   & \qquad + {\Large \scr o}(x^4) \\
				   & = 1+x + \dfrac12 x^2 - \dfrac18 x^4 + {\Large \scr o}(x^4).
			   \end{align*}$



		   1. $\sin^6(x)$ (à l'ordre $9$).

			 On sait $\sin (x)= x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)$.



			 Si l'on voulait calculer un DL de $\sin^2(x)$ à l'ordre $5$ on écrirait :

			 $
			 \sin^2 (x)  =  \big(x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)\big)^2 =  
			 \big(x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)\big) \times  \big(x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)\big) 
			 = x^2 -2\dfrac{1}{3!}x^4 + {\Large \scr o}(x^5).
			 $

			 En effet tous les autres termes sont dans ${\Large \scr o}(x^5)$.


			 Le principe est le même pour $\sin^6(x)$:

			 $
			 \sin^6 (x)  =  \big(x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4)\big)^6 =  
			 \big(x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4) \big) \times  \big(x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4) \big) 
			 \times \big(x -\dfrac{1}{3!}x^3 + {\Large \scr o}(x^4) \big) \times \cdots
			 $

			 Lorsque l'on développe ce produit en commençant par les termes de plus petits degrés on obtient 

			 $
			 \sin^6 (x)  = x^6 +  6 \cdot x^5 \cdot  (-\dfrac1{3!} x^3) +  {\Large \scr o}(x^9) =
			 x^6-x^8 + {\Large \scr o}(x^9)
			 $



		   1.  $\ln \big(\cos(x)\big)$ (à l'ordre $6$).

			  Le DL de $\cos x$ à l'ordre $6$ est

			  $
			  \cos x =  1 - \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4  - \dfrac{1}{6!}x^6 +
			  {\Large \scr o}(x^6).
			  $

			  Le DL de $\ln(1+u)$ à l'ordre $6$ est
			  $\ln(1+u)=u-\dfrac12u^2+\dfrac13u^3-\dfrac14u^4+\dfrac15u^5-\dfrac16u^6+o(u^6)$.

			  On pose $u= - \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 - \dfrac{1}{6!}x^6 + {\Large \scr o}(x^6)$ de sorte que

			  $
			  \ln(\cos                  x)                   =                  \ln
			  (1+u)=u-\dfrac12u^2+\dfrac13u^3-\dfrac14u^4+\dfrac15u^5-\dfrac16u^6+o(u^6).
			  $

			  Il ne reste qu'à développer les $u^k$, ce qui n'est pas si dur que cela si les calculs sont bien menés et 
			  les puissances trop grandes écartées.

			  Tout d'abord :
			  $\begin{align*}
			  u^2
				& = \left(- \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 - \dfrac{1}{6!}x^6 + {\Large \scr o}(x^6)\right)^2 \\
				& = \left(- \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 \right)^2 + {\Large \scr o}(x^6) \\
				& = \left(- \dfrac{1}{2!} x^2\right)^2 + 2 \left(- \dfrac{1}{2!} x^2\right) \left(\dfrac{1}{4!}x^4 \right) + {\Large \scr o}(x^6) \\
				& = \dfrac14 x^4 - \dfrac1{24} x^6 + {\Large \scr o}(x^6) \\
			  \end{align*}$

			 Ensuite :
			  $\begin{align*}
			  u^3 
				& = \left(- \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 - \dfrac{1}{6!}x^6 + {\Large \scr o}(x^6)\right)^3 \\
				& = \left(- \dfrac{1}{2!} x^2 \right)^3 + {\Large \scr o}(x^6) \\
				& =  -\dfrac18 x^6 + {\Large \scr o}(x^6) \\
			  \end{align*}$
			  En effet lorsque l'on développe $u^3$ le terme $(x^2)^6$ est le seul terme dont l'exposant est $\le 6$.

			  Enfin les autres termes $u^4$, $u^5$, $u^6$ sont tous des ${\Large \scr o}(x^6)$. Et en fait développer $\ln(1+u)$ à l'ordre $3$ est suffisant.

			  Il ne reste plus qu'à rassembler :
			  $\begin{align*}
			  \ln(\cos x) 
				& = \ln (1+u) \\
				& = u-\dfrac12u^2+\dfrac13u^3+o(u^3) \\
				& = \left(- \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 - \dfrac{1}{6!}x^6 + {\Large \scr o}(x^6)\right)\\
				& \qquad   -\dfrac12 \left(\dfrac14 x^4 - \dfrac{1}{24} x^6 + {\Large \scr o}(x^6)\right) \\
				& \qquad   +\dfrac13 \left(-\dfrac18 x^6 + {\Large \scr o}(x^6)\right)\\
				& = - \dfrac{1}{2} x^2 -\dfrac{1}{12}x^4 -\dfrac{1}{45}x^6  + {\Large \scr o}(x^6)\\
			  \end{align*}$

		 1.  $\displaystyle{\dfrac{1}{\cos x}}$ à l'ordre $4$.

			 Le DL de $\cos x$ à l'ordre $4$ est

			  $
			  \cos x = 1 - \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 + {\Large \scr o}(x^4).
			  $

			  Le DL de $\dfrac{1}{1+u}$ à l'ordre $2$ (qui sera suffisant ici) est
			  $\dfrac{1}{1+u}=1-u+u^2+o(u^2)$.

			  On pose $u=- \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 + {\Large \scr o}(x^4)$ et on a $u^2 = \dfrac14 x^4 + {\Large \scr o}(x^4)$.

			  $\begin{align*}
			  \dfrac{1}{\cos x}
				& =  \dfrac{1}{1+u} \\
				& =  1-u+u^2+o(u^2) \\
				& = 1 -\big(- \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 + {\Large \scr o}(x^4)\big)+\big(- \dfrac{1}{2!} x^2 + \dfrac{1}{4!}x^4 + {\Large \scr o}(x^4)\big)^2 +{\Large \scr o}(x^4)  \\
				& = 1+\dfrac{1}{2}x^2+\dfrac{5}{24}x^4 + {\Large \scr o}(x^4) \\
			  \end{align*}$


		 1.  $\tan x$ (à l'ordre $5$ (ou $7$ pour les plus courageux)).

			 Pour ceux qui souhaitent seulement un DL à l'ordre $5$ de $\tan x =\sin x \times \dfrac{1}{\cos x}$ alors
			 il faut multiplier le DL de $\sin x$ à l'ordre $5$ par le DL de $\dfrac{1}{\cos x}$ à l'ordre $4$ (voir question précédente).


			 Si l'on veut un DL de $\tan x$ à l'ordre $7$ il faut d'abord refaire le DL $\dfrac{1}{\cos x}$ mais cette fois à l'ordre $6$ :

			 $
			 \dfrac{1}{\cos                     x}=1+\dfrac{1}{2}x^2+\dfrac{5}{24}x^4
			 +\dfrac{61}{720}x^6 + {\Large \scr o}(x^6)
			 $

			 Le DL à l'ordre $7$ de $\sin x$ étant :

			 $
			 \sin  x  =  x  -\dfrac{1}{3!}x^3  +\dfrac{1}{5!}x^5  -  \dfrac{1}{7!}x^7
			 +{\Large \scr o}(x^7)
			 $

			 Comme  $\tan x = \sin x \times \dfrac{1}{\cos x}$, il ne reste donc qu'à multiplier les deux DL
			 pour obtenir après calculs :

			 $
			 \tan x  = x +  \dfrac{x^3}{3} + \dfrac{2x^5}{15} +  \dfrac{17x^7}{315} +
			 {\Large \scr o}(x^7)
			 $



		   1. $(1+x)^{\dfrac{1}{1+x}}$ (à l'ordre $3$).

			 Si l'on pense bien à écrire $(1+x)^{\dfrac{1}{1+x}}= \exp\left( \dfrac{1}{1+x} \ln(1+x) \right)$
			 alors c'est juste des calculs utilisant les DL à l'ordre $3$ de $\ln(1+x)$, $\dfrac{1}{1+x}$ et 
			 $\exp x$.

			 On trouve 

			 $
			 (1+x)^{\dfrac{1}{1+x}} = 1+x-x^2 + \dfrac{x^3}{2} + {\Large \scr o}(x^3).
			 $



!!! {{exercice("Pour les Stakhanovistes") }}

	Vérifiez les solutions proposées pour ces DL :



	$$
	\begin{align*}
	x/\sin x              &\underset{x\to 0}{=} 1 + x^2/6 + 7x^4/360 + {\Large \scr o}(x^4)              \cr
	1/\cos x              &\underset{x\to 0}{=} 1 + x^2/2 + 5x^4/24 + {\Large \scr o}(x^4)               \cr
	\ln(\sin x/x)         &\underset{x\to 0}{=} -x^2/6 -x^4/180 -x^6/2835  + {\Large \scr o}(x^6)        \cr
	\exp(\sin x/x)        &\underset{x\to 0}{=} e(1 - x^2/6 + x^4/45) + {\Large \scr o}(x^4)             \cr
	\sqrt{\tan x}         &\underset{x\to \pi/4}{=} 1 + h + h^2/2   + {\Large \scr o}(h^2), h = x-\pi/4                   \cr
	\sin(x+x^2+x^3-x^4)   &\underset{x\to 0}{=} x + x^2 + 5x^3/6 -3x^4/2  + {\Large \scr o}(x^4)           \cr
	\ln(x\tan(1/x))       &\underset{x\to 0}{=} x^{-2}/3 + 7x^{-4}/90  + {\Large \scr o}(1/x^4)              \cr
	(1-\cos x)/(e^x-1)^2  &\underset{x\to 0}{=} 1/2 - x/2 + x^2/6   + {\Large \scr o}(x^2)                 \cr
	\sin((\pi\cos x)/2)   &\underset{x\to 0}{=}1 -\pi^2x^4/32 + \pi^2x^6/192  + {\Large \scr o}(x^6)     \cr
	\cos x\ln(1+x)        &\underset{x\to 0}{=} x - x^2/2 - x^3/6    + {\Large \scr o}(x^4)                \cr
	(\sin x-1)/(\cos x+1) &\underset{x\to 0}{=} -1/2 + x/2 - x^2/8 + {\Large \scr o}(x^2)                 \cr
	\ln(2\cos x+\tan x)   &\underset{x\to 0}{=} \ln2+x/2-5x^2/8+11x^3/24-59x^4/192  + {\Large \scr o}(x^4) \cr
	e^{\cos x} &\underset{x\to 0}{=} e(1 - x^2/2 + x^4/6) + {\Large \scr o}(x^5) \cr
	x\sqrt{(x-1)/(x+1)}   &\underset{x\to 2}{=} 1/\sqrt3(2 + 5h/3 + h^3/54)  + o(h^3), h = x-2              \cr
	\sqrt{1+\sqrt{1-x}}   &\underset{x\to 0}{=} \sqrt2(1 - x/8 - 5x^2/128 - 21x^3/1024) + {\Large \scr o}(x^3)  \cr
	\sqrt{1-\sqrt{1-x^2}} &\underset{x\to 0}{=} |x|/\sqrt2(1 + x^2/8 + 7x^4/128) + {\Large \scr o}(x^5)        \cr
	e^x-\sqrt{1+2x}       &\underset{x\to 0}{=} x^2 - x^3/3 + 2x^4/3 - 13x^5/15  + {\Large \scr o}(x^5)        \cr
	(\sqrt[3]{x^3+x^2}+\sqrt[3]{x^3-x^2})/x &\underset{x\to +\infty}{=} 2 - 2x^{-2}/9 + o(1/x^3) \cr
	(1-x+x^2)^{1/x}       &\underset{x\to 0}{=} e^{-1}(1 + x/2 + 19x^2/24) + {\Large \scr o}(x^2)           \cr
	((1+x)/(1-x))^\alpha  &\underset{x\to 0}{=} 1 + 2\alpha x + 2\alpha^2x^2 + 2\alpha(2\alpha^2+1)x^3/3 + {\Large \scr o}(x^3) \cr
	(\sin x/x)^{2/x^2}    &\underset{x\to 0}{=} e^{-1/3}(1 - x^2/90) + {\Large \scr o}(x^3)                \cr
	(\sin x/x)^{3/x^2}    &\underset{x\to 0}{=} e^{-1/2}(1 - x^2/60 - 139x^4/151200)+ {\Large \scr o}(x^4) \cr
	(1+\sin x)^{1/x}      &\underset{x\to 0}{=} e(1 - x/2 + 7x^2/24)  + {\Large \scr o}(x^2)                \cr
	(1+\sin x + \cos x)^x &\underset{x\to 0}{=} 1 + x\ln2 + x^2(\ln^22+1)/2  + {\Large \scr o}(x^2)         \cr
	(\sin x)^{\sin x}     &\underset{x\to \pi/2}{=} 1 - h^2/2 + 7h^4/24  + {\Large \scr o}(h^4), h = x-\pi/2                 \cr
	(\tan x)^{\tan2x}     &\underset{x\to \pi/4}{=} e^{-1}(1 + 2h^2/3 + 4h^4/5)   + {\Large \scr o}(h^4), h = x-\pi/4     \cr
	\end{align*}
	$$


!!! {{ exercice("Série harmonique")}}

    On pose pour tout entier $n$ supérieur ou égal à 1 : 
	
	$$
	v_n=\displaystyle\sum_{k=1}^n \dfrac{1}{k} \text{ et } w_n=v_n-\ln(n)
	$$
	
	1. Démontrer que $(∀n∈ℕ^*)(w_n-w_{n+1} >= 0)$
	2. Déterminer le DL$_2(0)$ de $\ln(1+x)-\dfrac{x}{1+x} $
	3.   En   déduire   que    $   w_n-w_{n+1}   \underset{n   \to   +\infty}{=}
	   \dfrac{1}{2n^2} + {\Large \scr o}\left(\dfrac{1}{n^2} \right)$
	4. Démontrer que la série de terme général $w_n-w_{n+1}$ est convergente.
	5. En déduire que la suite $(w_n)$ converge.
	



!!! {{ exercice("ESCP 2019")}}

	
	On admet que $\displaystyle\sum_{p=1}^{+\infty}{1\over p^2}={\pi^2\over 6}$.


	1.  Montrer que  la série  $\displaystyle \sum_{p\geq  1 }{(-1)^{p+1}\over  p^2}$
	   converge. Déterminer sa somme. 


	2. 
	    1. Montrer que pour tout $x\in [0,1]$, pour tout entier $p\geq 1$, on a :
	   
            $
	        \ln(1+x)= \displaystyle\sum_{k=0}^{p} (-1)^{k}{x^{k+1}\over k+1}+(-1)^{p+1}\displaystyle\int_0^x
	        {t^{p+1}\over 1+t} dt
	        $
	   
	    2. Soit $n\in ℕ^*$ fixé. Montrer que pour tout entier $p\geq 1$ et
	       tout $x\in [0,1]$, on a :
          
	        $
		    \ln(1+x^n)= \displaystyle\sum_{k=1}^{p+1} (-1)^{k+1} {x^{nk}\over k}+R_p(x)\
		    \hbox{ avec } |R_p(x)| \leq {1\over p+2}
		    $

	3. 
		1. Montrer que $\displaystyle \int_0^1 \ln(1+x^n) dx= \sum_{k=1}^{+\infty} {(-1)^{k+1} \over k(nk+1)}$.
		2. Montrer qu'il existe une constante $C>0$ telle que pour tout $n\in ℕ^*$, on a:
			
			$
			\left| n \displaystyle\sum_{k=1}^{+\infty} {(-1)^{k+1} \over k(nk+1)}-
			\displaystyle\sum_{k=1}^{+\infty} {(-1)^{k+1} \over k^2} \right| \leq {C\over n}
			$
		3. En déduire un équivalent de $I_n=\displaystyle \int_0^1 \ln(1+x^n) dx$
		   lorsque $n$ tend vers $+\infty$. 
	
	4. Soit $(u_n)$  la suite définie sur $ℕ^*$ par  $u_n=\displaystyle \int_0^1 {dt
	   \over 1+t^n}$.
	   Déduire de la question précédente que
	   $
	   u_n\underset{n\to+\infty}{=} 1-{\ln 2\over n} + {\pi^2\over 12n^2 } + {\Large\scr o}\left( {1\over n^2}\right)
	   $




!!! {{ exercice("HEC voie E")}}

	1. Donner le DL$_4(0)$ de $x\mapsto\ln(1+x^2)$.
	2.  Trouver  des  équivalents  simples de  $\ln(1+x^2)$  aux  voisinages  de
	   $+\infty$ et 0.
