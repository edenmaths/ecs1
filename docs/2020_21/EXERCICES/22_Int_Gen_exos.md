
{{ chapitre(22, "Intégrales impropres : exercices", 12)}}


!!! {{exercice()}}
	Déterminer la nature des intégrales suivantes:
	
	1. $ \displaystyle\int_1^{+\infty} \dfrac{t^2}{(1+t^2)^2} {\rm d}t$
	2. $ \displaystyle\int_1^{+\infty} \dfrac{\ln(t)}{1+t^2} {\rm d}t$
	3. $ \displaystyle\int_2^{+\infty} \dfrac{\sin(t)}{t^2} {\rm d}t$
	4. $ \displaystyle\int_1^{+\infty} \dfrac{\ln(t)}{t} {\rm d}t$
	5. $ \displaystyle\int_1^{+\infty} \ln\left(1+\dfrac{1}{t^2}\right) {\rm d}t$
	6. $ \displaystyle\int_0^1 \dfrac{1-{\rm e}^{-t}}{t} {\rm d}t$
	7. $ \displaystyle\int_1^{+\infty} \sin\left(\dfrac{1}{t^2} \right)\arctan(t^2) {\rm d}t$
	8.                          $                          \displaystyle\int_0^1
	   \dfrac{\tan(\sqrt{t})}{\ln\left(\cos(\sqrt{t})\right)} {\rm d}t$
	9. $ \displaystyle\int_0^{\frac{π}{2}} \dfrac{1}{\sqrt{\sin(t)}} {\rm d}t$
	10. $ \displaystyle\int_0^{\frac{π}{2}} \dfrac{1}{\tan(t)} {\rm d}t$
	11. $ \displaystyle\int_0^{\frac{π}{2}} \ln\left(\sin(t)\right) {\rm d}t$
	
	
!!! {{exercice()}}

	Établir la convergence et calculer:
	
	1. $ \displaystyle\int_{-\infty}^{+\infty} \dfrac{1}{1+t^2} {\rm d}t$
	2. $ \displaystyle\int_{1}^{+\infty} \dfrac{\arctan(t)}{t^2} {\rm d}t$
	2. $ \displaystyle\int_{0}^{1} \dfrac{\ln(1-t^2)}{t^2} {\rm d}t$
	2.   $   \displaystyle\int_{0}^{+\infty}  \dfrac{1}{(1+t^2)^n}   {\rm  d}t$,
	    sachant que	   $n∈ℕ^*$ et $1+t^2-t^2=1$
	2. $ \displaystyle\int_{0}^{+\infty} \dfrac{1}{1+t^3} {\rm d}t$ en utilisant 
       $ \dfrac{1}{1+t^3} = \dfrac{a}{1+t} + \dfrac{bt+c}{t^2-t+1} $
	1. $ \displaystyle\int_0^1 \dfrac{\ln(1+t^2)}{t^2} {\rm d}t$
	1. $ \displaystyle\int_0^{+\infty} \ln\left(1+\dfrac{1}{t^2} \right){\rm d}t$










<!--4.2-->

!!! {{exercice("ESCP B/L 2019")}}

	Soient $f$ et $g$ les fonctions définies par :
	$$\forall x\in ]0,1[\cup]1,+\infty[,\ f(x)=\displaystyle\dfrac{\ln x}{ 1-x} \ \hbox{ et } \forall\,x\in]0,+\infty[,\,\,\ g(x)=\displaystyle\dfrac{x}{ {\rm{e}}^x-1}.$$

	
	1.  
	    1. Montrer  que $f$  se prolonge  par continuité  en $1$  et que  $g$ se
	       prolonge par continuité en $0$. 
	       Dans la suite, on désigne par $f$ et $g$ les prolongements ainsi obtenus.
	

	    1. Montrer que : $\forall x\geq 0$, on a $\ 0\leq g(x)\leq 1$.
	
	2. On pose $L(x)=\displaystyle\int_1^x f(t)~{\rm d}t$.

	    1.  Montrer que $L$ est définie et dérivable sur $]0,+\infty[$.
	    1.  Montrer que $L$ est définie et continue en $0$.
	

	3. 
	    1. à l'aide du changement de variable $x=-\ln t$, montrer que $L(0)=\displaystyle\int_0^{+\infty} g(x)~{\rm d}x$.
	    1.  Pour tout entier $k \geq 1$, montrer la convergence de l'intégrale $\displaystyle\int_0^{+\infty} x{\rm{e}}^{-kx}~{\rm d}x$ et la calculer.
	    1.  Pour  tout $n\in  ℕ^*$,  on  admet  la convergence  de  l'intégrale
	        $\displaystyle\int_{0}^{+\infty} \dfrac{x\operatorname{e}^{-(n+1)x}}{ 1-{\rm{e}}^{-x}}~{\rm d}x$.

	        Montrer que : $L(0)=\displaystyle \sum_{k=1}^n \int_0^{+\infty} x{\rm{e}}^{-kx}~{\rm d}x + \displaystyle\int_{0}^{+\infty} \dfrac{x{\rm{e}}^{-(n+1)x}}{ 1-{\rm{e}}^{-x}}~{\rm d}x$.
	    
		1.  En admettant que $\displaystyle\lim_{n\to +\infty} \int_{0}^{+\infty} \dfrac{x{\rm{e}}^{-(n+1)x}}{1-{\rm{e}}^{-x}}~{\rm d}x=0$ et que $\displaystyle\sum_{n=1}^{+\infty} \dfrac{1}{ k^2}=\displaystyle\dfrac{\pi^2}{ 6}$, donner la valeur de $L(0)$.





<!--4.4-->

!!! {{exercice("ESCP B/L 2019")}} 



	1.  Soit $h$ et $k$ deux fonctions continues sur $[a,b]$ et à valeurs réelles.

	    1. En considérant pour tout réel $x$ l'intégrale $\displaystyle \int_a^b (x h(t)+k(t))^2 dt$, établir l'inégalité suivante :
	   
	        $$
	        \left( \int_a^b h(t) k(t) dt \right)^2 \leq \int_a^b \big(h(t)\big)^2
	        dt\times\int_a^b\big(k(t)\big)^2 dt
	        $$

        1. À quelle condition nécessaire et suffisante cette inégalité est-elle une égalité ?

    1.  
        1. On considère  une fonction  $f$  de classe  $C^1$ sur  $[a,b]$ et  à
           valeurs dans $ℝ$. Établir l'inégalité suivante: 
		   
		    $$
		    \forall\,x\in [a,b],\,\,(f(x)-f(a))^2 \leq (x-a)\int_a^x
		    \big(f'(t)\big)^2 dt
		    $$

	    1.  En déduire l'inégalité:
           
	        $$
	        \displaystyle\int_a^b\big(f(x)-f(a)\big)^2dx\leq
	        \dfrac{(b-a)^2}{ 2}\int_a^b \big(f'(t)\big)^2 dt 
	        $$

	3. 
	     1.  Utiliser  la  question  précédente  pour  trouver  un  majorant  de
	        $\displaystyle\int_0^1\big(\ln(1+x)\big)^2 dx$. 
	
	     1.   Calculer $\displaystyle\int_0^1  (\ln(1+x))^2 dx$  et vérifier  la
	         majoration précédente (on donne $\ln 2\approx 0.69$). 
	
	 4. On revient au cas général.
        Quelles  sont les  fonctions pour  lesquelles l'inégalité  obtenue à  la
        question 2.b) est une égalité? 


 <!--4.14-->

!!! {{exercice("ESCP B/L 2019")}} 


	1. Déterminer   les  valeurs   de  $x$  réel   pour  lesquelles   la  série
	   $\displaystyle \sum_{n\geq 1} \dfrac{e^{-nx}}{ \sqrt{n}}$ converge.
	   On note alors $\displaystyle f(x)=\sum_{n=1}^{+\infty}
       \dfrac{e^{-nx}}{ \sqrt{n}}$.  On admet que  la fonction $f$  est continue
       sur son domaine de définition $D$. 

	2. Montrer que la fonction $f$ est décroissante sur $D$.

	3. Déterminer  $\displaystyle \lim_{x\to +\infty} f(x)$.
	
	4. Montrer que $\displaystyle \lim_{x\to 0^+} f(x)= +\infty$.
	
    5. 
	
	    1. Soit $ g : t \mapsto \displaystyle \frac{e^{-xt}}{\sqrt{t}}$. Montrer
	       que pour tout $k \in ℕ^*$ et tout $t\in [k,k+1]$, on a : $g(k+1) \leq
	       g(t) \leq g(k)$. 

	    1. En déduire que :
	
		    $$
		    \lim_{x\to +\infty} \dfrac 1 {f(x)}\times \int_0^{+\infty}
		    \dfrac{e^{-xt} }{ \sqrt{t} } {\rm d}t=1
		    $$

	    1.  En déduire  que :  $\displaystyle \lim_{x\to  +\infty} \displaystyle
	       f(x)\times \sqrt{x}= C$, où $C$  est une constante réelle strictement
	       positive. 




!!! {{exercice("EML 2004")}}

	**Partie I : étude de la fonction** $x\mapsto x \displaystyle\int_0^{+\infty} \dfrac{\sin(t)}{t+x}  {\rm d}t$
	
	
	On note $\displaystyle F:]0;+\infty[\longrightarrow ℝ$ et $\displaystyle G:
	]0;+\infty[\longrightarrow ℝ$ les applications définies, pour tout réel $x
	\in]0;+\infty[$ par :
	
	$$
	F(x)=\int_1^x{\sin u\over u}{\rm d}u\text{ et }G(x)=\int_1^x{\cos u\over u}{\rm d}u
	$$

	1.  
        1. Montrer,  pour tout  réel  $\displaystyle  x \in  ]0;+\infty[ :\quad
           F(x)=-{\cos x\over x}+\cos 1-\int_1^x{\cos u\over u^2}{\rm d}u$.
        
	    1. En déduire que $F$ admet une limite finie en $+\infty$. On note $\alpha$ cette limite.
        
	    1.  De manière  analogue,  montrer que  $G$ admet  une  limite finie  en
	       $+\infty$. On note $\beta$ cette limite.
        
	    1. En déduire que, pour tout réel $x \in ]0;+\infty[$, les intégrales
	       $\displaystyle   \int_x^{+\infty}  {\sin   u\over   u}{\rm  d}u$   et
	       $\displaystyle \int_x^{+\infty}{\cos  u\over u}{\rm  d}u$ convergent,
	       et  que : $\displaystyle  \int_x^{+\infty}  {\sin u\over  u}{\rm d}u=\alpha-F(x) \text{ et }\int_x^{+\infty}{\cos u\over u}{\rm d}u=\beta-G(x)$.

	1.  
        1. Montrer, pour tout réel $x\in ]0;+\infty[$ et tout réel $T\in ]0;+\infty[$ :
 
	        $$
		    \int_0^T{\sin  t\over  t+x}{\rm  d}t=\cos  x\int_x^{x+T}{\sin  u\over u}{\rm d}u-\sin x\int_x^{x+T}{\cos u\over u}{\rm d}u
		    $$
 
	    1. En  déduire  que,  pour  tout  réel  $x\in]0;+\infty[$,  l'intégrale
	       $\displaystyle \int_0^{+\infty}{\sin t\over t+x}{\rm d}t$ converge et
	       que :
            
            $$
			\displaystyle   \int_0^{+\infty}{\sin   t\over   t+x}{\rm   d}t=\cos x\int_x^{+\infty}{\sin u\over u}{\rm d}u-\sin x\int_x^{+\infty}{\cos u\over u}{\rm d}u
			$$

	1. On note  $\displaystyle  A :   ]0;+\infty[\longmapsto ℝ$  l'application définie, pour tout réel $x \in ]0;+\infty[$, par :
	   $A(x)= \displaystyle\int_0^{+\infty}{\sin t\over t+x}{\rm d}t$
	   Montrer que  l'application $A$ est  de classe $C^2$ sur  $]0;+\infty[$ et que, pour tout réel $x \in ]0;+\infty[$ :
	    
	    $$
	    A'' (x) + A(x) = \dfrac{1}{ x}
		$$
 
	1. Établir que $A(x)$ et $A'(x)$ tendent vers $0$ lorsque $x$ tend vers $+\infty$.
 
	1. 
        1.  Montrer :  $\displaystyle  \forall   x  \in  ]0;1],\quad  0\leq \int_x^1{\cos u\over u}{\rm d}u \leq -\ln x$
	
	    1.  En déduire  que  $\displaystyle  \sin x\int_x^{+\infty}{\cos  u\over u}{\rm d}u$
		    tend vers $0$ lorsque $x$ tend vers $0$ par valeurs strictement positives.

	    1. Montrer  que l'intégrale $\displaystyle  \int_0^{+\infty}{\sin u\over u}{\rm d}u$ converge,
		   et établir  que $A(x)$ tend vers $\displaystyle \int_0^{+\infty}{\sin u\over u}{\rm d}u$ lorsque $x$ tend vers $0$ par valeurs strictement positives.


    **Partie II : Étude de la fonction** $\displaystyle x\longmapsto \int_0^{+\infty}{\operatorname{e}^{-xt}\over 1+t^2}{\rm d}t$} 
	
	1. Montrer  que, pour tout réel  $x \in ]0;+\infty[$ et  tout entier naturel
	   $k$,   l'application   $t\longmapsto    t^k\operatorname{e}^{-xt}$   est   bornée   sur
	   $[0;+\infty[$,   et    en   déduire   que    l'intégrale   $\displaystyle \int_0^{+\infty}{t^k\operatorname{e}^{-xt}\over 1+t^2}{\rm d}t$ converge.

	1. On note, pour tout entier naturel $k$, $B_k : ]0;+\infty[\longrightarrow ℝ$ l'application  définie, pour tout réel  $x\in]0,+\infty[$, par : $\displaystyle  B_k(x)   =  \int_0^{+\infty}{t^k\operatorname{e}^{-xt}\over  1+t^2}{\rm d}t$. 
	   
        1. Montrer, en utilisant par exemple l'inégalité de Taylor-Lagrange :
	
	        $$
	 	    \forall u\inℝ,\quad |\operatorname{e}^u-1-u|\leq {u^2\over 2}\operatorname{e}^{|u}|
		    $$
		   
	    1. En  déduire, pour  tout réel  $x \in  ]0;+\infty[$, pour  tout entier naturel $k$ et pour tout réel $h$ tel que $0 <|h|\leq{x\over 2}$ :
		   
		    $$
			|{B_k(x+h)-B_k(x)\over    h}+B_{k+1}(x)|\leq     {|h|\over 2}B_{k+2}\Bigl({x\over2}\Bigr)
		    $$
			
		1. Montrer que, pour tout entier naturel $k$, $B_k$ est dérivable sur $]0;+\infty[$ et que :
	       
		    $$
			\forall x\in]0; +\infty[,\qquad B_k'(x)=-B_{k+1}(x)
			$$

	    1. En  déduire que $B_0$ est  de classe $C^2$ sur  $]0;+\infty[$ et que, pour tout réel $x \in]0;+\infty[$ :
	        
			$$
			B_0''(x) + B_0(x) = {1\over x}
			$$

    1. Montrer, pour tout réel $x \in ]0;+\infty[$ :
	    
		$$
		0\leq B_0(x) \leq {1\over x}\text{ et }0\leq -B_0'(x)\leq{1\over x^2}
		$$

       et en déduire les limites de $B_0(x)$ et $B_0'(x)$ lorsque $x$ tend vers $+\infty$.
	
	1. 
        1. Montrer : $\displaystyle  ∀x∈]0;+\infty[, \operatorname{e}^{-\sqrt  x}\int_0^{\frac{1}{\sqrt x}}\dfrac{1}{1+t^2}{\rm  d}t \leq B_0(x) \leq \int_0^{+\infty}\dfrac{1}{1+t^2}{\rm d}t$.
	
	    1. Justifier, pour tout réel $\displaystyle y\in\Big[0;{\pi\over 2}\Big[$ :
			$
			\int_0^y{\rm d}u=\int_0^{\tan y}{1\over 1+t^2}{\rm d}t\;,
			$
	       et  en   déduire :  $\displaystyle   \int_0^{+\infty}{1\over 1+t^2}{\rm d}t={\pi\over 2}$ .
	
	    1.  En déduire  la limite  de  $B_0(x)$ lorsque  $x$ tend  vers $0$  par valeurs strictement positives.

	**Partie III  - Calcul  de l'intégrale**  $\displaystyle \int_0^{+\infty}{\sin u\over u}{\rm d}u$}

	On  considère  l'application  $\varphi   :  ]0;+\infty[  \longrightarrow  ℝ$ définie, pour tout réel $x\in]0;+\infty[$, par :
	
	$$
	\varphi(x)=A(x)-B_0(x)\;,
	$$
	
	où $A$ a été définie dans la Partie {\bf I} et $B_0$ a été définie dans la Partie {\bf II}.

	On note $U : ]0;+\infty[\longrightarrow ℝ$ l'application définie, pour tout réel $x \in ]0 ;+\infty[$, par :
	
	$$
	U(x) =\bigl(\varphi(x)\bigr)^2+ \bigl(\varphi'(x)\bigr)^2
	$$

	
	1. Montrer que $U$ est constante sur $]0;+\infty[$.
	
	1. Quelle est la limite de $U(x)$ lorsque x tend vers $+\infty$ ?
	
	1. En déduire : $\forall x\in]0;+\infty[,\quad A(x) = B_0(x)$.
	
	1. Quelle est la valeur de $\displaystyle \int_0^{+\infty}{\sin u\over u}{\rm d}u$ ?











































