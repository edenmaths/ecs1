---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


20. [Espaces probabilisés discrets](20_probas_dis_exos.md)
21. [Développements limités](21_DL_exos.md)
22. [Intégrales impropres](22_Int_Gen_exos.md)
23. [VAR à densité](23_Probas_Dens_exos.md)
24. [Convergence et approximations](24_conv_prob_cours.md)
1. [Extrema - Convexité](25_convexite_exos.md)
