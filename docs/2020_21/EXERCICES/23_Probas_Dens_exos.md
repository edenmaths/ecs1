{{ chapitre(23, "Variables aléatoires à densité", 2)}}



!!! {{ exercice()}}

	Soit X une VAR de densité la fonction $f_X$ définie sur $ℝ$ par :
	
	$$
	\begin{cases}
	 f(x) = α(4x-2x^2) \text{ si } x∈[0,2]\\
	 f(x) = 0 \text{ sinon.}
	 \end{cases}
	$$
	
	Calculez la valeur de $α$ et $ℙ([X>1])$.
	
!!! {{ exercice()}}

	Soit $(α,β)∈ ℝ_+^*⊗ℝ$. Déterminez le réel $k$ de telle sorte que la fonction
	$f_{(α,β)}:   x\mapsto  \dfrac{k}{α^2+(x-β)^2}   $  soit   une  densité   de
	probabilité.
	

!!! {{ exercice()}}

	Représentez $F_X$ et $f_X$ dans les cas suivants:
	
	1. $F_X(x)=\begin{cases}  0 \text{  si }  x<0 \\ x  \text{ si  } 0≤x≤1  \\ 1
	   \text{ si } x>1 \end{cases}$
	   1. $F_X(x)=\begin{cases} 0 \text{ si } x<1 \\ 1 - \dfrac{1}{x} 
	   \text{ si } x≥1 \end{cases}$
	1. $F_X(x)=\begin{cases}  0 \text{  si }  x≤0 \\ \sqrt{x}  \text{ si  } 0<x≤1  \\ 1
	   \text{ si } x>1 \end{cases}$ 
   	1. $F_X(x)=\begin{cases}  0 \text{  si }  x<-1 \\ \dfrac{x+2}{4}   \text{ si  } -1≤x≤1  \\ 1
	   \text{ si } x>1 \end{cases}$
	1. $F_X(x)=\begin{cases} 0 \text{ si } x<0 \\ 1 - \operatorname{e}^{-λx}
	   \text{ si } x≥0 \end{cases}$ avec $λ>0$.




!!! {{ exercice()}}
	
	Les fonctions suivantes sont-elles des densités de probabilité ?
	
	2. $f(x)=1-\vert 1-x\vert$ si $0<x<2$ et $f(x)=0$ sinon.
	1. $ f(x)  =  \dfrac{1}{4}  x \operatorname{e}^{-\frac{x}{2}}$  si  $x>0$  et
	   $f(x)=0$ sinon.
	1. $f(x)=\dfrac{\mathbb{1}_{]-1,1[}(x)}{π\sqrt{1-x^2}}$
	1. $f(x)=\dfrac{1}{2} \operatorname{e}^{-\vert x\vert}$




!!! {{ exercice()}}

	Soit $f$ la fonction de $ℝ$ dans $ℝ$ définie par $f(x)= \dfrac{\operatorname{e}^{-x}}{(1+\operatorname{e}^{-x}
	)^2}$
	
	1. Démontrer que  $f$ est une DDP.  Déterminer la FDR d'une  VAR X ayant
		   $f$ pour densité.
	1. Soit $φ$ la fonction de $ℝ$ dans $ℝ$ définie par $φ(x)=\dfrac{\operatorname{e}^{x}-1
		}{\operatorname{e}^{x}+1
		} $.
		
		Démontrer  que  $φ$  réalise  une  bijection  de  $ℝ$  sur  $]-1,1[$  et
		déterminer sa bijection réciproque.
	1. On définit une VAR Y par $Y=φ(X)=\dfrac{\operatorname{e}^{X}-1
		}{\operatorname{e}^{X}+1
		} $. Déterminer **la** FDR et **une** DDP de Y.
		



!!! {{ exercice("Loi de Laplace")}}

	Soit  $f$  la   fonction  définie  sur  $ℝ$   par  $f:x\mapsto  \dfrac{1}{2}
	\operatorname{e}^{-|x|}$.
	
	Démontrer  que $f$  est  une DDP  d'une  VAR X  dont on  donnera  la FDR  et
	éventuellement l'espérance.
	
!!! {{ exercice("Exercice de probabilités aka d'intégration en ECS")}}

	Soit      $f$     définie      sur      $ℝ$     par      $f(x)=\begin{cases}
	\dfrac{2x}{(1+x^2)\sqrt{1-x^4}}     \text{     si      }     x∈[0,1[\\     0
	\text{ sinon}.\end{cases}$.
	
	1. Vérifier que $f$ est une DDP.
	2. Soit X une VAR de densité $f$ : admet-elle une espérance ?


!!! {{ exercice("Loi exponentielle")}}

	Soit $X\leadsto {\scr E}(1)$. On pose $Y=\ln(\operatorname{e}^{X}-1)$. 
	
	1. Déterminer la FDR et une DDP de Y.
	2. Étudier l'existence et éventuellement calculer $\mathbb{E}(Y)$.
	
!!! {{ exercice("Loi uniforme")}}

	Soit $X\leadsto {\cal U}([0,1])$. 
	
	1. Rappeler les valeurs de l'espérance de $X$.
	2. On définit la VAR $Y$ définie par $Y=\dfrac{1}{1+X} $.  Déterminer la FDR
	   de Y et en déduire une DDP.
	   
!!! {{ exercice("Loi normale")}}

	Soit $X\leadsto {\cal N}(0,1)$. On pose $Y=\operatorname{e}^{X}$. Déterminer
	une DDP de Y.
	
	
!!! {{ exercice("À partir de la loi exponentielle")}}

	$X\leadsto  \mathcal{E}(λ)$  avec  $λ>0$.  Déterminer une  densité  des  VAR
	suivantes:
	$ Y=2X+5\quad Z=\operatorname{e}^{X}\quad W=\dfrac{1}{(1+X)^2} $


!!! {{ exercice("Mémoire de la loi normale")}}

	$X\leadsto \mathcal{N}(0,1)$. Démontrer que pour tout $a>0$
	
	$$
	 \lim_{x\to+\infty}\dfrac{ℙ([X ≥ x + \frac{a}{x}])}{ℙ([X ≥x])}=\operatorname{e}^{-a}
	$$
	
	
!!! {{ exercice("ESCP BL")}}

	
	Toutes les variables aléatoires de cet exercice sont définies sur un espace probabilisé $(\Omega, {\cal T}_Ω, ℙ)$.

	1.  Soit $f$ la fonction définie sur $ℝ$ par $f(x)= e^{-x}e^{-e^{-x}}$. Montrer que $f$ est une densité.
	*Indication : on pensera au changement de variable* $u=e^{-x}$.


	2. Soit $X$ une variable aléatoire de densité $f$.

	    1. Déterminer la fonction de répartition $F$ de $X$.
		2. À l'aide d'un développement  limité, déterminer un fonction $h$ telle
		   $ \displaystyle\lim_{x\to +\infty} \dfrac{ℙ(X\geq x)}{h(x)}=1.$
		   
	3. Soit  $U$ une variable aléatoire  suivant la loi uniforme  sur $]0,1[$ et
	   $V=-\ln(-\ln U)$. Déterminer la loi de $V$.

	4. Soit $(Y_n)$ une suite de variables aléatoires indépendantes de loi exponentielle de paramètre $1$. Déterminer pour tout $x\in ℝ_+$:
		
		$$
		\lim_{n\to +\infty} ℙ(\max(Y_1, \ldots, Y_n)-\ln n\leq x)
		$$
		
		
!!! {{ exercice("ESCP BL")}}

	Toutes les variables aléatoires intervenant dans cet exercice sont supposées
	définies sur le même espace probabilisé $\big(\Omega, {\cal A}, P\big)$. 
		
		
	1. Pour tout $n\in ℕ^*$, on considère la fonction $f_n$ définie par 
	$f_n(x)= (1-\cos(2n\pi x))\times \mathbb{1}_{[0,1]}(x)$. 


	Vérifier que $f_n$ est une densité de probabilité.


	On considère  une suite $(X_n)_n$  de variables aléatoires telles  que, pour
	tout  entier naturel  $n$  non nul,  la variable  aléatoire  $X_n$ admet  la
	fonction $f_n$ comme densité. 


	2. Pour tout $n\inℕ^*$, calculer l'espérance de $X_n$.


	3. 
	     1. Pour tout $n$ de $ℕ^*$, déterminer la fonction de répartition $F_n$ de $X_n$.
		 2.        Pour       tout        $x$        réel,       on        pose:
		    $F(x)=\displaystyle\lim_{n\to\,+\infty}F_n(x)$.  Montrer   que  $F$   est  la
		    fonction de répartition d'une variable aléatoire $X$ dont on précisera la loi.


!!! {{ exercice("HEC voie E")}}

	Soit $f$ la fonction définie par $\displaystyle f(x)=\int_x^{2x}e^{-t^2}dt$.
	
	1. Étudier la parité de $f$ ( en utilisant un changement de variable).
    1.  Trouver le signe de $f(x)$ suivant les valeurs de $x$.
	1.  En encadrant  $\displaystyle e^{-t^2}$ pour $t>1$, majorer  $f(x)$ et en
	    déduire la limite de $f(x)$ quand $x$ tend vers l'infini. 
	1.  Étudier les variations de $f$.
	1.  Soit $X$ une V.A.R. suivant une loi normale centrée d'écart type $\sigma$.
	Trouver $a>0$ pour que $ℙ(a ≤ X ≤ 2a)$ soit maximal.
	
	

!!! {{ exercice("HEC voie E")}}

	Pour tout réel $m$, on note $f_m$ la fonction définie sur ${ℝ}$ par:
	
	$$
	f_m(x)=e^{-|x-m|}
	$$

	1. 
	    1.  Étudier cette fonction: continuité, dérivabilité, variations, limites,...
	    1.  Représenter graphiquement $f_0$.
	        Comment peut-on en déduire la représentation de $f_m$ ?
	1. 
	    1.  Calculer $\displaystyle \int_{-\infty}^{+\infty} f_m(t)\ dt$.
		1.  Déterminer l'unique réel $k$ tel que $k.f_m$ soit une densité de probabilité.
		1.  Soit $X_m$ une variable aléatoire admettant $k.f_m$ pour densité.
			Montrer que $X_m$ a une espérance et une variance et calculer ses moments.
		1.   Déterminer la  fonction de  répartition  $F_m$ de  $X_m$ et  donner
		    l'allure de la représentation graphique de $F_m$.
	1.   Une entreprise  vend de  la farine  conditionnée en  sac. Le  poids, en
	    kilogrammes, d'un sac quelconque est  une variable aléatoire $X$ de même
	    loi que $X_{20}$. 
		1.  Quelle est la probabilité qu'un sac pèse plus de 20 kilos? moins de 20 kilos?
		1.  Quelle est la probabilité que le poids d'un sac soit compris entre 18 et 22 kilos.
		1.  Quelle est la probabilité que le  poids d'un sac soit supérieur à 21
		    kilos, sachant qu'il est supérieur à 20 kilos ? 
		1.   Comment choisir  $\alpha$ pour  que la  probabilité de  l'événement
		    « l'écart $|X-20|$ entre le poids d'un sac et sa moyenne est
		    inférieur à $\alpha$ » soit supérieur ou égales à $0,9$? 



!!! {{ exercice("HEC voie E")}}

	Un composant électronique a une durée de vie $X$, variable
	aléatoire positive de densité $f$. Pour tout réel $t \in ℝ^{+*}$, 
	on définit la fiabilité de ce composant à l'instant $t$ par
	
	$$
	R(t)=ℙ([X>t])
	$$
	
	et on définit le taux de défaillance par
	
	$$
	h(t)=\lim_{x \rightarrow 0}\frac{ℙ_{[t<X]}([t<X\le t+x])}{x}
	$$
	
	
	1.  Quel lien existe-t-il entre $R$ et $t$?
	1.  Montrer que, pour tout $t$ strictement positif :
	
		$$
		h(t)=-[\ln(R(t))]'=\frac{f(t)}{R(t)}
		$$
		
	1.  On suppose que $X$ suit une loi exponentielle de paramètre
		$\lambda$. Montrer que, pour $s$ et $t$ strictement positifs, on a

	    $$
		ℙ_{[X>t]}([X>t+s])=ℙ([X>s])
		$$
	
	1.  Montrer qu'un composant qui a un taux de défaillance constant
	    a une durée de vie exponentielle.
	1.  On considère $n$ composants électroniques de durée de vie
	    indépendantes et de même loi $X$. On note $N(t)$ le nombre de
	    composants encore en marche à la date $t$. Quelle est la loi de
	    $N(t)$?
		Montrer que, pour tout $t>0$, on a
		
		$$
		h(t)=\lim_{x \rightarrow 0}\frac{\mathbb{E}(N(t))-\mathbb{E}(N(t+x))}{x\mathbb{E}(N(t))}
		$$
		
		Commenter ce résultat.
