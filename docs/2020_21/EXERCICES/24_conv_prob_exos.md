{{ chapitre(24, "Probabilités : convergences et approximations - Exercices", 7)}}



!!! {{ exercice()}}

	Soit  $(X_n)_{n\inℕN^\ast}$  une  suite  de   VAR  telle  que,  pour  tout
	$n\inℕ^\ast$, $X_n$  suit une loi uniforme  sur $\left\{0,\frac1n, \frac2n,
	\dots, \frac{n-1}n, 1 \right\}$. Démontrer qu'elle converge en loi vers une VAR. $X$ de loi
	uniforme sur $[0,1]$. 
	
	
	
!!! {{ exercice()}}

	Si pour  tout $ n  \inℕ^*$, $X_n$ suit  une loi exponentielle  de paramètre
	$n$, la suite des $(X_n)$ converge  en probabilités vers la v.a.r. constante
	égale à $0$. 
	
!!! {{ exercice()}}

	Pour tout $  n \in ℕ^* $, on  définit une variable aléatoire $  X_n $ telle
	que $  X_n =  n $  avec probabilité  $ \frac{1}{n}  $ et  $ X_n  = 0  $ avec
	probabilité $ 1 - \frac{1}{n} $.
	
	1. Démontrer que $(X_n) _{n \geqslant 0}$ converge en probabilités vers la v.a.r. constante $0$
	2.  Démontrer  que  $  \left(\mathbb{E}(X_n)\right)_{n\inℕ} $  ne  converge  pas  vers
      $\mathbb{E}(0)$. 
	  
	  
!!! {{ exercice()}}

	Pour tout  $ n \in ℕ^*  $, on définit  $X_n$ une variable aléatoire  de loi
	uniforme  sur $  \left[ -\frac{1}{n}  ,  \frac{1}{n} \right]  $. Étudier  la
	convergence en loi de la suite $(X_n) _{n\in ℕ^*}$. 
	
	
!!! {{ exercice("TCL")}}

	Soit  $\left(X_n  \right)_{n\inℕ^*}$  une  suite  de  variables  aléatoires
	suivant chacune une loi ${\cal B}\left( n , \dfrac{1}{3} \right) $ 

	1.   Étudier  la   convergence  en   loi  de   la  suite   de  variables   $
	   \left( \dfrac{  X_n - \frac{n}{3} }{  \sqrt{\frac{2n}{9}} } \right)_{n\in
	   ℕ^*} $ 

	2.  En déduire que $ \displaystyle \lim_{n\to +\infty} \dfrac{1}{3^n} \displaystyle\sum _{k=0}
		^{ \lfloor n/3\rfloor } \dbinom{n}{k} 2^{n-k} = \dfrac{1}{2} $. 
		
	
	!!! {{ exercice()}}

	    === "Énoncé"

	        On lance un dé à six faces numérotées de 1 à 6 bien équilibré. 
			Avec l'inégalité de Bienaymé-Tchebychev, déterminer un minorant du nombre de
			lancers nécessaires pour  que l'on ait plus d'une chance  sur deux d'obtenir une
			fréquence de  6 s'écartant de moins  de $10^{-2}$ de la  valeur théorique $\frac
			16$.  

	        Discuter de la pertinence de la valeur obtenue.
	
	        Obtenez un minorant plus précis à l'aide d'un code Scilab.
	

		=== "Indications"

	         ```scilab
			 p = 0 // la proba cherchée
			 n = 3 // pour que l'intervalle de la boucle de long non 0
			 d = 0.01 // la précision demandée
			 while p < 0.5 // tant qu'on n'as dépassé la proba 1/2
				 n = n + 1 // on incrémente n
				 p = 0 // on remet la proba à 0 avant de la recalculer
				 for k = ceil(n/6-d):floor(n/6+d) // à moins de 0.01 de n/6
				     // on rajoute ℙ(X=k)
					 p = p + prod((n-k+1):n)/prod(1:k)*(1/6)^k*(5/6)^(n-k)
				 end
			 end
			 ```

	
!!! {{ exercice()}}

	Soit $X$ une v.a.r. suivant une loi de Poisson de paramètre $\lambda$.

    1. Démontrer que $ℙ\left(X\geqslant \lambda^2\right)\leqslant \frac{1}{\lambda}$
    1. Démontrer      que     $[X\leqslant      \frac{\lambda     }{2}]\subset
       \left[|X-\lambda|\geqslant \frac{\lambda }{2} \right]$. 
	1.  En  déduire  que  $ℙ\left(X\leqslant  \frac{\lambda}{2}\right)\leqslant
	   \frac{4}{\lambda}$
	   
	   
!!! {{ exercice()}}

	Soit $c>0$. Pour tout $n\inℕ^\ast$, on définit la  v.a.r. 
	$X_n$ par 
	
	$$
	X_n(\Omega) = \left\{ 0,n^c\right\} \quad \text{et}\quad ℙ(X_n = n^c)=\frac1n
	$$
	

	1. Démontrer  que $(X_n)_{n\inℕ}$ converge  en probabilités vers  la variable
	   aléatoire constante égale à 0. 
	1. Déterminer, selon la valeur de $c$, $ \displaystyle\lim_{n\to+\infty}\mathbb{E}(X_n)$.
	1. Conclure 


!!! {{ exercice()}}

	Soit $(p_n)_{n\inℕ^\ast}$ une suite de réels dans $]0,1[$, qui converge
	vers un réel $p\in]0,1[$.  

	Soit  $(X_n)_{n\inℕ^\ast}$ une  suite de  variables aléatoires  telles que,
	pour tout $n\inℕ^\ast, X_n\leadsto \mathcal{G}(p_n)$.  

	Démontrer que $(X_n)_{n\inℕ^\ast}$ converge en  loi vers une variable suivant
	la loi géométrique de paramètre $p$. 
	
	
!!! {{ exercice()}}

	1. Soit $t\inℝ_+^\ast$. Soit $X$ une v.a.r discrète telle que ${\rm e}^{tX}$ admette une espérance.
	   Démontrer, à l'aide de l'inégalité de Markov, que pour tout $a\inℝ$, 
	   
	    $$
	    ℙ(X≥ a)≤ \dfrac{\mathbb{E}\left({\rm e}^{tX}\right)}{e^{ta}}
	    $$
	   
	1.  Soient $n\inℕ^\ast$ et $p\in]0,1[$. On suppose que $X\leadsto {\cal B}(n,p)$ 
		1.  Démontrer que pour tout $t\inℝ_+^\ast$, ${\rm e}^{tX}$ admet une espérance et que
			
			$$
			\mathbb{E}\left({\rm e}^{tX} \right) = \left(1-p+p{\rm e}^t \right)^n.
			$$
		
		1. Étudier les variations de la fonction $f:t\mapsto(1-p){\rm e}^{-\frac t2} + p{\rm e}^{\frac t2}$

	    1.  À l'aide de la question  1, montrer que $ℙ\left(X\geqslant \frac n2
	        \right)\leqslant 2^n\left(p\left(1-p\right)\right)^{\frac n2}$. 
			
!!! {{ exercice()}}

	Soit $(X_k)_{k\inℕ^\ast}$  une suite  de variables aléatoires  définies sur
	l'espace probabilisé  $(\Omega, \mathcal{A}, ℙ)$, indépendantes  et suivant
	toutes la loi uniforme sur le segment $[0,1]$. 

	1.  Étudier la convergence de la suite $(Y_n)_{n\inℕ^\ast}$ définie par une relation 
		
		$$
		Y_n= \max(X_1,X_2,\dots,X_n).
		$$

	    *on étudiera la convergence en loi et la convergence en probabilité.*

	1.  Pour tout  $n\inℕ^\ast$, on pose $Z_n = n(1-Y_n)$.  Démontrer que $(Z_n)$
	    converge en loi vers une loi à préciser. 
		
		
!!! {{ exercice()}}

	On dispose de trois dés cubiques équilibrés et indiscernables que l'on lance simultanément.

	1.  Vérifier qu'il y a autant de résultats donnant 9 ou 10.
	1.  On répète $n$ fois l'expérience et on note $X_n$ (respectivement $(Y_n)$
	    le nombre  de fois où  l'on a obtenu 9  (resp. 10). Écrire  une fonction
	    **Scilab** d'entête `function [X,Y]  = Toscane(n)` qui simule $n$
	    tirages et retourne le nombre de fois où 9 et 10 ont été obtenus.
	1.  L'exécution de `[X,Y] = Toscane(10000)` donne `X = 1168` et `Y = 1289`. 
		Pourquoi parle-t-on de paradoxe ?
	1.  Déterminer la loi de $X_n$ et $Y_n$.
	1.  Que dire de la  convergence en probabilité des suites $\left(\dfrac1nX_n
	    \right)$ et $\left(\dfrac1nY_n \right)$ ?\par Lever le paradoxe. 
		
		
!!! {{ exercice()}}

	Soit  $n\inℕ^\ast$. On  considère  les  variables aléatoires  indépendantes
	$X_1,\dots,X_n$. de même loi et d'espérance $m$, d'écart-type $\sigma>0$. On
	pose $S_n=X_1+\dots+X_n$. 
	
	1.  Démontrer que $S_n$ possède une espérance et une variance que l'on déterminera.
	1.   On  pose  $S_n^\ast  =  \dfrac{S_n  -  \mathbb{E}(S_n)}{\sigma_{S_n}}$.  Donner
	    l'expression de $S_n^\ast$ en fonction de $S_n$, $m$, $\sigma$. 
	1.  Soit $\beta\inℝ$. On pose $ \displaystyle p_{n,\beta} = ℙ\left(|S_n^\ast|<n^\beta \right)$
		
		1.  On suppose $\beta=0$. Prouver 
		
		    $$
		    \displaystyle\lim_{n\to+\infty}p_{n,0}= \Phi(1) - \Phi(-1)
		    $$
		
		    où $\Phi$ désigne la fonction de répartition de la loi normale centrée
		    réduite $\mathcal{N}(0,1)$. 
	
		    Donner une valuer approchée de la limite sachant $\Phi(1)\approx 0,8413$.
	
	    1.  On suppose $\beta>0$.
		    1.  Démontrer 
			    $p_{n,\beta} = ℙ\left( |S_n - nm|<\sigma n^{\beta+\frac12}\right)$.
		    1.  Par l'inégalité de Bienaymé-Tchebychev, prouver
		       $p_{n,\beta}\geqslant 1-\frac{1}{n^{2\beta}}$. 
		    1.  Déterminer la limite $p_{n,\beta}$ quand $n$ tend vers $+\infty$.
	    1.  Désormais, on a $\beta <0$. 
		    On supposera ici que les variables aléatoires $X_1,\dots,X_n$ suivent
		    une loi $\mathcal{N}(0,1)$. 
		    1.  $\forall n\inℕ^\ast,\quad p_{n,\beta} = 2\left( \Phi(n^\beta) - \frac12\right)$
		    1.  $ \displaystyle\lim_{n\to+\infty} p_{n,\beta} = 0$
		    1.  $p_{n,\beta} \underset{n\to+\infty}{\sim} \sqrt{\frac{2}{\pi}}n^\beta$
		

!!! {{ exercice()}}

	
	Soit $(X_i)_{i\in ℕ^*}$ une famille de variables  aléatoires à
	valeurs dans $ℕ^*$, indépendantes, de même loi et définies sur
	le m\^eme espace probabilisé $(\Omega,{\cal A},P)$. Pour tout entier
	$n\in ℕ^*$, on définit la variable aléatoire $Y_n$ par  :
	
	$$
	Y_n=\underset{1≤ i≤ n}{\min} X_i
	$$



	1.  Exprimer la fonction de répartition de $Y_n$    à
	l'aide de la fonction $k\mapsto P(X_1\geq k)$.

	2.  **Dans cette question seulement** on suppose que les $X_i$ suivent
	la loi géométrique de paramètre $p\in\ ]0,1[$ ;  on note
	$q=1-p$.

	     1. Calculer l'espérance de $Y_n$.
	     1. Étudier la convergence en loi de la suite $(Y_n)_{n\inℕ^*}$.

	3. Démontrer que la série de terme général $ℙ([X_1\geqslant k])$ converge si et seulement
		si
		$X_1$ admet une espérance et qu'alors :

	    $$
		\mathbb{E}(X_1)=\displaystyle\sum_{k=1}^{+\infty} ℙ([X_1\geqslant k])
		$$

	4. En déduire que   si $X_1$ admet une espérance,  alors $Y_n$
	admet une espérance et comparer ces deux espérances.
	
	5. Soit $N$ une variable aléatoire à  valeurs dans $ℕ^*$
 	   indépendante des $X_i$, et soit $Y$ la variable aléatoire
	   définie par : 
	   
	    $$
	    \forall\,\omega\in\Omega ,Y(\omega)=\underset{1≤ i≤N(\omega)}{\min} X_i(\omega)
	    $$

	   Démontrer que si
	   $X_1$ admet une espérance, alors $Y$ admet une espérance et que l'on a
	   $\mathbb{E}(Y)\leq \mathbb{E}(X_1)$.




!!! {{ exercice("Scilab")}}

	1.  Si $x = (x_ 1 , \dots , x_ n ) \inℝ^n$ , on appelle vecteur des moyennes cumulées de $x$ le vecteur

	    $$
		\left(x_ 1,\frac{x_ 1 + x_ 2}2,\frac{ x_ 1 + x_ 2 + x_ 3}3,~\dots~,\frac{x_1+\dots+x_n}{n}\right)
		$$

	    Écrire une fonction d'entête `cummoy(v)` qui prend en entrée un
	    vecteur  `v`, et  qui  renvoie un  vecteur  contenant les  moyennes
	    cumulées des coordonnées de `v`. 
		
	1. 
		1.  Écrire un programme qui demande à  l’utilisateur un réel $p ∈ ]0, 1[$ et
			un entier naturel $n$ non nul,qui simule $n$ réalisations $x_1 , \dots ,
			x_ n$  de variables aléatoires de  loi de Bernoulli de  paramètre $p$ et
			qui  représente graphiquement  $\frac{x_  1  + \dots  +  x_ k}k$  en
			fonction de $k$ pour $k$ variant de $1$ à $n$.  

	    1.  Tester ce programme pour différentes valeurs de $p$ et de $n$. Commenter.
	
	1.  Recommencer  la question précédente  en remplaçant la loi  de Bernouilli
	    par une loi Binômiale, par une  loi géométrique, une loi de Poisson, une
	    loi exponentielle puis une loi normale. Commenter. 


!!! {{ exercice("Scilab")}}

	On considère deux variables aléatoires indépendantes  $X$ et $Y$ de même loi
	de Poisson de paramètre $λ$. 

	1.  Écrire une fonction Scilab  d’en-tête function `r = estime(lambda)`
	    qui simule un grand nombre de fois les variables $X$ et $Y$ , et renvoie
	    une estimation de $ℙ ([X = Y ])$.

	    On rappelle  que l’instruction `grand(1,1,’poi’,lambda)`  simule la
	    loi de Poisson de paramètre `lambda`. 

	1.   Grâce à  la fonction  précédente,  on trace,  en fonction  de $λ$,  une
	     estimation de $\sqrt{λ\pi}ℙ ([X = Y ])$ pour $λ ∈]0; 20]$. 

	     À  la vue  de ce  graphe, proposer  un équivalent  de $ℙ  ([X =  Y ])$
	     lorsque $λ$ tend vers $+\infty$. 
		 
		 


!!! {{ exercice("Scilab : convergence de lois binomiales vers une loi de Poisson")}}

    Nous avons démontré, dans le chapitre sur les équivalents que, si
	$X_n\leadsto {\cal B}(n,\frac{\lambda}{n})$, alors pour tout $k\inℕ$,
	$ \displaystyle\lim_{n\to+\infty}ℙ([X_n=k])={\rm e}^{-\lambda}\frac{\lambda^k}{k!}$, ce qui laisse
	voir un lien entre la loi binômiale et  la loi de Poisson de paramètre $np$ pour
	les grandes valeurs de $n$. 



	1.  Recopier,  exécuter puis commenter (au  sens décrire leur rôle)  le code
	    ci-dessous (pour vous souvenir de ce que fait chaque commande). 
		*N'hésitez pas à commenter (au sens désactiver) certaines lignes
		avec `//` pour mieux observer leur rôle* 
		
		```scilab
	    clf()
    	n = 1000
    	p = 0.01
	    lam = n*p
		u = grand(1,10000,'bin',n,p)
		v = grand(1,10000,'poi',lam)
		U = tabul(u,"i")
		V = tabul(v,"i")
		plot2d3(U(:,1), U(:,2), 4)
		e = gce();
		e.children.line_style = 1;
		e.children.thickness = 3;
		plot2d3(V(:,1), V(:,2))
		xtitle("Comparaison Binômiale/Poisson")
		```
	
	1.  Tester  avec différentes valeurs  de $n$, en commençant  par $n=10$,
		    puis $n=100$... et le paramètre $p_n$ correspondant. 
			*Vous êtes  invités à  exécuter plusieurs fois  le code  pour chaque
			réglage, afin de vous convaincre  que l'observation n'est pas due au
			hasard.* 

	1.   Quelles sont  les valeurs  pour  lesquelles l'approximation  de la  loi
	    binomiale par la loi de Poisson semble 
		pertinente ? 

	1.  Redémontrer que : si $X_n\leadsto{\cal B}(n,\frac{\lambda}{n})$, alors
	    pour tout $k\inℕ$
		
		$$
		\displaystyle\lim_{n\to+\infty}ℙ([X_n=k])={\rm e}^{-\lambda}\frac{\lambda^k}{k!}
		$$

		 
		 
!!! {{ exercice("Scilab :  convergence de lois binomiales vers une loi normale")}}



	1.  Reprendre le code précédent en l'adaptant pour obtenir un échantillon de
	    taille 10~000 de $X_n^\ast$, VAR  centrée réduite de $X_n$ suivant une
	    loi binômiale de paramètres $(n,p)$.  On créera également un échantillon
	    de $Z\leadsto\mathcal{N}(0,1)$. 
	1.  Tester le code avec différentes valeurs de $n$ et $p$.
	1.  Que constate-t-on ? Pour quelles valeurs de $n$ et $p$ peut-on approcher
	    $X_n^*$ par $Z$ ?  Que se passe-t-il lorsque $n$ est  trop petit, ou $p$
	    trop grand ou trop petit ?
		
	1. Reprendre l'exercice précédent avec une loi de Poisson centrée réduite.
