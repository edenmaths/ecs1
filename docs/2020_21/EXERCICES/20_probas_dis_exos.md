---
hide:
  #- navigation # Hide navigation
  #- toc # Hide table of contents
---



{{ chapitre(20, "Espaces probabilisés discrets : exercices", 16)}}


### Culture

!!! {{ exercice("Probabiliités, « experts » et journalistes")}}

	   Le        3         juin        2011,        est         paru        dans
	[Libération](https://www.liberation.fr/france/2011/06/03/accident-nucleaire-une-certitude-statistique_740208/) 
	un article  signé par Bernard  Laponche ,  physicien nucléaire et  expert en
	politique de l'énergie et de Benjamin Dessus , ingénieur et économiste. Du solide quoi !
	
	
	*La probabilité d'occurrence  d'un accident majeur sur ces  parcs serait donc
	de 50% pour la France et de 
	plus de 100% pour l'Union européenne.*
	
	
	Bon, évidement, quand un mathématicien  lit qu'une probabilité dépasse 100%,
	cela le laisse perplexe...Étienne 
	Ghys  , grand  mathématicien français  qui a  lu l'article, [a relevé  cette
	bourde](https://images.math.cnrs.fr/Accident-nucleaire-une-certitude-statistique.html)
	dans « Image des mathématiques » 
	le lendemain. 
	
	L'article  commence par  estimer  la probabilité  d'un  accident majeur  par
	réacteur nucléaire et par année de 
	fonctionnement. Selon l'article, le parc  mondial actuel de réacteurs cumule
	14000 réacteurs-ans (environ 450 
	réacteurs pendant 31 ans). Pendant cette période, il y a eu quatre accidents
	majeurs, ce qui mène à une 
	probabilité d'accident majeur d'environ 0,0003 par an pour chaque réacteur.
	Les auteurs en  « déduisent »  donc que la probabilité d'un accident majeur
	en France (avec ses 58 réacteurs) 
	pendant les trente prochaines années serait  de 58 fois 30 fois 0,0003, donc
	d'environ 50%. Quant à la probabilité 
	d'un accident en Europe ( 143  réacteurs) dans les trente prochaines années,
	elle  « est » de 143 fois 30 fois 
	0,0003,  « donc »  d'environ 129%.  Comment une  probabilité pourrait-elle
	dépasser 100% ? Plus sûr que la 
	certitude ? Les  auteurs ont sans doute mauvaise conscience  d'écrire que la
	probabilité est de 129% alors ils se 
	contentent d'écrire qu'elle est de « plus de 100% »... Ils concluent « La
	réalité, c'est que le risque d'accident 
	majeur en Europe n'est pas très improbable, mais au contraire une certitude statistique. »
	Quand on voit leur CV...
	
	**Bernard Laponche** : *polytechnicien, docteur ès sciences et en économie de
	l'énergie, il a été ingénieur 
	au Commissariat à l'énergie atomique (CEA), responsable syndical à la CFDT, puis directeur général
	de l'Agence Française pour la Maîtrise de l'Énergie (AFME) dans les années 80. Co-fondateur avec
	Florence Rosenstiel et directeur du bureau d'étude ICE (International Conseil Energie) de 1988 à
	1998, il a été conseiller pour l'énergie et la sûreté nucléaire auprès de la
	Ministre de l'aménagement 
	du territoire et de l'environnement Dominique Voynet en 1998 et 1999.*
	
	**Benjamin  Dessus**, *ingénieur  et  économiste, a  débuté  sa carrière  aux
	laboratoires de Marcoussis dans le 
	domaine l'électronique quantique et des lasers avant de rejoindre les Etudes
	et Recherches d'Electricité 
	de France pour y monter un laboratoire de métrologie optique. En 1982, à la création de l'AFME
	(Agence Française de la Maîtrise de l'Énergie, devenue ADEME), il prend la direction des services
	techniques de cette agence. Il la quitte en 1987 pour rejoindre le CNRS où il assumera jusqu'en 2001
	la direction de plusieurs programmes interdisciplinaires de recherche (PIRSEM, Ecotech, ECODEV)
	consacrés aux problèmes d'énergie et d'environnement. Parallèlement, il a contribué à la formulation
	de la stratégie climat du Fonds pour l'Environnement Mondial (FEM) - dont il a fait partie de 1991
	à 1994 du Scientific and Technical Advisory Pannel (alors présidé par Robert Watson) - puis présidé
	de 1994 à 2003 le conseil scientifique et technique du Fonds Français pour l'Environnement Mondial
	(FFEM). Spécialiste reconnu des questions  énergétiques et en particulier du
	nucléaire (rapport Charpin- 
	Dessus-Pellat  relatif  à  l'Étude  économique  prospective  de  la  filière
	électrique nucléaire en juillet 2000).* 
	
	
	Bon, pouvez-vous corriger les calculs de nos experts ?
	
	
	Si l'on garde cette  probabilité de 0,0003 par an et  par réacteur de tomber
	en panne (mais on peut vraiment en discuter), 
	quelle  est  la  probabilité  qu'il  n'y ait  pas  d'accident  dans  les  30
	prochaines années sur les 143 réactuers européens. 
	On fera  beaucoup d'hypothèses :  on suppose que la  loi suivie est  une loi
	binomiale, qu'il y a indépendance entre les 
	réacteurs et les années.
	
	Qu'ont calculé les « experts » en fait ?

    Prenons  un autre  exemple  moins  discutable :  quelle  est la  probabilité
	d'avoir au moins une fille dans une famille de 
    quatre enfants ? Nos  experts auraient dit : « proba  d'avoir une fille 0,5
	donc $4×0,5 = 2$ donc la probabilité vaut 2 ». 
    Qu'en pensez-vous ? 
	
    Autre  exemple  :  Google  a  le  monopole  de  la  sécurité  des  centrales
	nucléaires. Son programme commet une erreur 1 
    fois sur 100. Il y a 450 réacteurs dans le monde : quelle est la probabilité
	qu'il n'y ait aucun accident. Quelle est l'espérance du nombre d'accidents?



### Échauffement


!!! {{ exercice()}}

    
    Soit X une VAR  telle que $X(\Omega)=ℕ^\star$ et $ℙ{}([X=n])=\frac{\lambda}{n!}$.

	- Déterminez $\lambda$.
    - Calculer $ \mathbb{E}(X)$.
	- Calculer $ \mathbb{E} \left(\frac{1}{1+X}\right)$

	


!!! {{ exercice()}}
    
	
	=== "Énoncé"
	
	     Écrire à l'aide des opérations ensemblistes $∩$, $∪$, et $\overline{\cdot}$
	     et à l'aide des événements $A,\ B, \ C,\dots,A_1,\dots,A_n,\dots$ les
	     événements suivants:
	
		 1. L'un au moins des événements $A$, $B$, $C$ est réalisé.
		 2. L'un et l'un seulement des événements $A$ ou $B$ se réalise.
		 3. $A$ et $B$ se réalisent mais pas $C$.
		 4. Tous les événements $A_n$, $n∈ℕ^*$ se réalisent.
		 5. Il y a une infinité des événements $A_n$, avec $n∈ℕ^*$, qui se réalisent.
		 6. Un nombre fini des événements $A_n$, avec $n∈ℕ^*$, se réalise.
		 5.  Il y  a une  infinité  des événements  $A_n$,  avec $n∈ℕ^*$,  qui ne  se
		 réalisent pas
		 5.  Tous les  événements $A_n$,  avec $n∈ℕ^*$,  se réalisent  à partir  d'un
		 certain rang.

    === "Indication"
	
	     Pour  la 5.  : au-delà  de tout  rang $p  ≥1$, il  y a  un au  moins un
	     événement $A_n$ qui se réalise.
		 
		 
!!! {{ exercice()}} 

	<!-- II-9 PUF -->
    
	Une  étude  sérieuse  vient  de   prouver  que  le  nombre  d'étudiants
	footballeurs ou adeptes de la trottinette arrivant
	en salle 506 entre 8h00 et 8h15 les jeudis suit une loi de Poisson de
	paramètre $\lambda =3.$ 
	
	- Quel est, en moyenne, le nombre de ces étudiants qui arrivent entre 8h00 et 8h15 ?

	- Calculer la probabilité, qu'entre 8h00 et 8h15 aucun de ces étudiants ne se présente en 506.

	- Calculer la probabilité, qu'entre 8h00 et 8h15 trois de ces étudiants se présentent en 506.

	- Calculer la probabilité, qu'entre 8h00 et 8h15 plus de 4 de ces étudiants se présentent en 506.

	- Calculer la probabilité, qu'entre 8h00 et 8h15 au moins un de ces étudiants se présente en 506.





!!! {{ exercice()}}

    === "Énoncé"

        Soit un réel $C$.  On pose, pour tout $ n \in \mathbb{Z} \setminus \left\{ 0 , -1 \right\} $, $ u_n = \dfrac C{n(n+1)} $. 
		
        1. Montrer qu'il existe une unique valeur de $C$ telle que la suite $u$ soit la loi d'une variable aléatoire réelle discrète $X$.
        2.  Déterminer la loi de $ Y = | X |$.

	=== "Indications"

         bientôt







!!! {{ exercice()}}

    === "Énoncé"

          On lance indéfiniment une pièce truquée de façon à ce que la probabilité d'obtenir pile soit $\frac23$. On note $X$ le nombre de lancers nécessaires à l'obtention, pour la première fois de deux « Piles » consécutifs, et pour tout $n\inℕ^*$, on note $a_n=ℙ([X=n])$. 
		  
          1. Calculer $a_1$, $a_2$ et $a_3$.
          2. Montrer : $\forall n\geqslant 3,\quad a_{n+2} =\frac13 a_{n+1} + \frac29 a_n$
          3. En déduire la loi de $X$.  Vérifier  $ \displaystyle\sum_{n=1}^{+\infty}ℙ([X=n])=1$
          4. $X$ admet-elle une espérance ? Le cas échéant, la calculer.

	=== "Indications"

         Pour tout $k\inℕ^*$, on note 
	     $F_k$ l'événement « Obtenir Face au tirage n°$k$ »
		
		1. $a_1=0$ car on ne peut avoir deux « Pile » consécutifs dès le premier tirage.
		   $a_2=ℙ(X=2)=ℙ(\overline{F_1}\cap\overline{F_2})=ℙ(\overline{F_1})ℙ(\overline{F_2})=\frac23\times\frac23 = \frac49$ par indépendance des tirages.
		   L'événement $[X=3]$ est réalisé si les tirages n° 2 et 3 sont Pile, le premier étant nécessairement Face
		   $a_3=ℙ(X=3)=ℙ(F_1\cap\overline{F_2}\cap\overline{F_3})=\frac13\times \frac23\times \frac23=\frac{4}{27}$.
		1. Soit $n\geqslant 3$. $(F_1,\overline{F_1})$ est un système complet d'événement. La formule des probabilités totales donne alors :
		   $
		   ℙ(X=n+2) = ℙ(F_1)ℙ_{F_1}(X=n+2) + ℙ(\overline{F_1})ℙ_{\overline{F_1}}(X=n+2)
		   $
		   Or, si on suppose $F_1$ réalisé, $[X=n+2]$ est réalisé ssi on obtient pour la première fois deux piles consécutifs $(n+1)$ tirages plus tard. Donc $ℙ_{F_1}(X=n+2)=ℙ(X=n+1)=a_{n+1}$
		   Si on suppose $\overline{F_1}$ réalisé, il faut alors obtenir Face puis  obtienir deux Piles consécutifs pour la première $n$ tirages plus tard et 
		   $ℙ_{\overline{F_1}}(X=n+2)=ℙ(\overline{F_2})ℙ(X=n)=\frac23a_n$
		   On en déduit : $ a_{n+2} = \frac13 a_{n+1} + \frac13\times\frac23 a_n$
		   $\forall n\inℕ^*,\quad  a_{n+2} = \frac13 a_{n+1} + \frac29 a_n$
		   *on aurait aussi pu utiliser le s.c.e $(\overline{F}_1\cap F_2,\overline{F_1}\cap\overline{F_2}, F_1 )$*
		1. La suite $(a_n)_{n\inℕ^*}$ est récurrente linéaire d'ordre 2. L'équation caractéristique est $x^2-\frac13x-\frac29$ de racines $-\frac13$ et $\frac23$. 
		   Il existe donc des réels $\alpha$ et $\beta$ tels que, pour tout $n\inℕ$, 
		   $a_n = a\left(-\frac13\right)^n + \beta\left(\frac23\right)^n$.
		   Or, $a_1=0$ et $a_2=\frac49$ donc $\alpha$ et $\beta$ vérifient :
		   $\begin{cases}
		   -\frac13\alpha + \frac23\beta = 0 \\
		   \frac19\alpha + \frac49\beta = \frac49 \\
		   \end{cases}$
		   d'où
		   $\alpha = \frac{4}{3}$ et $\beta=\frac23$ (*à détailler éventuellement un peu*, mais surtout vé-ri-fier !)
		   Finalement, pour tout $n\inℕ^*,\quad a_n = \frac{1}{3}\left(4\left(-\frac13\right)^n + 2\left(\frac23\right)^n \right)$
		   $n\inℕ^*,\quad ℙ(X=n) = \frac{1}{3}\left(4\left(-\frac13\right)^n + 2\left(\frac23\right)^n \right)$
		   On a bien une somme de termes généraux de deux séries géométriques convergentes et
		   $ \displaystyle\sum_{n=1}^{+\infty}ℙ(X=n)
		    =\frac43\displaystyle\sum_{n=1}^{+\infty}\left(-\frac13\right)^n 
		    +\frac23\displaystyle\sum_{n=1}^{+\infty}\left(\frac23\right)^n 
		    =\frac43\frac{-\frac13}{1+\dfrac13} 
		    +\frac23\frac{\frac23}{1-\dfrac23}
		    =\frac{-\frac43}{3+1} 
		    +\frac{\frac43}{3-2}
		    =-\frac13 + \frac43
		    =1$
		   On a bien $ \displaystyle\sum_{n=1}^{+\infty}ℙ(X=n)=1$.
		1. $\forall n\inℕ^*,\quad nℙ(X=n)=|\frac43n\left(-\frac13\right)^n + \frac23n\left(\frac23\right)^n| \leqslant  \frac43n\left(\frac13\right)^n + \frac23n\left(\frac23\right)^n$
		   On reconnaît des séries géométriques dérivées convergentes (car $|\frac13|<1$ et $|\frac23|<1$) donc la série $\sum nℙ(X=n)$ converge absolument et $X$ admet une espérance.
           $\mathbb{E}(X) = \frac43\displaystyle\sum_{n=1}^{+\infty}n\left(-\frac13\right)^n
	       +\frac23\displaystyle\sum_{n=1}^{+\infty}n\left(\frac23\right)^n$
	       $\mathbb{E}(X) =  -\frac49\displaystyle\sum_{n=1}^{+\infty}n\left(-\frac13\right)^{n-1}
		   +\frac49\displaystyle\sum_{n=1}^{+\infty}n\left(\frac23\right)^{n-1}$
	       $= -\frac49\frac{1}{\left(1+\frac13\right)^2}
		   +\frac49\frac{1}{\left(1-\frac23\right)^2}$
	       $ =  \frac{-4}{\left(3+1\right)^2}
		   +\frac{4}{\left(3-2\right)^2}$
	       $ = -\frac14 + 4$
	       $ = \frac{15}{4}$




!!! {{ exercice()}}

    === "Énoncé"

        Soit $X$ une variable aléatoire, de loi géométrique de paramètre $p_1$.
		
        1. Calculer $ℙ ( [X > n] )$ pour $n > 1$.
        1.  Montrer que $ℙ_{[ X > n ]} ( [X = n + k] ) = ℙ([ X = k ])$ pour $n, k > 1$. Pourquoi parle-t-on de loi sans mémoire ? 
        1.  On considère $Y$ une variable aléatoire indépendante de $X$, de loi géométrique de paramètre $p_ 2$.  Calculer $ℙ ([ X = k ]∩ [ Y = k ])$ pour $k > 1$.
        1. En déduire $ℙ ([ X = Y] )$ .

	=== "Indications"
	
         bientôt












!!! {{ exercice("Collectionneur")}}
    
	1. Combien faut-il en moyenne de lancers de dés pour obtenir les six faces?

    2. Combien faut-il en moyenne de tirages d'un entier choisi au hasard entre 0
	et 9 pour les obtenir tous ?





### Concours








!!! {{ exercice("ESCP")}}

	<!-- ESCP 2018 3.13 -->
    === "Énoncé"

		Une pièce truquée donne Pile avec la probabilité $p$ avec $0<p<1$ et Face
	    avec la probabilité $q=1-p$.

		On propose l'expérience suivante pour « rééquilibrer » la pièce.

		L'expérience est constituée d'une suite de parties consécutives.


		Chaque partie consiste à lancer la pièce *deux fois de suite.

		- Si à l'issue d'une partie on obtient deux fois le même résultat (2
	      Pile ou 2 Face), on refait une partie; 
        - Si à l'issue d'une partie  on obtient deux résultats différents, alors
	      on arrête l'expérience et on rend le résultat du dernier lancer. 


        L'expérience est modélisée à l'aide d'un espace probabilisé $(\Omega,\mathcal{T}_Ω , ℙ)$.

	    Soit $T$ la variable aléatoire égale au nombre de lancers nécessaires pour
	    que l'expérience se termine. 
	  

        1. Écrire un script `Scilab` qui simule la variable aléatoire $T$.
	    2. Donner l'ensemble des valeurs prises par $T$. En déduire $ℙ([T=2k+1])$
	       pour $k\in ℕ$.
	    3.
	        1. Calculer $ℙ([T=2k])$, pour $k\in ℕ$ (on pourra s'intéresser à
	           l'événement $A=[X_1=X_2]$, où $X_1, X_2$ sont les 
	           deux variables aléatoires donnant les résultats des deux premiers lancers).
	        2. En déduire que l'expérience se termine presque sûrement.
	        3. Calculer l'espérance de $T$.
	    4. Soit $R$ la variable aléatoire donnant le résultat de l'expérience. Donner la loi de $R$.

	=== "Indications"

         bientôt...


!!! {{ exercice("ECRICOME Eco")}}
    <!-- ECRICOME eco 2015-->
    Dans tout cet exercice, $N$ désigne un entier naturel supérieur ou égal à $3$. 
	
	On dispose de deux urnes opaques $U_1$ et $U_2$, d'apparence identique et contenant chacune $N$ boules indiscernables au toucher. 
	
	L'urne $U_1$ contient $(N-1)$ boules blanches et une boule noire. 
	
	L'urne $U_2$ contient $N$ boules blanches.

	##### I -  Une première expérience aléatoire

	On effectue des tirages **sans remise** dans l'urne $U_1$, jusqu'à l'obtention de la boule noire.  
	On note $X$ la variable aléatoire qui prend pour valeur le nombre de tirages
	nécessaires pour l'obtention de la boule noire. 
	On notera pour tout entier naturel $i$ non nul :
	- $N_i$ l'événement « on tire une boule noire lors du $i$-ième tirage ».
	- $B_i$ l'événement « on tire une boule blanche lors du $i$-ième tirage ».

    1. On simule $10000$ fois cette expérience aléatoire. 
	   Recopier  et compléter  le programme  SCILAB suivant  pour qu'il  affiche
	   l'histogramme donnant  la fréquence d'apparition  du rang d'obtention  de la
	   boule noire: 
	   ```scilab
       N = input(' Donner un entier naturel non nul') ;
	   S = zeros(1,N);
	   for k = 1 : 10000
	     	i = 1 ;
		    M = N ;
		    while ________________
			   i = i + 1 ;
			   M = ________________ ;
		    end
		    S(i) = S(i) + 1 ;
	   end
	   disp(S / 10000)
	   bar(S / 10000)
	   ```
    1. On exécute le programme complété ci-dessus. On entre $5$ au clavier et on
	   obtient l'histogramme suivant :
       ![Placeholder](./IMG/sujetE12015_fig03.png){ align=right  width=400}
	   Quelle conjecture  pouvez-vous émettre sur  la loi de la  variable aléatoire
	   $X$ ?
	   Pour les questions suivantes, on revient au cas général où $N \geqslant 3$.
    1. En écrivant soigneusement les événements utilisés, calculer $P(X=1)$, $P(X=2)$ et $P(X=3)$.
	1. Déterminer la loi de la variable aléatoire $X$.
	1. Préciser le nombre moyen de tirages nécessaires à l'obtention de la boule noire.


	##### II - Une deuxième expérience aléatoire

	On choisit une des  deux urnes au hasard (chaque urne  a la même probabilité
	d'être  choisie) et  on tire  dans  l'urne choisie  une par  une les  boules
	**sans  remise** jusqu'à  être en  mesure de  pouvoir connaître  l'urne
	choisie. 
	
	On note $Y$ la variable aléatoire qui prend pour valeur le nombre
	de tirages ainsi effectués. 
	On note :
	
	- $C_1$ l'événement « on choisit l'urne $U_1$ ».
	- $C_2$ l'événement « on choisit l'urne $U_2$ ».

        1. Montrer que pour tout entier $j \in [\![ 1, N ]\!]$
	       $
		   ℙ_{C_1}(Y=j) = \frac{1}{N}
		   $
		   
	    2. Calculer $ℙ_{C_2}(Y=j)$ pour tout entier $j \in [\![ 1, N ]\!]$.
		   *(On distinguera les cas $j=N$ et $1 \leqslant j \leqslant N-1$)*.
	    3. Montrer que 
		  $
		  ℙ([Y=j])  =  \left\{  \begin{array}{ll} \displaystyle  \frac{1}{2N}  & \text{ si } j \in [\![ 1, N-1 ]\!] \\ \displaystyle \frac{1}{2} + \frac{1}{2N} & \text{ si } j = N \end{array} \right. 
	      %\}$
		  
	      4. Calculer l'espérance de $Y$.

	##### III - Une troisième expérience aléatoire

	On effectue une succession infinie de tirages \textbf{avec remise} dans l'urne $U_1$. On admet qu'on obtient presque-sûrement au moins une boule blanche et au moins une boule noire lors de ces tirages. 
	
	On note $T$ la variable aléatoire prenant pour valeur le nombre de tirages nécessaires jusqu'à l'obtention d'au moins une boule noire et d'au moins une boule blanche. 
	
	On note $U$ la variable aléatoire prenant pour valeur le nombre de boules blanches tirées jusqu'à l'obtention d'au moins une boule noire et d'au moins une boule blanche. 
	
	Par exemple, si les tirages ont donné successivement : noire, noire, noire, blanche, blanche, noire, alors $T=4$ et $U=1$.
	
	   1. Préciser les valeurs prises par $T$.
	   2. Montrer soigneusement que pour tout entier $k \geqslant 2$,
	      $
		   ℙ(T=k) = \frac{1}{N} \left( \frac{N-1}{N} \right)^{k-1} + \frac{N-1}{N} \left( \frac{1}{N} \right)^{k-1}
	      $
	   3. Montrer que la variable aléatoire $T$ admet une espérance que l'on calculera.
	   4.
	      1. Calculer $ℙ([U=1] \cap [T=2])$.
	      2.  Calculer $ℙ([U=1] \cap [T=k])$ pour tout entier $k \geqslant 3$.
	   5. Soit $j$ un entier tel que $j \geqslant 2$.
	      1. Calculer $ℙ([U=j] \cap [T=j+1])$.
	      2. Que vaut $ℙ([U=j]\cap [T=k])$ pour tout entier $k \geqslant 2$ tel que $k \neq j+1$~?
	   6. Les variables aléatoires $T$ et $U$ sont-elles indépendantes~?
	   7.  Calculer $ℙ([U=1])$ puis déterminer la loi de $U$.



!!! {{ exercice("ESCP")}}

	<!-- ESCP 2018 3.13 -->
    === "Énoncé"

		 *On rappelle les résultats suivants :*

		 (i)  Soit $I$ un ensemble dénombrable infini indexé par $ℕ$ sous la forme
		 $\, I = \{ φ(n)\,,\, n \in ℕ \} \,$ où $φ$ est une bijection
		 de $ℕ$ dans $I$. Si la série $ \displaystyle\sum u_{φ(n)}$ converge absolument, alors sa somme
		 est indépendante de l'indexation $φ$, et pourra également être notée
		 $\, \displaystyle  \sum_{i \in I}  u_i \,$. On  dit alors que  la série
	     $\displaystyle\sum_{i \in I} u_i$ converge absolument.
		 
		 (ii)  Dans ce cas, si $\displaystyle I = \bigcup_{j \in J} I_j $ (union disjointe)
		 avec $J$ un ensemble dénombrable et $I_j$ des ensembles dénombrables pour
		 tout $j$, alors pour tout $j$, $\displaystyle \sum_{k \in I_j} u_k$ converge absolument, et
		 $
		   \displaystyle\sum_{i \in I} u_i = \sum_{j\in J} \left[ \sum_{k \in I_j} u_k \right]
	     $
		 
		 (iii) Si $I$ et $J$ sont des ensembles dénombrables et si
		 $\displaystyle \sum_{i  \in I}  u_i$ et  $\displaystyle \sum_{j  \in J}
	     v_j$ sont absolument convergentes, 
		 alors $\displaystyle \sum_{(i,j) \in I \times J} \bigl( u_i\,v_j \bigr)$ aussi, et
		 $
		 \displaystyle\left( \sum_{i \in I} u_i \right) \left( \sum_{j \in J} v_j \right) =
			\sum_{(i,j) \in I \times J} \bigl( u_i\,v_j \bigr) 
		 $
		 
		 On prendra soin de justifier clairement, à l'aide de ces résultats, les calculs
		 de sommes de séries qu'on sera amené à faire ci-dessous.

		 Soit $p$ et $q$ deux réels de l'intervalle $%[$
		 $\, \left]0,1\right[ \,$. $%]$
		 

	    1.  Vérifier que: $ \forall (i,j) \in ℕ^2,\, ℙ\bigl[ (i,j) \bigr] =
	         p\,q\,(1-p)^i\,(1-q)^j $ définit bien une probabilité $ℙ$ sur $ℕ^2$. 
        2.
		    1. Déterminer les lois des variables aléatoires discrètes $X$ et $Y$ définies sur
		        $\, \bigl( ℕ^2, {\mathfrak P}(ℕ^2),ℙ \bigr) \,$ par
		        
			    $$ 
				\forall (i,j) \in ℕ^2,\quad X(i,j) = i \quad\hbox{et}\quad
	            Y(i,j) = j 
	            $$
		        
				et les relier à des lois connues.
	        2. Calculer $ ℙ(X=Y)$ et $ ℙ(X>Y) $.

		 
		 3. Soit $Z$ la variable aléatoire discrète définie par:
		     
		    $$
			 \forall (i,j) \in ℕ^2,\quad Z(i,j) = \begin{cases} \phantom{-}1 \text{  si } i \text{ et } j \text{ sont pairs}\\ -1 \text{ si } i \text{ et } j \text{  sont impairs}\\  \phantom{-}  0 \text{  si  } i  \text{  et }  j \text{ sont de parités différentes} \end{cases}
		    $$
			 
		     Montrer que $Z$ admet une espérance et la calculer.
		 
		 4. Soit $D$ l'ensemble défini par $\, D = \bigl\{ (i,i)\,,\, i \in ℕ \bigr\} \,$.
		    Justifier que la série $\, \displaystyle \sum_{(i,i) \in D} Z(i,i)\,ℙ(i,i) \,$
		    est absolument convergente et calculer sa somme.


	=== "Indications"

         bientôt...





!!! {{ exercice("Approximation de la loi binomiale par la loi de Poisson")}}


    On considère  une suite de v.a.r.   $X_n$ ayant une loi  de probabilité
	binomiale de paramètres $n$ et $\lambda/n$. Montrez que:

	$$
	\displaystyle\lim_{n\to +\infty}ℙ([X_n=k])=\frac{\lambda^k}{k!}\mathrm{e}^{-\lambda}
	$$
		 
	 On dit alors que la suite de variables aléatoires $X_n$ converge en loi
     (On étudiera  cela en  fin de  semestre) vers  une v.a.r.  X de  loi de
	 Poisson de paramètre $\lambda$. 

	 Déduisez-en   une   approximation  de   $\binom{n}{k}p^k(1-p)^{n-k}$
	 lorsque $n>>1$.

	 ![Placeholder](./IMG/costa.png){ align=right  width=200}
	 **Application: la fusion amoindrit les risques**

	 Deux  sociétés assurent  des  biens  très chers  mais  dont le  risque
	 d'accident est très faible: vaut-il mieux fusionner ou bien les risques
	 s'additionnent-ils?

	 Prenons le cas d'une société d'assurance sicilienne ayant pour client la société
	 des croisières Costa.


	 - Supposons que la société Costa possède 500 navires dont la valeur
	   est de 5 millions de
	   $\mathrm{Schpz\check{r}s\mathring{w}}$.  L'assureur rembourse la
	   perte totale d'un navire, évènement dont la probabilité est
	   estimée à 0,001 pour une année.  On suppose que les risques de
	   pertes des navires sont indépendants.  Donner la loi de X égale au
	   nombre de navires perdus au cours de l'année.
	 - L'assureur rembourse son client une fois par an. Combien doit-il
	   avoir dans son coffre pour qu'il puisse rembourser son client avec
	   une probabilité de 0,999.  On utilisera l'approximation du
	   préambule et on utilisera une table de la loi de Poisson.
	 - Une autre compagnie assure les 500 navires de la White Star dans
	   les mêmes conditions.  Les deux compagnies décident de fusionner
	   suite au suicide du président de la première compagnie dont on a
	   retrouvé le corps percé de 52 coups de couteaux.  La compagnie
	   d'assurance prend donc en charge à présent 1000 navires. Refaire
	   les calculs de la question précédente dans ces
	   conditions. Commentaires?



!!! {{ exercice("ESSEC")}}

      **Etude des séries dérivées de la série géométrique.**


	 Dans toute cette question, on désigne par $x$ un nombre réel $x$ tel
	 que $0\leq x<1$.

	 1. Calculer pour tout nombre entier naturel $n$ les deux sommes suivantes: 

	    $$
	    \displaystyle\sum_{k=0}^{n}x^{k}\qquad \text{et}\qquad \sum_{k=1}^{n}kx^{k-1}
	    $$

	 1. Déterminer la limite de $x^{n}$ et de $nx^{n}$, et des deux
	    sommes précédentes quand $n$ tend vers $+\infty $.

        *On admettra alors qu'il est licite, pour $0\leq x<1$ de dériver
	    terme à terme l'égalité classique:*

	    $$
	    \displaystyle\sum_{m=0}^{+\infty }x^{m}=\dfrac{1}{1-x}
	    $$

	    *autrement dit, que l'on a pour tout nombre entier naturel non nul $k$ la
	     relation suivante $(R)$:* 

	    $$
	    \displaystyle\sum_{m=k}^{+\infty }m(m-1)\ldots (m-k+1)x^{m-k}=\dfrac{d^{k}}{dx^{k}}\left( 
	    \dfrac{1}{1-x}\right) 
	    $$


	 1.  Exprimer ainsi $\dfrac{1}{(1-x)^{3}}$ sous la forme d'une série.

	 1.  Expliciter la dérivée $k^{i\grave{e}me}$ de la fonction $%
	     x\mapsto \dfrac{1}{(1-x)}$.

     	 Effectuer dans la relation $(R)$ le changement d'indice $n=m-k$ et
	     déduire de ces résultats l'expression de 
		 $\sum\limits_{n=0}^{+\infty}\binom{n+k}{k}x^{n}$ en fonction de $k$ et $x$.


	 **Application à l'étude de la loi binomiale négative.**

	 On considére une suite d'épreuves de Bernoulli identiques,
	 indépendantes et menant au succés avec la probabilité $p$ ($0<p<1
	 $). 

	 Pour tout nombre entier $k\geq 1$, on désigne par $X_{k}$ la variable
	 aléatoire indiquant le numéro de l'épreuve où intervient le $%
	 k$-iéme succés (et $X_{k}$ prend donc des valeurs supérieures ou
	 égales à $k$).


	 1.  On suppose $k=1$. Préciser la loi de $X_{1}$, la probabilité $
	     ℙ(X_{1}=n+1)$ pour tout nombre entier naturel $n$ et l'espérance $
	     \mathbb{E}(X_1)$ de la variable aléatoire $X_{1}$.

	 1.  On suppose $k>1$. Déterminer la probabilité d'obtenir $k-1$
	     succés en $n+k-1$ épreuves, puis en déduire la probabilité $
	     ℙ(X_{k}=n+k)$ pour tout nombre entier naturel $n$.

	 1.  À l'aide des résultats précédents, vérifier que la
	     série $ \displaystyle\sum ℙ(X_{k}=n+k)$ a pour somme $1$, puis calculer
	     l'espérance $\mathbb{E}(X_{k})$ de la variable aléatoire $X_{k}$ en fonction
	     de $p$ et $k$. 

	     Comment peut-on interpréter ce dernier résultat?

	 *On dit alors que la variable aléatoire $X_{k}$ suit la loi
	 binomiale négative de paramétres $p$ et $k$.*



### Scilab


!!! {{ exercice()}}

    Compléter  la fonction  pour qu'elle  simule un  tirage d'une  loi binômiale
	$\mathcal{B}(n;p)$ en utilisant `rand`: 
	
	```scilab
	function tirage = binomiale(n,p)
 	...
	endfunction
	```
	
	
!!! {{exercice()}}
    
	À l'aide de la fonction `binomiale` précédente, on lance le script suivant:
	
	```scilab
	N = 100
	n = 10
	p = 0.3
	liste = zeros(1,n+1)
	for k = 1:N
		r = binomiale(n,p)
		liste(r+1) = liste(r+1) + 1
	end
	clf();bar(1:(n+1),liste)    
    ```
	
	Que représentent `N`, `n`, `p`, `liste` ? Que fait `bar` ?
	
	Modifier le code pour que le graphique représente la loi binômiale, c'est à
	dire que la hauteur des barres soit la fréquence d'apparition de la valeur. 
	
	Modifier  alors la  valeur  de  $p$. Quel  impact  sur  la distribution  des
	fréquences   ?   Quelle   interprétation  en   termes   usuels   (espérance,
	écart-type...) 
	
	Fixer  $p$  à  $\frac12$  puis  faire  varier  $n$.  Quel  impact  ?  Quelle
	interprétation.
	
	
!!! {{exercice()}}
    
	On répète indéfiniment  et indépendemment, une même épreuve  de Bernoulli de
	paramètre $p$. On  note $X$ la variable aléatoire qui,  à une issue, associe
	le rang d'apparition du premier succès. 
	
	Compléter la fonction pour qu'elle renvoie  la valeur prise par $X$ dans une
	simulation en utilisant `rand`:
	
	```scilab
	function tirage = geom(p)
 	... 
	endfunction 
	```
	
	Tester la  fonction pour $p=0,\!1$.  Le résultat renvoyé est-il  crédible (à
	combien de  tirages peut-on  s'attendre en moyenne  pour obtenir  le premier
	succès ?) 
	
	Dans le même fichier, taper le code suivant :
	
	```scilab
	liste = []
	for i=1:200
		liste = [liste, geom(0.2)]
	end
	```
	
	Que fait-il ?  À quoi sert-il ?  La  moyenne  des   valeurs  obtenues
	correspond-elle à la prévision précédente ? 
	
	 Ajouter à la fin du code `#!scilab clf(); histplot(10,liste)`. Faire tourner le
	programme plusieurs fois. Puis modifier en `#!scilab clf(); histplot(50,liste)` et
	tester plusieurs fois. Que fait la commande `#!scilab histplot` ? 
	
	
	Modifier la boucle du code ci-dessus dans **Scinotes** avec `#!scilab for i=1:5000`
	puis reprendre les commandes ci-dessus. Que se passe-t-il ?
	
	
	
!!! {{exercice()}}

     Écrire une fonction  `kpile` qui prend en entrée un  réel $0<p<1$
	 et un entier $k ∈ ℕ^∗$ et qui simule l’expérience aléatoire et renvoie le
	 temps d’attente du $k$-ième pile lors de lancers successifs d’une pièce
	 truquée de sorte que Pile apparaît avec la probabilité $p$.
	
	 On réalise 10000  fois cette expérience.  Calculer une  valeur approchée du
	 nombre moyen de lancers réalisés à chaque expérience pour $k=10$ et
	 $p=0,3$. Exécuter plusieurs fois le programme. 
	
	 Afficher un diagramme des résultats obtenus pour $k=10$ et $p=0,7$.


!!! {{exercice()}}
	
	On lance une infinité de fois une pièce équilibrée de façon indépendance. La
	suite de variables aléatoires $(S_ n )_{ n∈ℕ}$ définie par:
	$S_ 0 = 0\quad \text{et}\quad\forall n ∈ ℕ,\quad S_{ n+1} − S_n =
	\begin{cases}
	1 \text{ si le } n\text{-ième lancer est tombé sur Pile}\\
	−1 \text{ si le }n\text{- ième lancer est tombé sur Face}
	\end{cases}
	$
	
	$(S_n)$ est appelée marche aléatoire simple symétrique sur $ℤ$.
	
	1. Écrire une fonction `marchealeatoire` qui prend en entrée un entier
	   naturel $n$ et qui construit un vecteur $[S_0 , S_1 , \dots , S_ n ]$
	   correspondant aux $n$ premiers pas d’une marche aléatoire. 
	2. Écrire  un  programme  demandant  une  valeur  de  $n$  et  représentant
	   graphiquement les $n$ premiers pas. On pourra utiliser `plot`.
	3. Écrire une fonction `retourzero`  qui renvoie l’instant en lequel la
	   marche aléatoire revient en 0 pour la première fois, c'est-à-dire le plus
	   petit entier $k$ non nul tel que $S_k = 0$. 
	


!!! {{exercice()}}

    On dispose d’un stock suffisant de  boules rouges et d’une urne qui contient
	$r > 2$ boules rouges et $b >  2$ boules bleues. On tire une boule au hasard
	dans l’urne. Si elle est rouge, on  la remet dans l’urne. Si elle est bleue,
	on la remplace par une rouge et on la remet dans l’urne. On recommence cette
	opération indéfiniment. 
	
	1. Écrire une fonction commençant  par `#!scilab function R = tirage(r,b,n)`
	   qui simule $n$ tirages successifs avec cette règle et renvoie le vecteur `R`
	   dont la valeur est $[r_1 ,...,r n]$ où, pour tout $k ∈[\![1,n]\!]$, $r_ k$
	   est le nombre de boules rouges à l’issue du $k$-ème tirage. 
	2. Écrire un  programme qui  simule $N=10$  fois l'expérience  avec $r=10$,
	   $b=20$ et $n=100$ et représente dans un même graphique (il suffit de ne pas
	   mettre `#!scilab clf()` dans la boucle) l'évolution du nombre de boules rouges
	   pour chacun des tirages. 
	3.  Écrire  un programme qui demande  un entier $N$ et  les nombres $r,b,n$,
	qui  simule $N$  fois  cette  expérience et  qui  renvoie  un vecteur  $[m_1
	,...,m_n]$ où, pour tout $k\in[\![1,n]\!]$,  $m_k$ est la valeur moyenne des
	valeurs prises par $r_k$. *On pourra utiliser les commandes `#!scilab length()`
	et `#!scilab sum()` qui renvoient respectivement le nombre de composantes et la
	somme des composantes d'un vecteur (ou d'une matrice)*. 
	

!!! {{exercice("EDHEC") }}
    
	Soit $n ∈ ℕ $avec $n > 2$. On lance $n$ fois une pièce donnant « Pile » avec la
    probabilité $0<p<1$ et « Face » sinon. Pour tout entier naturel $k > 2$, on 
    dira que le $k$-ème lancer est un changement s’il amène un résultat différent du
    $(k − 1)$-ème lancer. On note alors $X_n$ la variable aléatoire égale au nombre de
    changements survenus au cours des $n$ lancers.
	
	1. Rappeler comment simuler avec `rand()`une épreuve de bernoulli de
	   paramètre $p$.
	2. Écrire  une fonction  `X =  CountCh(n,p)` qui  simule une  réalisation de
	   l'expérience aléatoire et retourne la valeur prise par $X_n$ 
	3. Ajouter  au code une suite  d’instructions qui affichera un  diagramme en
	   barres affichant le nombre de changements `CountCh(10000,p)` pour $p$
	   allant de 0,1 à 0,9 avec un pas de 0,1. 


