---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 25

**Semaine du 31 mai au 5 juin 2021**



## Cours



### [Développements limités](../../COURS/21_DL_cours/)

Comparaisons de  fonctions - Fonction  négligeable devant une autre  - Fonctions
équivalentes  - Notion  de DL  - Formule  de Taylor-Young  - Linéarité,  produit,
substitution - Application au  calcul de limite - Étude de série  - Lien avec la
continuité et la dérivabilité.

### [Intégrales impropres](../../COURS/21_DL_cours/22_Int_Gen_cours/)




## Questions de cours 

- Convergence des intégrales de Riemann.
- Critère d'intégrabilité de fonctions positives l'une négligeable devant l'autre.
- Tout exemple des exercices 21-16, 21-17 et 21-18 de la [feuille d'exercices](../../EXERCICES/21_DL_exos/)
- Tout exemple des exercices 22-13 et 22-14 de la [feuille d'exercices](../../EXERCICES/22_Int_Gen_exos/)
- Tout extrait du concours blanc.
