---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 26

**Semaine du 7 au 11 juin 2021**



## Cours



### [Intégrales impropres](../../COURS/21_DL_cours/22_Int_Gen_cours/)


### [Variables aléatoires à densité](../../COURS/21_DL_cours/23_Probas_Dens_cours/)




## Questions de cours : toute erreur ⟹ note en-dessous de la moyenne

- Critère d'intégrabilité de fonctions positives l'une négligeable devant l'autre.
- Tout exemple des exercices 22-13 et 22-14 de la [feuille d'exercices](../../EXERCICES/22_Int_Gen_exos/)
- Tout extrait du concours blanc.
- Recherches 23.1/23.2/23.3/23.4/23.5
- $ X\leadsto {\cal U}([0,1]) ⟺ Y = a + (b-a)X \leadsto {\cal U}([a,b])$
- Espérance d'une loi exponentielle
- La loi exponentielle est sans mémoire
- Fonction de répartition et espérance de $X\leadsto {\mathcal N}(μ,σ^2)$ 

