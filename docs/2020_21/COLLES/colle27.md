---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Vingt-septième et dernière colle de première année

**Semaine du 14 au 18 juin 2021**



## Cours





### [Variables aléatoires à densité](../../COURS/23_Probas_Dens_cours/)

### [Probabilités : convergence et approximation](../../COURS/24_conv_prob_cours/)

Inégalité de Markov et de Bienaymé - Convergences en probabilité et en loi - Loi
faible des grands nombres. (Théorème Limite Central non vu).

## Questions de cours : toute erreur ⟹ note en-dessous de la moyenne

- Recherches 23.1/23.2/23.3/23.4/23.5
- $ X\leadsto {\cal U}([0,1]) ⟺ Y = a + (b-a)X \leadsto {\cal U}([a,b])$
- Espérance d'une loi exponentielle
- La loi exponentielle est sans mémoire
- Fonction de répartition et espérance de $X\leadsto {\mathcal N}(μ,σ^2)$ 
- [Inégalité de Markov](https://edenmaths.gitlab.io/ecs1/2020_21/COURS/24_conv_prob_cours.html#inegalite-de)
- [LfGN pour la loi binomiale](https://edenmaths.gitlab.io/ecs1/2020_21/COURS/24_conv_prob_cours.html#loi-faible-des-grands-nombres-pour-la-loi-binomiale)
- [Convergence en loi de VAR discrètes à valeurs dans $ℕ$](https://edenmaths.gitlab.io/ecs1/2020_21/COURS/24_conv_prob_cours.html#cas-ou-x_nsubseteq-n-et-xsubseteq-n)
