---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 24

**Semaine du 17 au 21 mai 2021**



## Cours


### [Espaces probabilisés discrets](../../COURS/20_probas_dis_cours/)

Ensembles dénombrables - Tribus - Espaces probabilisés - Limite monotone - VAR -
Loi d'une VAR - Fonction de répartition - Moments d'une VAR discrète - Espérance -
Moments d'ordre  quelconque - Moments centrés  - VAR fonction d'une  autre VAR -
Loi d'une VAR du type $f(X)$ - Théorème de transfert - Moment d'une transformation
affine de VAR - Loi géométrique - Loi de Poisson.


### [Développements limités](../../COURS/21_DL_cours/)

Comparaisons de  fonctions - Fonction  négligeable devant une autre  - Fonctions
équivalentes  - Notion  de DL  - Formule  de Taylor-Young  - Linéarité,  produit,
substitution - Application au  calcul de limite - Étude de série  - Lien avec la
continuité et la dérivabilité.



## Questions de cours 

- Espérance/Variance de la loi géométrique.
- Espérance/Variance de la loi de Poisson.
- La loi géométrique est sans mémoire.
- Soit $X$  une VAR discrète admettant un moment  à un ordre $s\in\mathbb{N}^*$,
  alors $X$ admet un moment à tout ordre $r≤s$. 
- Lien avec la continuité et la dérivabilité.
-    Nature   de    la   série    de   terme    général   $u_n=\dfrac{1}{n}    -
  \ln\left(1+\dfrac{1}{n} \right)$.
-                                                       $\displaystyle\lim_{x\to
  0}\dfrac{\sqrt{1+x}-\sqrt{1-x}}{\operatorname{e}^x-\operatorname{e}^{x^2}}$
- Tout exemple des exercices 16, 17 et 18 de la [feuille d'exercices](../../EXERCICES/21_DL_exos/)
