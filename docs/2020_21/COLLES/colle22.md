---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 22

**Semaine du 3 au 7 mai 2021**



## Cours



### [Matrices et applications linéaires](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/Poly_AppLin20.pdf)

Matrice d'une  famille - Image d'un  vecteur et produit matriciel  - Application
canoniquement  associée à  une  matrice  - Correspondance  entre  points de  vue
vectoriel  et matriciel  sur  les  homomorphismes -  Rang  d'une  matrice -  Cas
particulier des endomorphismes - Polynômes d'endomorphismes - 

### [Espaces probabilisés discrets](../../COURS/20_probas_dis_cours/)

Ensembles dénombrables - Tribus - Espaces probabilisés - Limite monotone - VAR -
Loi d'une VAR - Fonction de répartition.


## Questions de cours 

- Soit $φ∈\mathcal{L}(ℝ^3)$ l'endomorphisme canoniquement associé à la matrice $M=
    \begin{bmatrix}
      0&1&1\\
      1&0&-1\\
      -1&1&2
    \end{bmatrix}$.

    Démontrer  que $φ$  est un  projecteur. Déterminez  ${\rm Ker}(φ)$  et ${\rm
    Ker}(φ-{\rm Id}_{ℝ^3})$.
	
-  Soit E et F deux $\mathbb{K}$-espaces vectoriels de dimensions finies
    respectives $p$ et  $n$, $\mathcal{B}_E$ une base de  E, $\mathcal{B}_F$ une
    base de F. Soit:
	
    $$
    φ\colon \begin{array}{rll}
              \mathcal{L}(E,F) & \to & \mathfrak{M}_{np}(\mathbb{k})\\
              u & \mapsto & {\rm Mat}_{\mathcal{B}_F,\mathcal{B}_E}(u)
            \end{array}
    $$
	
    Alors $φ$ est un isomorphisme.
	
-  Soit   $Ω$  un   ensemble  et   $A\subseteq  Ω$.   Soit  $   \mathcal{T}_Ω  =
  \bigl\{ ∅,A,\complement_Ω A,Ω\bigr\}$.
  Démontrer que $(Ω, \mathcal{T}_Ω )$ est un espace probabilisable.
  
- Pourquoi n'y a-t-il pas d'équiprobabilité sur un espace dénombrable?
- Soit  $(Ω,\mathcal{T}_Ω ,ℙ)$  un espace  probabilisé et  $X$ une  VAR discrète
  définie sur cet espace.
  Alors $\{ω∈Ω\ \mid\ X(ω) = a\} ∈ \mathscr{T}_Ω$.
