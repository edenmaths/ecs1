---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 23

**Semaine du 10 au 12 mai 2021**



## Cours


### [Espaces probabilisés discrets](../../COURS/20_probas_dis_cours/)

Ensembles dénombrables - Tribus - Espaces probabilisés - Limite monotone - VAR -
Loi d'une VAR - Fonction de répartition - Moments d'une VAR discrète - Espérance -
Moments d'ordre  quelconque - Moments centrés  - VAR fonction d'une  autre VAR -
Loi d'une VAR du type $f(X)$ - Théorème de transfert - Moment d'une transformation
affine de VAR - Loi géométrique - Loi de Poisson.



## Questions de cours 

-  Soit   $Ω$  un   ensemble  et   $A\subseteq  Ω$.   Soit  $   \mathcal{T}_Ω  =
  \bigl\{ ∅,A,\complement_Ω A,Ω\bigr\}$.
  Démontrer que $(Ω, \mathcal{T}_Ω )$ est un espace probabilisable.
- Soit  $(Ω,\mathcal{T}_Ω ,ℙ)$  un espace  probabilisé et  $X$ une  VAR discrète
  définie sur cet espace.  Alors $\{ω∈Ω\ \mid\ X(ω) = a\} ∈ \mathscr{T}_Ω$.
- Espérance/Variance de la loi géométrique.
- Espérance/Variance de la loi de Poisson.
- La loi géométrique est sans mémoire.
- Soit $X$  une VAR discrète admettant un moment  à un ordre $s\in\mathbb{N}^*$,
  alors $X$ admet un moment à tout ordre $r≤s$. 
