{{ chapitre(21, "Développements limités")}}


!!! exo ""
    ![Placeholder](./IMG/young.jpg){ align=right width=250}
	En 1910, l'anglais William Henry YOUNG publie *The Fundamental Theorems of the
	Differential Calculus* dans lequel il  introduit *The Expansion Theorem* que
	nous  appelons  en  France  *Formule  de  Taylor-Young*.  Contrairement  aux
	formules de Taylor vues précédemment, celle-ci sera d'une portée *locale* et
	va  nous  permettre d'avoir  une  approximation  polynomiale *locale*  d'une
	fonction. Ce théorème nous aidera calculer des limites qui jusqu'alors
	nous résistaient. Le chapitre va comporter énormément de résultats et peu de
	démonstrations...
	
	Nous  allons dans  un premier  temps  étendre aux  fonctions numériques  les
	résultats  vus pour  les suites  sur les  notations de  Landau et  la notion
	d'équivalence. La nouveauté sera que nous ne nous contenterons plus détudier
	les  fonctions au  voisinage de  $+\infty$ mais  étendrons les  résultats au
	voisinage de n'importe quel réel.
 	
	

## Comparaison de fonctions d'une variable au voisinage d'un point

Nous passerons assez  rapidement sur ces notions déjà découvertes  dans le cadre
de l'étude des suites numériques.

On  notera $\overline{\mathbb{R}}=\mathbb{R}\cup\{-\infty,  +\infty\}$ et  $\cal
V_{x_0}$ un voisinage de $x_0$.


### Fonction négligeable devant une autre

!!! {{ definition("Fonction négligeable") }}
    Soit $x_0 \in \overline{\mathbb{R}}$, $f$ et $g$ deux fonctions à valeurs réelles,
    définies sur un voisinage $\cal V_{x_0}$ de $x_0$. 
	On dit que $f$ est **négligeable devant** $g$ au voisinage de $x_0$ lorsqu'il existe une
    fonction $ε$ définie sur $\cal V_{x_0}$ telle que 
	
	$$
	\forall x \in {\cal V}_{x_0},\quad f(x) = g(x)×ε(x)\quad {\rm avec } \displaystyle\lim _{x\to x_0}ε(x) = 0
	$$
		
	On note alors $ f(x) \underset{x \to x_0}{=} {\Large \scr o}(g(x)) $ 



Par  exemple, soit  $f$  et  $g$ les  fonctions  définies  sur $\mathbb{R}$  par
$f(x)=x^2$ et $g(x)=x^5$.

- $g(x)=x^3×f(x)$ et $ \displaystyle\lim_{x\to0}x^3=0$ donc $g(x)\underset{x \to
0}{=} {\Large \scr o}(f(x))$ soit encore  $x^5\underset{x \to
0}{=} {\Large \scr o}(x^2)$

- $f(x)=\dfrac{1}{x^3} ×g(x)$ et $ \displaystyle\lim_{x\to+\infty}\dfrac{1}{x^3} =0$ donc $f(x)\underset{x \to
+\infty}{=} {\Large \scr o}(g(x))$ soit encore  $x^2\underset{x \to
+\infty}{=} {\Large \scr o}(x^5)$


Le  théorème suivant  nous  donnera souvent  un moyen  pratique  de définir  une
relation de négligeabilité:

!!! {{ theoreme("critère de négligeabilité")}}

    === "Énoncé"

	    Soit $x_0 \in \overline{\mathbb{R}}$, $f$ et $g$ deux fonctions à valeurs
	    réelles, définies sur un voisinage $\cal V_{x_0}$ de $x_0$. 
		
		Si $g$ ne s'annule pas sur $\cal V_{x_0}$, alors 
		
		$$
		f(x)\underset{x \to x_0}{=} {\Large \scr o}(g(x))   \Longleftrightarrow   \displaystyle\lim_{x\to   x_0}
		\dfrac{f(x)}{g(x)} = 0
		$$

	=== "Indications"

         Deux  implications  à  démontrer.  Pour le  sens  direct,  utiliser  la
         définition  en faisant  attention  à définir  un  autre voisinage  pour
         introduire $ε$.
         Pour le sens réciproque, introduire $ε(x)=\dfrac{f(x)}{g(x)}$. 


On se souviendra en particulier que $ f(x)\underset{x \to x_0}{=} {\Large \scr
o}\left(1\right)⟺ \displaystyle\lim_{x\to x_0}f(x)=0$.


!!! {{ exercice()}}

	Comparer les  fonctions suivantes au voisinage de 0 puis  de $+\infty$
	pour la relation de négligeabilité:

	$$
	f:x\mapsto \dfrac{x}{|\ln(x)|} \quad g:x\mapsto \sqrt{x} \quad h:x\mapsto x^3-x^2
	$$


!!! {{ theoreme("Règles de calcul")}}

	Dans  tout  ce  qui  suit, $x_0∈  \overline{\mathbb{R}}$  et  les  fonctions
	introduites sont définie sur un voisinage $\cal V_{x_0}$ de $x_0$.
	
	1.   **Transitivité**:   $   f(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right) ∧ g(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(h(x)\right) ⟹ f(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(h(x)\right)$
	2. **Produit** $  f_1(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g_1(x)\right) ∧ f_2(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g_2(x)\right) ⟹ f_1(x)×f_2(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g_1(x)×g_2(x)\right)$
	3. **Somme**: $ f_1(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right) ∧ f_2(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right) ⟹ f_1(x)+f_2(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right)$
	4. **Produit par un scalaire**  $f(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right)  ⟹ (∀λ∈ℝ)(λ·f(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right))$
	5.  **Produit par une fonction** $ f(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right)  ⟹ f(x)×h(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)×h(x)\right)$
	5.  **Produit par une fonction bornée** Si $h$ est bornée sur $\cal V_{x_0}$
	    alors $\underset{x \to x_0}{=} {\Large \scr
	   o}\left(g(x)\right)  ⟹ f(x)×h(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right)$
	6. **Valeur absolue**  $f(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(g(x)\right)  ⟹ |f|(x)\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left(|g|(x)\right)$
	7. **Puissance positive** $f(x)\underset{x \to x_0}{=} {\Large \scr
	   o}\left(g(x)\right)  ⟹ (∀α>0)((f(x))^α\underset{x  \to   x_0}{=}   {\Large   \scr
	   o}\left((g(x))^α\right))$ sous couvert de définition des puissances.


Il  n'y  a  plus qu'à  trouver  des  exemples  et  à démontrer  chacune  de  ces
propositions.



!!! {{ theoreme("Croissances comparées")}}

    $$
	\begin{array}{rlll}
	x^\alpha       & \underset{x \to +\infty}{=}& {\Large \scr o}(x^\beta)           & \text{ lorsque } \alpha<\beta , \\
	x^\beta        & \underset{x \to 0}{=}      & {\Large \scr o}(x^\alpha)          & \text{ lorsque } \alpha<\beta , \\
	(\ln x) ^\beta &  \underset{x \to +\infty}{=}& {\Large \scr o}(x^\alpha)          & \text{ lorsque } \alpha>0 \text{ et } \beta>0 , \\
	(\ln|x|)^\beta& \underset{x \to 0}{=}   & {\Large \scr o}(\frac{1}{x^\alpha}) & \text{ lorsque } \alpha>0 \text{ et } \beta>0 , \\
	x^\alpha       &  \underset{x \to +\infty}{=}& {\Large \scr o}(a^x)                & \text{ lorsque } \alpha>0 \text{ et } a > 1 ,   \\
	a^x            &  \underset{x \to +\infty}{=}& {\Large \scr o}(b^x)                & \text{ lorsque } |a| < |b| .
	\end{array}
	$$

Par exemple,  $ \displaystyle\lim  _{\underset{x>0}{x \to  0}} x^\alpha  (\ln x)
^\beta = 0$ car $\displaystyle\lim_{\underset{x>0}{x \to 0}} x^\alpha (\ln x) ^\beta \overset{y=\frac{1}{x}}{=} \displaystyle\lim_{y \to +\infty} \frac{ (-\ln(y))^\beta }{y^\alpha} = 0$





### Fonctions équivalentes

!!! {{ definition("Fonctions équivalentes") }}
    Soit $x_0 \in \overline{\mathbb{R}}$, $f$ et $g$ deux fonctions à valeurs réelles,
    définies sur un voisinage $\cal V_{x_0}$ de $x_0$. 
	On dit que  $f$ et $g$ sont **équivalentes** au  voisinage de $x_0$
	 lorsqu'il existe une
    fonction $φ$ définie sur $\cal V_{x_0}$ telle que 
	
	$$
	\forall x \in {\cal V}_{x_0},\quad f(x) = g(x)×φ(x)\quad {\rm avec } \displaystyle\lim _{x\to x_0}φ(x) = 1
	$$
		
	On note alors $ f(x) \underset{x \to x_0}{\sim} g(x) $ 




!!! {{ theoreme("critère d'équivalence")}}

    === "Énoncé"

	    Soit $x_0 \in \overline{\mathbb{R}}$, $f$ et $g$ deux fonctions à valeurs
	    réelles, définies sur un voisinage $\cal V_{x_0}$ de $x_0$. 
		
		Si $g$ ne s'annule pas sur $\cal V_{x_0}$, alors 
		
		$$
		f(x)\underset{x \to x_0}{\sim} g(x) \Longleftrightarrow \displaystyle\lim_{x\to x_0}
		\dfrac{f(x)}{g(x)} = 1
		$$

	=== "Indications"

         Deux  implications  à  démontrer.  Pour le  sens  direct,  utiliser  la
         définition  en faisant  attention  à définir  un  autre voisinage  pour
         introduire $φ$.
         Pour le sens réciproque, introduire $φ(x)=\dfrac{f(x)}{g(x)}$. 






Par exemple, $ \displaystyle\lim_{x\to 0} \dfrac{\sin(x)}{x} =1$ donc $ \sin(x) \underset{x \to 0}{\sim} x$


!!! {{ exercice()}}

    Déterminer un équivalent de $x\sqrt{\ln(x)}-\sqrt{x}\ln(x)$ au voisinage de $+\infty$.


!!! warning "Équivalence à zéro"
    
	Seule la fonction constante égale à zéro est équivalenet à zéro: pourquoi ?
	On nécrira donc  pas $ f(x) \underset{x  \to x_0}{\sim} 0$ sauf  dans ce cas
	très particulier.


!!! {{ theoreme("Équivalence et signe")}}

    === "Énoncé"
	
		1. Si $ f(x)\underset{x \to x_0}{\sim} g(x) $ et si $g$ ne s'annule pas sur
			un voisinage de $x_0$ alors $f$ ne s'annule pas non plus sur un voisinage
			de $x_0$.
		2.   Si $  f(x)\underset{x  \to x_0}{\sim}  g(x)  $ alors  $f$  et $g$  sont
			strictement de même signe sur un voisinage de $x_0$.
		
    === "Indications"
		Remarquez qu'il existe $\cal W_{x_0}$ un voisinage de $x_0$ sur lequel on a par exemple:
		$(x∈\cal W_{x_0})(1/2< φ(x) < 3/2)$.

!!! warning "locale vs globale"
	
	N'oubliez pas  que toutes  les propriétés vues  ici sont  vraies LOCALEMENT:
	elles peuvent être totalement fausses sur le voisinage d'un autre point.

!!! {{ theoreme("Équivalents et limites")}}

	1. $ f(x)\underset{x \to x_0}{\sim} g(x) ∧ \displaystyle\lim_{x\to
	x_0}g(x)=\ell ⟹ \displaystyle\lim_{x\to x_0}f(x)=\ell$

	2. $\displaystyle\lim_{x\to x_0}f(x)=\ell ∧ \ell∈ℝ^*⟹  f(x) \underset{x \to x_0}{\sim} \ell$
     






!!! {{ theoreme("Règles de calcul")}}

	Dans  tout  ce  qui  suit, $x_0∈  \overline{\mathbb{R}}$  et  les  fonctions
	introduites sont définie sur un voisinage $\cal V_{x_0}$ de $x_0$.
	
	1.    **Transitivité**:   $   f(x)\underset{x    \to   x_0}{\sim}   g(x)   ∧
	     g(x)\underset{x \to  x_0}{\sim} h(x)  ⟹ f(x)\underset{x  \to x_0}{\sim}
	     h(x)$
	1. **Symétrie**  $   f(x)\underset{x    \to   x_0}{\sim}   g(x)    ⟹ g(x)\underset{x  \to x_0}{\sim}
	     f(x)$
	2. **Produit** $  f_1(x)\underset{x  \to   x_0}{\sim} g_1(x) ∧ f_2(x)\underset{x  \to   x_0}{\sim}   g_2(x) ⟹ f_1(x)×f_2(x)\underset{x  \to   x_0}{\sim}  g_1(x)×g_2(x)$
	3. **Quotient**: si $g_2$ ne s'annule pas sur $\cal V_{x_0}$ alors $ \dfrac{f_1}{f_2}(x)  \underset{x \to x_0}{\sim} \dfrac{g_1}{g_2}(x)$
	4. **Produit par un scalaire**  $f(x)\underset{x  \to   x_0}{\sim}  g(x)  ⟹ (∀λ∈ℝ^*)(λ·f(x)\underset{x  \to   x_0}{\sim} g(x))$
	5.  **Produit par une fonction** $ f(x)\underset{x  \to   x_0}{\sim}   g(x)  ⟹ f(x)×h(x)\underset{x  \to   x_0}{\sim}  g(x)×h(x)$
	6. **Valeur absolue**  $f(x)\underset{x  \to   x_0}{\sim} g(x)  ⟹ |f|(x)\underset{x  \to   x_0}{\sim}   |g|(x)$
	7. **Puissance** $f(x)\underset{x \to x_0}{\sim} g(x)  ⟹ (∀α∈ℝ)((f(x))^α\underset{x  \to   x_0}{\sim}   (g(x))^α)$ sous couvert de définition des puissances.


Il  n'y  a  plus qu'à  trouver  des  exemples  et  à démontrer  chacune  de  ces
propositions.


!!! warning "Opérations dangereuses (i.e. interdites...)"

	1. somme
	2. composition, notamment  par $\ln$  et  $\exp$, sauf  la composition  par
	   $x\mapsto x^α$
	3. puissances dépendantes de $x$.
	
	Déterminer dans chaque cas des contre-exemples.
	
	
Voici  une situation  qu'il  ne faudra  pas confondre  avec  la composition  des
équivalents (qui est interdite):

!!! {{ theoreme("Substitution dans un équivalent")}}

    1. $  f(x) \underset{x  \to x_0}{\sim}  g(x) ∧  \displaystyle\lim_{t\to t_0}
       x(t)=x_0     ⟹    f\bigl(x(t)\bigr)     \underset{t    \to     t_0}{\sim}
       g\bigl(x(t)\bigr)$
	2. $ f(x) \underset{x \to x_0}{\sim} g(x) ∧ \displaystyle\lim_{n\to +\infty}
       x_n=x_0     ⟹    f\bigl(x_n\bigr)     \underset{n\to +\infty}{\sim}
       g\bigl(x_n\bigr)$

Quelle différence voyez-vous entre la substitution et la composition? 

Par exemple,  déterminez un équivalent  de $\sin \left(\dfrac{1}{n}  \right)$ au
voisinage de $+\infty$.


!!! {{ theoreme("Relation entre équivalence et négligeabilité")}}

    Soit $x_0 \in  \overline{ℝ}$, et $f$, $g$ deux fonctions  à valeurs réelles,
    définies sur un voisinage $\cal V_{x_0}$ de $x_0$. Alors:
	
	-  $ f(x)  \underset{x \to  x_0}{\sim} g(x)  \Longleftrightarrow (f  - g)(x)
	  \underset{x \to x_0}{=} {\Large \scr o}\left(g(x)\right) $. 
	- Si $ f(x)\underset{x \to x_0}{=}  {\Large \scr o}\left(g(x)\right) $ et si
	  $  g(x) \underset{x  \to x_0}{\sim}  h(x)  $ alors  $ f(x)\underset{x  \to
	  x_0}{=} {\Large \scr o}\left(h(x)\right) $.
	  
	  
	  
!!! {{ theoreme("Équivalents usuels au voisinage de 0")}}

    Soit $\alpha \in ℝ^*$ fixé
	
	$$
	\ln(1+x)  \underset{x\to 0}{\sim}  x,  \quad {\rm  e}^x  - 1  \underset{x\to
	0}{\sim} x ,\quad \sin(x) \underset{x\to 0}{\sim} x ,\quad 
	\cos(x)  -  1  \underset{x\to  0}{\sim}  - \frac{1}{2}  x^2  ,\quad  (  1  +
	x )^\alpha - 1 \underset{x\to 0}{\sim} \alpha x
	$$



!!! {{ exercice()}}

    Déterminez un équivalent de $x\mapsto \dfrac{\sin(42x)}{x^{17}}$. 




## Développements limités



### (Re-)découverte des développements limités aux ordre 0 et 1

!!! quote "Le Commercial Mathématicien - Acte II Scène 4"
	![Placeholder](./IMG/jourdain.jpg){ align=right width=400}
	MAÎTRE DE MATHÉMATIQUES. La voix, O, se forme en rouvrant les mâchoires, et rapprochant les lèvres par les deux coins, le haut et le bas, O.

	MONSIEUR JOURDAIN. O, O. Il n’y a rien de plus juste. A, E, I, O, I, O. Cela est admirable ! I, O, I, O.

	MAÎTRE DE MATHÉMATIQUES. L’ouverture de la bouche fait justement comme un petit rond qui représente un O.

	MONSIEUR JOURDAIN. O, O, O. Vous avez  raison, O. Ah la belle chose, que de
	savoir quelque chose !
	
	MAÎTRE DE MATHÉMATIQUES. Je vous expliquerai à fond toutes ces curiosités.
	
	MONSIEUR JOURDAIN : Par ma foi ! il y a plus de trois ans que je calcule des
	développements limités sans que j’en susse rien, et je vous suis le plus obligé du monde de m’avoir appris cela.


#### Ordre 0

Considérons   une   fonction   continue   en  $x_0$.   Cela   signifie   que
$\displaystyle\lim_{x\to     x_0}f(x)    =     f(x_0)$     ou    encore     que
$\displaystyle\lim_{x\to x_0}f(x)-f(x_0) = 0$ soit, en utilisant le langage vu à
la section précédente: $ f(x)-f(x_0)\underset{x \to x_0}{=} {\Large \scr o}\left(1\right)$ c'est-à dire
 
$$
f(x)\underset{x \to x_0}{=} f(x_0) + {\Large \scr o}\left(1\right)
$$


**Nous venons d'écrire le développement limité à l'ordre 0 de** $\mathbf{f}$ **au voisinage de** $\mathbf{x_0}$

#### Ordre 1

À vous  de jouer? À  votre avis, quelle  forme devrait prendre  un développement
limité  à l'ordre  1 ?  Quelle  propriété qu'on  demanderait à  $f$ de  vérifier
pourrait nous y amener?


### Un peu d'ordre : notion de DL

Voici  une  définition  qui  va  nous permettre  d'ordonner  nos  intuitions  et
d'avancer:

!!! {{ definition("$\operatorname{DL}_n(x_0)$") }}
    Soient  $n\inℕ$ et  $x_0\inℝ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $x_0$.Soient  $n\inℕ$ et  $x_0\inℝ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $x_0$. On dit que $f$ **admet un développement
    limité** d'ordre $n$ en $x_0$ (${\rm DL}_n(x_0)$ pour les intimes) lorsqu'il existe $ (a_0,a_1,\cdots,a_n) \in
    ℝ^{n+1} $ tel que: 

	$$
	f(x) \underset{x \to x_0}{=} a_0 + a_1 (x-x_0) + \cdots + a_n (x-x_0)^n +  {\Large \scr o}\left((x-x_0)^n\right)
	$$

	Le polynôme  $ a_0 + a_1  (x-x_0) + \cdots +  a_n (x-x_0)^n $ est  appelé la
	**partie   régulière**   du  développement   limité   et   $  {\Large   \scr
	o}\left((x-x_0)^n\right)$ son **reste**.

Par  exemple,  on  sait  que  $  \sin(x) \underset{x  \to  0}{\sim}  x$  i.e.  $
\sin(x)\underset{x \to  0}{=}x {\Large  \scr o}\left(x\right)$ qui  constitue un
${\rm DL}_1(0)$ de sin, la partie régulière étant $x$.



!!! {{ exercice()}}

    === "Énoncé"

         1. Déterminez le DL$_2(0)$ de $f:x\mapsto x^3\sin \dfrac{1}{x} $.
		 2.  Déterminez le DL$_1(0)$ de $g:x\mapsto (1+x)^{42}$
		 3.  Déterminez le DL$_n(0)$ de $h: x\mapsto \dfrac{1}{1-x} $.

	=== "Indications"

         1. $f(x) = x^2ε(x)$
		 2. Binôme de Newton
		 3. Suite géométrique


!!! {{ theoreme("Unicité du DL")}}

    === "Énoncé"

         Soient $n\inℕ$, $x_0\inℝ$ et $f$ une fonction à valeurs réelles définie au voisinage de $x_0$. Si $f$ admet un développement limité à l'ordre $n$ en $x_0$, alors ce développement est unique.

	=== "Indications"

         On suppose comme d'habitude qu'il y en a deux....

	



Il existe le pendant en l'infini:

!!! {{ definition("$\operatorname{DA}_n(+\infty)$") }}
    Soient  $n\inℕ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $+\infty$. On dit que $f$ **admet un développement
    asymptotique** d'ordre $n$ en $+\infty$ (${\rm DA}_n(+\infty)$ pour les intimes) lorsqu'il existe $ (a_0,a_1,\cdots,a_n) \in
    ℝ^{n+1} $ tel que: 

	$$
	f(x) \underset{x \to +\infty}{=} a_0 +  \dfrac{a_1}{x}  + \cdots + \dfrac{a_n}{x^n}  + {\Large \scr o}\left(\dfrac{1}{x^n} \right)
	$$


En fait,  on se  ramène le  plus souvent  à des DL  au voisinage  de 0  grâce au
théorème suivant:


!!! {{ theoreme("Équivalence de l'étude en 0")}}

	Soient  $n\inℕ$ et  $x_0\inℝ$. Soit  $f$  une fonction  à valeurs  réelles
    définie au voisinage de $x_0$. 
	
	Soit  $g$ la  fonction par $g:  h\mapsto f(h  +
	x_0)$. Alors  $g(x-x_0) = f(x_0  + x-x_0) = f(x)$.  Donc $g$ est  définie au
	voisinage de 0 et
	
	$$
	f(x)\underset{x \to  x_0}{=}a_0+a_1(x-x_0)+\cdots+a_n(x-x_0)^n+ {\Large \scr
	o}\left((x-x_0)^n\right) ⟺ g(h)\underset{h \to 0}{=}a_0+a_1h+\cdots+a_nh^n+ {\Large \scr o}\left(h^n\right)
	$$
	
En $+\infty$ on posera $g(h)=f\left(\dfrac{1}{h} \right)$.





### Formule de Taylor-Young

La formule suivante va nous donner une **condition suffisante** d'existence d'un DL:

 ![Placeholder](./IMG/FTY.png){ align=middle width=70%}
 
 
 ou dans la langue de Molière:
 
!!! {{ theoreme("Formule de Taylor-Young")}}

    === "Énoncé"
		Soit $  n \in  ℕ $.  **Si** $f$  est une  fonction de  classe $C^n$  sur un
		intervalle $I$ et $x_0\in I$,  **alors** $f$ admet pour développement limité
		d'ordre $n$ au voisinage de $x_0$:
		
		$$
		f( x_0 + h ) \underset{h \to 0}{=} \displaystyle\sum_{k=0}^n \dfrac{ h^k }{ k! } f^{(k)}(x_0) + {\Large \scr o}\left(h^n\right)
		$$
         

	=== "Indications"
		<!-- ( -->
        Admis :) Mais nous aurions pu le démontrer par récurrence.
		
		
		
!!! {{ exercice()}}

    Déterminez le DL$_3(π/2)$ de $f: x\mapsto \sin x$.

 
Cette formule nous donnera la clé des DL de référence que vous allez avoir le
plaisir de démontrer:

!!! {{ theoreme("DL$_n(0)$ de référence")}}

    -  $\exp(x)  \underset{x \to 0}{=}   1  +  x  +  \dfrac{x^2}2  +   \dfrac{x^3}{3!}  +  \cdots  +
      \dfrac{x^n}{n!} + {\Large \scr o}\left(x^{n}\right)
	  \underset{x   \to  0}{=}   \displaystyle\sum   _{k=0}^n  \dfrac{x^k}{k!}   +  {\Large   \scr
	  o}\left(x^{n}\right) $
	- $  \ln(1+x) \underset{x \to 0}{=} x  - \dfrac{x^2}{2} +  \dfrac{x^3}{3} + \cdots  + (-1)^{n-1}
	  \dfrac{x^n}{n}  + {\Large \scr o}\left(x^{n}\right)
	  \underset{x \to 0}{=}  \displaystyle\sum _{k=1}^n  (-1)^{k-1}  \dfrac {x^k}{k}  +
	  {\Large \scr o}\left(x^{n}\right)$
	- $\dfrac1{1-x} \underset{x \to 0}{=} 1 + x + x^2 +  \cdots + x^n + {\Large \scr o}\left(x^{n}\right)
		\underset{x \to 0}{=} \displaystyle\sum _{k=0}^n x^k +
	  {\Large \scr o}\left(x^{n}\right)$
	- $\dfrac1{1+x} \underset{x \to 0}{=} 1 - x + x^2 +  \cdots + (-1)^n x^n + {\Large \scr o}\left(x^{n}\right)
		\underset{x \to 0}{=} \displaystyle\sum _{k=0}^n
		(-1)^k x^k + {\Large \scr o}\left(x^{n}\right)$
	-   $(1+x)^{\alpha}  \underset{x \to 0}{=}   1   +  \alpha   \dfrac{x}{1!}  +   \alpha(\alpha-1)
	  \dfrac{x^2}{2!}    +     \cdots    +    \alpha(\alpha-1)\cdots(\alpha-n+1)
	  \dfrac{x^n}{n!} + {\Large \scr o}\left(x^{n}\right)$
	-  $ \sin(x)  \underset{x \to 0}{=} x  -  \dfrac{x^3}{3!} +  \dfrac{x^5}{5!} +  \cdots +  (-1)^n
	  \dfrac{  x^{2n+1}  }{ (2n+1)!  }  +  {\Large \scr o}\left(x^{2n+1}\right)  \underset{x \to 0}{=} \displaystyle\sum  _{k=0}^n  (-1)^k
	  \dfrac{ x^{2k+1} }{ (2k+1)! } + {\Large \scr o}\left(x^{2n+1}\right)$
	-  $ \cos(x)  \underset{x \to 0}{=} 1  -  \dfrac{x^2}{2!} +  \dfrac{x^4}{4!} +  \cdots +  (-1)^n
	  \dfrac{  x^{2n}   }{  (2n)!   }  + {\Large \scr o}\left(x^{2n}\right)   \underset{x \to 0}{=}  \displaystyle\sum   _{k=0}^n  (-1)^k
	  \dfrac{ x^{2k} }{ (2k)! } + {\Large \scr o}\left(x^{2n}\right)$
	


### Règles de calcul

#### ${\Large \scr o}\left(x^n\right)$


!!! {{ theoreme("Sur la manipulation des o")}}

	1. $ {\Large \scr o}\left(x^n\right) + {\Large \scr o}\left(x^n\right) \underset{x \to 0}{=} {\Large \scr o}\left(x^n\right)$
	1. $ {\Large \scr o}\left(x^n\right) - {\Large \scr o}\left(x^n\right) \underset{x \to 0}{=} {\Large \scr o}\left(x^n\right)$
	1. $ {\Large \scr o}\left(x^n\right) + {\Large \scr o}\left(x^p\right) \underset{x \to 0}{=} {\Large \scr o}\left(x^{\operatorname{min}(n,p)}\right)$
	1. $ x^p × {\Large \scr o}\left(x^n\right) \underset{x \to 0}{=} {\Large \scr o}\left(x^{p+n}\right)$
	1. $ {\Large \scr o}\left(x^n\right) × {\Large \scr o}\left(x^p\right) \underset{x \to 0}{=} {\Large \scr o}\left(x^{n+p}\right)$
	1. $  {\Large \scr o}\left(λ·x^n\right) \underset{x \to 0}{=} {\Large \scr o}\left(x^n\right)$


#### Linéarité

!!! {{ theoreme("Linéarité")}}

    Soit $f$ et $g$ admettant des DL$_n(0)$ de parties régulières respectives $P(x)$
    et  $Q(x)$.  Alors, pour  tout  $λ∈ℝ$,  $λf+g$  admet  un DL$_n(0)$  de  partie
    régulière $λP(x)+Q(x)$.
	
!!! {{ exercice()}}

    Déterminez le DL$_n(0)$ de 
	$\operatorname{ch}(x)=\dfrac{\operatorname{e}^x+\operatorname{e}^{-x}}{2}$ et de 
	$\operatorname{sh}(x)=\dfrac{\operatorname{e}^x-\operatorname{e}^{-x}}{2}$.
	
#### Produit
	
!!! {{ theoreme("Produit")}}

	Soit $f$ et $g$ admettant des DL$_n(0)$ de parties régulières respectives $P(x)$
    et $Q(x)$. Alors $f×g$  admet  un DL$_n(0)$  de  partie
    régulière le  produit $P(x)×Q(x)$  en ne  retenant que  les termes  de degré
    inférieur ou égal à $n$.
	
	
!!! {{ exercice()}}

    1. DL$_3(0)$ de $\operatorname{e}^{2x}\sqrt{1+x}$.
	1.  DL$_2(1)$ de $ \dfrac{\exp x}{x} $.
	1. DL$_7(0)$ de $\tan x$


#### Substitution

Comme pour les relations de dominance, on peut effectuer des substitutions. Il
faudra veiller à tronquer les parties régulières pour conserver l'ordre.

!!! {{ theoreme("Substitution")}}
	
	Si $  f(x)\underset{x \to x_0}{=}P(x) {\Large  \scr o}\left(x^n\right)$ avec
	$P∈ℝ_n[X]$    et    si   $u:    t\mapsto    u(t)$    est   telle    que    $
	\displaystyle\lim_{t\to0}u(t)=t_0$ alors on a le développement asymptotique:
	
	$$
	f(u(t))\underset{t \to t_0}{=}P(u(t)) {\Large \scr o}\left((u(t))^n\right)
	$$

	
!!! {{ exercice()}}

	DL$_2(0)$ de $x\mapsto 2^x$.




	
### Applications


#### Calcul de limite

!!! {{ exercice()}}

    $ \displaystyle\lim_{x\to 0}\dfrac{\sqrt{1+x}-\sqrt{1-x}}{\operatorname{e}^x-\operatorname{e}^{x^2}}$ 

#### Continuité et dérivabilité

!!! {{ theoreme("Lien avec la continuité et la dérivabilité")}}

    Avec les hypothèses habituelles:
	
	1. $f$ est continue en $x_0$ si, et seulement si, $f$ admet un DL$_0(x_0)$
	1. $f$ est dérivable en $x_0$ si, et seulement si, $f$ admet un DL$_1(x_0)$

#### Étude de série


!!! {{ exercice()}}

	Nature de la série de terme général 
	$u_n=\dfrac{1}{n} - \ln\left(1+\dfrac{1}{n} \right)$.
	
	
