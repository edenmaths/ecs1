{{ chapitre(23, "Variables aléatoires à densité")}}


!!! exo ""
	![Placeholder](./IMG/etheridge.jpeg){ align=right width=210}
	Alison ETHERIDGE  est une  probabiliste anglaise  née en  1964, actuellement
	directrice  du département  de  statistique de  l'Université d'Oxford.   Ses
	domaines de  recherche vont de  la finance  mathématique à la  génétique des
	populations  en passant  par  l'écologie mathématique  et  sa résolution  du
	problème dit du « *pain in the torus* », appellation qui ne fait sourire que
	les  probabilistes.  Son  travail  se caractérise  par  la  mobilisation  de
	domaines très divers ayant pour noyau les processus stochastiques.
	
	Mais  revenons à  notre petite  introduction aux  probabilités. De  nombreux
	problèmes, notamment  ceux étudiés par  Alison, font intervenir le  temps et
	nos modèles discrets ne sont  plus adaptés.  Il devient impossible
	dans ces conditions d'étudier la VAR à partir de sa 
	loi : la structure de $ℝ$ fait  que la probabilité $ℙ([X=a])$ est nulle pour
	tout  réel $a$.  Il faut  donc  changer d'approche...C'est  la fonction  de
	répartition de la VAR qui sera la clé :  si on ne peut pas donner de sens à
	$ℙ([X=1])$ on va pouvoir en donner à $ℙ([0,9≤ X ≤1,1])$.
	
	

## Programme


#### Introduction aux variables aléatoires à densité
<table border="0">
 <tr>
    <td>Définition d'une variable aléatoire à densité</td>
	<td>On dit   qu'une variable aléatoire $X$ est à densité
	si sa fonction de répartition $F_X$ est continue sur $ℝ$ et de classe
	$\scr C^1$ sur $ℝ$ éventuellement privé d'un ensemble fini de points.
</td>
	</tr>
    <tr>
	<td>Toute fonction $f_X$
à valeurs positives,
qui éventuellement ne diffère de $F'_X$ qu'en un nombre fini de points,
est une densité de $X$.
</td>
<td>
Pour tout $x$ de $ℝ$,
$\displaystyle F_X(x)=\int_{-\infty}^x f_X(t) {\rm d} t$.

</td>
	</tr>
    <tr>
	<td>Caractérisation de la loi d'une variable aléatoire à densité
par la donnée d'une densité $f_X$.</td>
<td>

</td>
	</tr>
    <tr>
	<td>Toute fonction $f$ positive,
continue sur ℝ\ éventuellement privé d'un nombre fini de points,
et telle que $\displaystyle \int_{-\infty }^{+\infty }f(t){\rm d} t=1$
est la densité d'une variable aléatoire.
</td>
<td>Résultat admis.

</td>
	</tr>
    <tr>
	<td>Transformation affine d'une variable à densité.
	</td>
<td>Les étudiants devront savoir calculer la fonction de répartition
et la densité de $aX+b$ ($a\neq0$).

</td>
	</tr>
    <tr>
	<td>Espérance d'une variable à densité.
	
Variables aléatoires centrées.
</td>
<td>
Une variable aléatoire $X$ de densité $f_X$ admet une espérance $\mathbb{E}(X)$
si et seulement si l'intégrale
$\displaystyle \int_{-\infty}^{+\infty}xf_X(x)\hbox{d}x$ est absolument convergente ;
dans ce cas, $\mathbb{E}(X)$ est égale à cette intégrale.

Exemples de variables aléatoires n'admettant pas d'espérance.


</td>
	</tr>
    <tr>
	<td>$\mathbb E(aX+b)=a\mathbb E(X)+b $. </td>
	<td></td>
	</tr>

</table>


#### Lois de variables à densité usuelles 
<table border="0">
    <tr>
	<td>Loi uniforme sur un intervalle. Espérance. 
</td>
<td>Notation $X \leadsto\mathcal{U}[a, b]$

$\displaystyle X\leadsto\mathcal{U}[0 , 1]\Longleftrightarrow Y=a+ (b-a)X \leadsto\mathcal{U}[a , b]$. 

</td>
	</tr>
    <tr>
	<td>Loi exponentielle. Caractérisation par
l'absence de mémoire. Espérance.
</td>
<td>
Notation $X \leadsto\mathcal{E}(\lambda)$.

$\displaystyle X\leadsto\mathcal{E}(1) \Longleftrightarrow Y=\frac{1}{\lambda}X \leadsto\mathcal{E}(\lambda)\quad (\lambda >0)$.

</td>
	</tr>
    <tr>
	<td>Loi normale centrée réduite,
loi normale (ou de Laplace-Gauss). Espérance.
</td>
<td>
Notation $X \leadsto\mathcal{N}(\mu,\sigma^2)$. 

$X\leadsto \mathcal{N}(\mu,\sigma^2) \Leftrightarrow X^{*}=\dfrac{X-\mu}{\sigma}\leadsto \mathcal{N}(0,1)$
avec $\sigma>0$.

On attend des étudiants qu'ils sachent représenter graphiquement
les fonctions densités des lois normales et
utiliser la fonction de répartition $\Phi$ de la loi
normale centrée réduite.
</td>
</tr>
</table>



## Retour sur la fonction de répartition


### Rappel des résultats généraux

On rappelle la définition vue en début d'année :

!!! {{ definition("Fonction de répartition") }}
	 
	Soit X une v.a.r. On appelle fonction de répartition de X la fonction
    F de $ℝ$ dans $ℝ$ définie par : 
       
	$$
	F_X(x)=ℙ(\{ω∈Ω\, \mid\, X(ω) ≤x\}) = ℙ(X^{-1}(]-\infty,x]) = ℙ([X \leqslant x])=ℙ_X(]- ∞ ,x])
	$$

et les propriétés suivantes :

!!! {{ theoreme("Propriétés de la fonction de répartition") }}
         Soit  $X$   une  VAR  discrète   définie  sur  un   espace  probabilisé
         $(Ω,\mathscr{T}_Ω,ℙ)$.
		 La fonction de répartition $F_X$ vérifie les **trois** propositions suivantes:
		 
		 1. $F_X$ est croissante sur $ℝ$.
	     3.  $\displaystyle\lim_{x\to+\infty}F_X(x)=1, \
	        \displaystyle\lim_{x\to-\infty}F_X(x)=0$
         4. $F_X$ est continue à droite en tout point de $ℝ$.
		 
Le point le plus fondamental de ce chapitre et que nous
admettrons est qu'**à toute fonction vérifiant ces 3  propriétés on peut
associer  une  variable aléatoire  X  et  donc  une unique  probabilité**  $ℙ_X$
(muni en  fait d'une tribu spéciale  : non pas $\mathfrak{P}(ℝ)$  mais une tribu
moins fine, la tribu *borélienne* $\cal B(ℝ)$ i.e.  la tribu *engendrée* par les intervalles $]-\infty,x]$...mais ceci ne nous concerne pas en ECS...


Nous avions de plus remarqué que $F_X$, si elle était continue à droite, ne
l'était pas forcément à gauche (là où il y a d'éventuels *sauts*).

Nous     avions     donc     établi     que     $ℙ([X=x])     =     F_X(x)     -
\displaystyle\lim_{\substack{t\to x\\t<x}}F_X(t)$.



### Fonction de répartition continue

Dans le cas où la fonction de  répartition est continue, les choses vont changer
car on ne peut plus définir la loi en aucun réel particulier : $ℙ([X=x])     =     F_X(x)     -
\displaystyle\lim_{\substack{t\to x\\t<x}}F_X(t)=  F_X(x)- F_X(x)= 0$.

Il en suit :


!!! {{ theoreme("Loi à partir de la fonction de répartition")}}

	Soit $F_X$ une  fonction de répartition **continue** sur  $ℝ$ d'une certaine
	loi $ℙ_X$ d'une VAR X et soit $(a,b)∈ℝ^2$, alors:
	
	- $ℙ([X=a]) = 0$
	- $ℙ([X < a])=ℙ([X≤a]) = F_X(a)$
	- $ℙ([X > a])=ℙ([X≥a]) = 1-F_X(a)$
	- $ℙ([a<X<b])=ℙ([a<X≤b])=ℙ([a≤X<b])=ℙ([a≤X≤b])=F_X(b)-F_X(a)$

À démontrer.


### Densité de probabilité

!!! {{ definition("Densité de probabilité") }}
    Une fonction  réelle $f_X$ définie sur  $ℝ$ est une densité  de probabilité si
    elle est :
	
	- **positive** sur $ℝ$
	- **continue par morceaux** (avec un nombre **fini**de morceaux) et d'intégrale convergente sur $ℝ$
	- vérifie $ \displaystyle\int_{-\infty}^{+\infty} f(t) {\rm d}t=1$
	
	
!!! {{ theoreme("Fonction de répartition liée à une densité")}}

	Si $f_X$ est une densité de probabilité, alors la fonction:
	
	$$
	F_X:x\mapsto \displaystyle\int_{-\infty}^x f_X(t) {\rm d}t
	$$
	 
	est la fonction de répartition d'une VAR.
	 
Vérifiez-le !


!!! {{theoreme( "Classe d'une fonction de répartition")}}

	Si $F_X$ est la fonction de  répartition associée à une densité $f_X$, alors
	$F_X$ est de classse $\scr C^1$ par morceaux sur $ℝ$ (avec un nombre fini de
	morceaux).




!!! {{ definition("densité d'une loi") }}
    Soit X une VAR. On dit que X **a une loi de densité** $f_X$ (ou par abus *est de
    densité $f_X$*) si pour tout réel $x$:
	
	$$
	ℙ([X ≤x]) = \displaystyle\int_{-\infty}^x f_X(t) {\rm d}t
	$$
	


!!! {{ theoreme("Propriétés d'une fonction de répartition liée à une densité")}}

	Soit X une VAR dont on connaît la loi de densité $f_X$, alors:
	
	1. Sa fonction de répartition $F_X$ est continue sur $ℝ$ de sorte que:
	   
	    $$
	    ∀x∈ℝ, \ ℙ([X=x])=0
	    $$
	   
	2. La fonction $F_X$  est dérivable en tout point $x$  où $f_X$ est continue
	   et : 
	
	    $$
	    F_X'(x) = f_X(x)
	    $$
		

Vérifiez en  particulier que le  deuxième point assure  que $F_X$ vérifie  les 3
propriété du théore 23-1.

Les représentations de $F_X$  et $f_X$ ne nous donnent pas la  même vision de la
répartition  des probabilités.  Comment lire  quelles  sont les  valeurs de  $x$
correspondant aux plus fortes probabilités ?
	   

<div class="chart-container" style="position: relative; width:70%">
	<canvas id="myChart5"  style="background-color: rgba(0,0,0,0.2)"></canvas>
</div>


<div class="chart-container" style="position: relative; width:70%">
	<canvas id="myChart6"  style="background-color: rgba(0,0,0,0.2)"></canvas>
</div>


!!! {{ theoreme("Densité d'une loi dont on connaît la fonction de répartition")}}

	Si la fonction de répartition $F_X$ est 
	
	- **continue sur** $ℝ$
	- **dérivable  sauf éventuellement  en un  nombre fini  de points**  alors X
	  admet la densité: $
	    F_X'(x) = f_X(x)
	   $
	  
!!! note "Pourquoi ce nom de densité de probabilité ?"

	Pour un « petit » accroissement $Δx$ de $x$, on a
	
	$$
	f_X(x) \simeq \dfrac{F_X(x+Δx)-F_X(x)}{Δx} = \dfrac{ℙ_X([x, x+Δx])}{Δx} 
	$$
	
	physiquement homogène  à la  « densité  » de  probabilité sur  un intervalle
	donné.
	
	Ainsi $f_X(x){\rm d}x$ est homogène à  une probabilité, ce qui donne un sens
	à l'écriture généralisée:
	
	$$
	ℙ(A)       =       \displaystyle\sum_{\substack{       \vphantom{a}       \\
	x∈A}}\!\!\!\!\!\!\!\!\int \ p(x) \text{ et } \mathbb{E}(X) = \displaystyle\sum_{\substack{       \vphantom{a}       \\
	x∈X(Ω)}}\!\!\!\!\!\!\!\!\!\!\!\int \ x×p(x)
	$$
	
	Avec $p(x)=p_i$ dans le cas discret et $f_X(x){\rm d}x$ dans le cas continu.


!!! warning "Attention !"

	$f_X$ n'est pas une probabilité !!!! 




!!! info "Pour vérifier que F est une fonction de répartition d'une VAR à densité"

	1. $F$ est continue sur $ℝ$.
	1. $F$ est de classe $\cal C^1$ sur $ℝ$ sauf éventuellement en un nombre fini de points.
	1. $F$ est croissante sur $ℝ$.
	3.  $\displaystyle\lim_{x\to+\infty}F(x)=1,\ \displaystyle\lim_{x\to-\infty}F(x)=0$
         







!!! {{ exercice()}}

	Soit     F    définie     sur     $ℝ$    par     $F(x)    =     \dfrac{1}{1+
	\operatorname{e}^{-x}}$.  
	
	1. Démontrer que F est la fonction de répartition
	d'une variable à densité X.
	2. On admet que  $Y=X^2$ est une VAR. Démontrer que c'est  une VAR à densité
	   en étudiant sa fonction de répartition.
	3. Déterminez des expressions des densités $f_X$ et $f_Y$.


!!! remarque

	Pour une VAR X donnée, il n'y a qu'une FDR mais il y a plusieurs DDP il : il
	suffit  de changer  un  nombre fini  de  valeurs de  $f_X$  par des  valeurs
	positives. 


!!! {{ exercice()}}
	
	Soit la fonction $ f_X\colon \begin{array}{rll}
 	ℝ &   \to   & ℝ \\
    x &  \mapsto & \begin{cases}  k \operatorname{e}^{-2x}  \text{ si }  x>0\\ 0
    \text{ sinon }\end{cases}
    \end{array}
	$
	
	1. Déterminez $k$ pour que $f_X$ soit une densité de probabilité.
	2. Calculez $ℙ([1<X≤3])$.
	3. Déterminez l'expression de $F_X(x)$ en fonction de $x$.


	  
!!! {{theoreme("Calcul de probabilités d'une loi à densité")}}
	
	Soit $F_X$ une  fonction de répartition continue sur  $ℝ$ d'une certaine
	VAR $X$ de densité $f_X$ et soit $(a,b)∈ℝ^2$ tels que $a ≤b$ alors:
	
	$$
	ℙ([a<X<b]=F_X(b)-F_X(a)=\displaystyle\int_a^b f_X(t) {\rm d}t
	$$
	


!!! info "Thumb rule"

	-  Si on  a  juste besoin  de calculer  *une*  probabilité $ℙ([a<X<b])$,  on
	  *intègre* directement la densité sur $[a,b]$ : $ℙ([a<X<b]= \displaystyle\int_a^b f_X(t) {\rm d}t$
	- Si on a *plusieurs* probabilités  à calculer, on détermine l'expression de
	  la fonction  de répartition $F_X$  et on utilise  autant de fois  que l'on
	  veut $ℙ([a<X<b]=F_X(b)-F_X(a)$. 
	  
	  
### Transformation affine de VAR à densité

!!! {{ theoreme("Caractéristiques de $Y=aX+b$")}}

    Soit $X$ une variable aléatoire à densité, et $Y=aX+b$ avec $a\neq 0$. Alors
    $Y$  est également  une  variable aléatoire  à densité,  et  sa fonction  de
    répartition et une densité sont données par : 
	
	$$
	\forall y \in ℝ,\ F_Y(y) = F_X \left( \dfrac{y-b}{a} \right) ,\quad f_Y ( y ) = \dfrac{1}{|a|}
	f_X \left( \dfrac{y-b}{a} \right)
	$$
	
	
!!! Remarque

	Ce théorème sert souvent en ECS pour traiter les VAR centrées réduites de la
	forme $Y=\dfrac{X-μ}{σ} $ où $σ>0$. Alors la FDP de Y est par exemple $f_Y(x)=σf_X(σx+μ)$.
	

## Espérance d'une VAR à densité

!!! {{ definition("Espérance d'une VAR à densité") }}
    Soit $X$ une variable aléatoire de densité $f_X$.  **Si** l'intégrale 
	$ \displaystyle\int_{-\infty}^{+\infty} t×f(t) {\rm d}t$ est **absolument convergente**, alors
    $X$ admet une espérance et 
	
	$$
	\mathbb E(X) = \displaystyle\int_{-\infty}^{+\infty} t×f(t) {\rm d}t
    $$


C'est toujours le même principe que dans le cas discret : 
  $\mathbb E(X)=\displaystyle\sum_{\substack{ \vphantom{a} \\
	x∈X(Ω)}}\!\!\!\!\!\!\!\!\!\!\!\!\int \ \ x×p(x)$

!!! Attention

	L'espérance n'existe pas toujours !
	
!!! {{ exercice("Des études d'espérance")}}
	![Placeholder](./IMG/expectations.jpg){ align=right width=210}
    Étudier les espérances des VAR X dont on donne:
	
	1.   La    FDR   $F_X(x)=\begin{cases}    0   \text{   si    }   x    <   0\\
	   1-\operatorname{e}^{-λx} \text{ sinon}\end{cases}$ avec $λ>0$.
    1. La FDR $F_X(x)=\begin{cases} 0 \text{ si } x < 0\\ x \text{ si } x∈[0,1]\\
	   1 \text{ sinon}\end{cases}$
	1. La FDR $F_X(x)=\dfrac{1}{π} \left(\arctan(x) + \dfrac{π}{2} \right)$.
	1. Une DDP $f_X(x)=\dfrac{1}{\sqrt{2\pi}} \operatorname{e}^{-\frac{x^2}{2}}$.


!!! {{ theoreme("Linéarité de l'espérance")}}

	Soit  $X$  une variable  aléatoire  à  densité  admettant une  espérance  et
	$(a,b)∈ℝ^*⊗ℝ$. 
	Alors $aX+b$ admet une espérance, et: 
	
	$$
	\mathbb E(a X + b) = a \mathbb E(X) + b
	$$

!!! Remarque

	Le théorème de transfert sera vu en seconde année.
	
	
## Quelques lois classiques

### Loi uniforme

#### Loi uniforme sur [0, 1]

C'est la loi la plus simple...

!!! {{ theoreme("loi uniforme sur [0,1]") }}
    
	La   fonction  $f:x\mapsto   \mathbb  1_{[0,1]}(x)$   est  une   densité  de
	probabilité.
	
	Toute variable  aléatoire admettant $f$  pour densité est  appelée *variable
	aléatoire uniforme sur* $[0,1]$. 
	
	On dit aussi que  X suit une loi uniforme sur $[0,1]$  et on note $X\leadsto
	{\cal U}([0,1])$.


Malgré  (ou  grâce à)  sa  simplicité,  elle est  très  importante  car elle  va
permettre de simuler toutes les autres lois. 

Sur Scilab  notamment, la  fonction `rand`  a les  caractéristiques statistiques
de tirages  indépendants de  VAR de  loi uniforme sur  $[0,1]$. Elle  utilise un
*générateur  congruentiel*  qui  produit  une suite  d'entiers  à  partir  d'une
*graine* $x_0$ selon la formule:

$$
x_{n+1} = (ax_n+b)\ {\rm mod } \ m
$$

avec  $a=8\,433\,148\,614$, $b=453\,816\,693$  et  $m=2^{31}$. Il  a  l'avantage d'être  de
période $2^{31}$ :  on doit attendre une séquence de  $2^{31}$ nombres (soit plus
de 2 milliards) avant de revenir sur un nombre déjà donné.


Il existe une fonction plus sophistiquée,  `grand`, qui utilise au choix le même
générateur que  `rand` mais propose aussi  le *Keep It Simple  Stuid* de période
$2^{123}$ ou par défaut le  *Mersenne-Twister* de période $2^{19937}$ (un nombre
de 139 chiffres...).

Observons par exemple la distribution d'un  tirage de 10 000 nombres avec `rand`
en traçant un histogramme comportant 20 rectangles de même largeur:

```scilab
--> r = rand(10000,1);

--> scf(0);

--> histplot(20, r);
```


![Placeholder](./IMG/uni.svg){ align=center width=90%}



!!! {{ exercice("Caractéristiques de la loi uniforme sur [0, 1]")}}

	Soit  $X\leadsto \mathcal{U}([0,1])$.  Déterminer $F_X$,  $\mathbb E(X)$  et
	tracer les représentations graphiques de $f_X$ et $F_X$.


#### Loi uniforme sur [a, b]


!!! {{ theoreme("loi uniforme sur [a, b]") }}
    
	Soit $(a,b)∈ℝ^2$ avec $a<b$. La fonction $f:x\mapsto \dfrac{1}{b-a} \mathbb 1_{[a,b]}(x)$ est une densité de
	probabilité. 
	
	Toute variable  aléatoire admettant $f$  pour densité est  appelée *variable
	aléatoire uniforme sur* $[a,b]$. 
	
	On dit aussi que X suit une loi uniforme sur $[a,b]$ et on note $X\leadsto {\cal U}([a,b])$.




!!! {{ exercice("Caractéristiques de la loi uniforme sur [a, b]")}}

	Soit  $X\leadsto \mathcal{U}([a,b])$.  Déterminer $F_X$,  $\mathbb E(X)$  et
	tracer les représentations graphiques de $f_X$ et $F_X$.

!!! {{ theoreme("Lien entre $\mathcal{U}([a,b])$ et $\mathcal{U}([0,1])$")}}

    Soit $(a,b)∈ℝ^2$ avec $a<b$.
	
	$$
	Y\leadsto {\cal U}([a,b]) ⟺ X = \dfrac{Y-a}{b-a} \leadsto {\cal U}([0,1])
	$$
	
	$$
	X\leadsto {\cal U}([0,1]) ⟺ Y = a + (b-a)X \leadsto {\cal U}([a,b])
	$$
	
	

!!! {{ exercice()}}

	À  partir  de  7h,  les  chronobus passent  toutes  les  15  minutes  devant
	l'Externat. Une étudiante se présente entre 17h et 17h30 devant l'aubette de
	bus, l'heure exacte de son arrivée étant une variable aléatoire uniforme sur
	cette période  de temps.  Déterminer la  probabilité qu'elle  doive attendre
	moins de 5 minutes le bus, puis plus de 10 minutes.



### Loi exponentielle


#### Poisson exponentiel

Nous avons  déjà étudié  la loi de  Poisson qui nous  permettait par  exemple de
compter le  nombre d'occurrences d'événements  rares dans un intervalle  de temps
donné.

Supposons que  nous voulions  déterminer la  probabilité qu'il  y ait  un nageur
avalé par un requin blanc sur les côtes vendéennes dans les 50 prochaines années.

On estime qu'il y a eu 20 nageurs vendéens victimes d'un requin blanc lors des 200 000
dernières années.  Le conseil régional  de Vendée estime  donc que le  nombre de
victimes annuelles suit une loi de Poisson de paramètre $ \dfrac{1}{10\,000}$.

Ce qui nous intéresse maintenant, c'est de savoir comment est réparti le temps d'attente
(qui est continu) entre deux attaques mortelles de requin blanc. 

Comme souvent lors  de l'étude de variables continues, on  va travailler avec la
fonction de répartition (qui nous donne une probabilité : $ℙ([T≤t])$) plutôt que
la densité.

Soit $N_t$ la VAR qui donne le nombre de victimes entre maintenant et un instant
$t$  fixé. Notons  $λ=\dfrac{1}{10\,000}$, alors  $N_t\leadsto {\cal  P}\left(λt
\right)$ donc $ℙ(N_t=n)=\dfrac{(λt)^n \operatorname{e}^{-λt}}{n!}$. 

Notons T la VAR continue égale au  temps d'attente avant la prochaine victime
du requin.

Si $t<0$, $F_T(t)=0$.

Si $t≥0$, 
$F_T(t)=ℙ([T≤t]) = ℙ([N_t≥1]) = 1 - ℙ([N_t=0]) = 1 - \operatorname{e}^{-λt}$.


Quelle est alors la probabilité qu'il y ait une victime du requin blanc dans les
500 prochaines années ? 

On dit dans ce cas que $T$ suit une loi exponentielle de paramètre $λ$. 

Le  temps  d'attente d'un  «  phénomène  de Poisson  »  n'est  pas le  seul  cas
d'utilisation de la  loi exponentielle. On peut en donner  une définition et des
propriétés.

#### Caractéristiques de la loi exponentielle

!!! {{theoreme("Densité de la loi exponentielle")}}

	Soit $λ>0$. La fonction $f$ définie par :
	
	$$
	f(x) = λ \operatorname{e}^{-λx}×\mathbb 1_{ℝ^+}(x)
	$$
	
	est une densité de probabilité.
	
	Toute  VAR X  admettant  $f$  pour densité  est  appelée variable  aléatoire
	exponentielle  de  paramètre   $λ$.  On  dit  aussi  que  X   suit  une  loi
	exponentielle de paramètre $λ$ et on note $X\leadsto {\cal E}(λ)$.
	

!!! {{theoreme("Fonction de répartition de la loi exponentielle")}}

	Si $X\leadsto {\cal E}(λ)$ alors la FDP de X est définie par :
	
	$$
	F_X(x)=(1-\operatorname{e}^{-λx})\mathbb 1_{ℝ^+}(x)
	$$


!!! {{theoreme("Espérance et  loi exponentielle")}}

	Si $X\leadsto {\cal E}(λ)$ alors elle admet une espérance  définie par :
	
	$$
	{\mathbb E}(X)= \dfrac{1}{λ} 
	$$


!!! {{theoreme("Loi de paramètre 1")}}

	 $X\leadsto {\cal E}(λ) ⟺ λX \leadsto {\cal E}(1)$ 
	


!!! {{theoreme("Loi sans mémoire")}}
	
	La VAR X est  sans mémoire si, et seulement si X  est presque sûrement nulle
	ou suit une loi exponentielle.
	

### Loi normale (Laplace-Gauss)


![Placeholder](./IMG/normal.jpg){ align=left width=30%}
![Placeholder](./IMG/normal2.jpg){ align=right width=30%}
Il est  temps de parler  de la la  plus ancinne et la  plus célèbre des  lois de
probabilité.  Elle  a été  étudiée  bien  avant  la  formalisation des  lois  de
probabilités. Depuis Galilée  en  1618, en passant par Jacques Bernoulli,
Abraham de Moivre (qui  va introduire la loi normale comme  loi limite d'un eloi
binomiale), Pierre-Simon de Laplace (qui généralise ce théorème central limite que
vous avez évoqué au  lycée, mais toujours en étudiant des  lancers de pièces non
équilibrées et de  la distribution des résultats autour de  1/2). Gauss introduit
en  1816  la courbe  qui  porte  son nom  pour  étudier  le mouvement  de  corps
célestes. Ensuite arrivent Galton (qui  sera à l'origine du mouvement eugéniste)
et  le  belge Adolphe  Quetelet  qui  rapproche  la  distribution des  tours  de
poitrines de  soldats écossais  de la  courbe de  Gauss. Pendant  longtemps, les
français  ont parléd  e la  loi de  Laplcae  et les  anglo-saxons de  la loi  de
Gauss. Puis en 1885 Francis Galton parle  de la courbe da Gauss comme *perfectly
normal in shape* ce  qui lancera l'appellation de loi normale.  Pour que tout le
monde soit content on parle aussi maintenant de loi de Laplace-Gauss....


#### Гаусса не обманешь!

![Placeholder](./IMG/normal3.jpg){ align=center width=80%}

Mais des spécialistes de la géopolitique seront peut-être plus intéressés par une
manisfestation  qui eut  lieu à  Moscou après  les élections  présidentielles de
décembre 2011
et où les manifestants portaient des pancartes où on pouvait voir des courbes de
Gauss  (les Russes  n'ont pas,  comme  en France,  abandonné l'enseignement  des
mathématiques...)


Sur les figures ci-dessous, on peut voir la distribution du nombre de bureaux de
vote aux
élections selon leur taux de participation.  D'un côté au Mexique, en Bulgarie, en
Pologne, en Suède, de l'autre en Russie...

![Placeholder](./IMG/election1.png){ align=left width=48%}
![Placeholder](./IMG/election2.png){ align=right width=48%}


et voici les courbes correspondant aux élections de 2011:

![Placeholder](./IMG/election3.png){ align=middle width=48%}
![Placeholder](./IMG/election3.jpeg){ align=middle width=48%}


Conclusion : est-ce qu'un un peu de maths aide à être démocrate ?

![Placeholder](./IMG/putin.jpg){ align=middle width=28%}
	  
	  
Pour  voir  que  les  choses  n'ont  pas  changé  en  2016,  2018  et  2020,  ce
[site](https://github.com/dkobak/elections/)  analyse  les  anomalies  dans  les
résultats russes depuis 2000.

#### Loi normale centrée réduite

!!! {{ theoreme("(admis) VAR normales...ou gaussiennes...ou laplaciennes centrées réduites") }}
    La  fonction   définie  par:   $  \forall  t   \in  ℝ  $,   $  f_X   (t)  =
    \dfrac{1}{\sqrt{2\pi}} \operatorname{e}^{-\frac{t^2}{2}}  $ est une densité  d'une variable
    aléatoire à densité $X$. On dit que $X$ suit une **loi normale centrée
    réduite** et on note $ X \leadsto \mathcal{N}( 0 , 1) $. 




<div class="chart-container" style="position: relative; width:70%">
	<canvas id="myChart7"  style="background-color: rgba(0,0,0,0.2)"></canvas>
</div>


!!! {{ theoreme("Fonction de répartition de N(0,1)")}}

    Pour tout réel $t$, 
	
	$$
	F_X  (x)  =  \dfrac{1}{\sqrt{2\pi}}  \displaystyle\int  _{-\infty}  ^x  {\rm
	e}^{ -\frac{t^2}{2} } {\rm d}t
	$$
	
	De plus, $ F_X(-x) = 1 - F_X(x) $ et en particulier $ F_X(0) = \dfrac{1}{2} $.



!!! {{ theoreme("Nouvelle formule")}}

	$ \displaystyle\int _0 ^{+\infty} {\rm e}^{-\frac{t^2}{2}} {\rm d}t = \sqrt{\dfrac{\pi}{2}}$


!!! {{ theoreme("Espérance")}}
	
	Si $  X \leadsto \mathcal{N}( 0  , 1 ) $,  alors $X$ admet une  espérance et
	$\mathbb{E}(X)=0$.


#### Cas général


!!! {{ theoreme("Loi normale générale") }}
    Soit $  ( m ,  \sigma ) \in  ℝ \otimes ℝ_+^* $. On  dit que $X$  suit une
    **loi normale** de paramètres $ m $ et $ \sigma^2 $ et on note $ X
    \leadsto   \mathcal{N}(    m   ,    \sigma^2)    $   lorsque
    $\dfrac{1}{\sigma}( X-m ) \leadsto \mathcal{N}( 0 , 1 ) $.  Une
    densité est alors: $ \forall t \in ℝ $, 
	
	$$
	f_X(t) = \dfrac{1}{\sigma\sqrt{2\pi}} {\rm e}^{-\dfrac{(t-m)^2}{2\sigma^2}}
	$$



!!! {{ theoreme("Fonction de répartition et espérance")}}

    Si $ X \leadsto \mathcal{N}( m , \sigma^2) $, alors pour tout réel $t$,
	
	$$
	F_X (x) = \dfrac{1}{\sigma\sqrt{2\pi}}  \displaystyle\int _{-\infty} ^x {\rm
	e}^{-\frac{(t-m)^2}{2\sigma^2}} {\rm d}t
	$$
	
	De plus, $X$ admet une espérance: $\mathbb{E}(X)=m$.
	

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.2.0/chart.js" integrity="sha512-opXrgVcTHsEVdBUZqTPlW9S8+99hNbaHmXtAdXXc61OUU6gOII5ku/PzZFqexHXc3hnK8IrJKHo+T7O4GRIJcw==" crossorigin="anonymous"></script>

<script src="../courbes_probas.js"></script>

