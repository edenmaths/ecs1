


{{ chapitre(20, "Espaces probabilisés discrets")}}


!!! exo ""
    ![Placeholder](./IMG/sdpoisson.jpg){ align=right  width=250}
     En  1837,  Siméon  Denis POISSON  (1781-1840)  publia  ses
     *Recherches sur la  probabilité des jugements en  matière criminelle et
      en matière civile*. Il introduit  notamment, pour étudier les délibérations
      de jurys, une loi qui portera son nom pour
      modéliser des situations rares  (résultats qu'avait obtenus Abraham DE
      MOIVRE un siècle plus tôt). En 1898, ses travaux seront repris par
      Ladislaus BORTKIEWICZ  pour modéliser le nombre  de soldats prussiens
      tués par des coups de sabots de  chevaux. Depuis on l'étudie pour modéliser la
      gestion de  stocks, les défauts  de crédit, les problèmes  d'absentéisme, les
      catastrophes, etc.
      Pour cela,  on a  besoin de  passer des probabilités  *à la  limite* en
      faisant tendre le nombre d'expériences possibles vers l'infini. Les outils vus
      jusqu'à maintenant étaient trop sommaires, notre étude des séries va nous
      permettre d'aller plus loin.

## Définition générale des probabilités

### Ensembles dénombrables



!!! {{ definition("Ensemble dénombrable")}}
    Un ensemble est *dénombrable* si, et  seulement si, il est en bijection
    avec $ℕ$. Ses éléments peuvent donc être énumérés en une suite $(e_n)_{n∈ℕ}$.


Par exemple, $ℕ$, $ℤ$  et $ℚ$ sont dénombrables mais pas  $ℝ$ ni tout intervalle
$[α,β]$ avec $α<β$.

On admettra que:

- Tout ensemble dénombrable est infini.
- Toute partie d'un ensemble dénombrable est soit dénombrable, soit finie.
- La réunion d'une famille finie ou dénombrable d'ensembles eux-mêmes finis
  ou dénombrables est un ensemble fini ou dénombrable.


### Tribus


!!! {{ definition("Tribu")}}
    Une tribu ${\mathscr T}_Ω$ définie sur un univers $Ω$ est une partie de
    $𝔓(Ω )$ qui vérifie:
    
    1. $Ω  ∈  {\mathscr T}_Ω$;
    2. $A ∈ {\mathscr T}_Ω    \Longrightarrow   \complement_Ω    A    ∈  {\mathscr T}_Ω$ (stabilité par passage au complémentaire);
    3. $∀      I⊆ℕ$,       $(A_i)_{i ∈       I} ∈ {\mathscr  T}_{Ω   }^{\#I}\Longrightarrow  \displaystyle\bigcup_{i  ∈   I}A_i  ∈{\mathscr T}_Ω ∧  \displaystyle\bigcap_{i  ∈   I}A_i  ∈ {\mathscr T}_Ω$ (stabilité par réunion et intersection dénombrable).


!!! info "Stabilité"

    === "Remarque"
        En fait la stabilité par intersection dénombrable est une conséquence de
        la  stabilité par  réunion et  par passage  au complémentaire  si on  se
        souvient des lois de DE MORGAN.

    === "Démonstration"
        Soit $\bigl( A_i \bigr)_{i\in I} \in \mathscr{T}_Ω^{\#I}$. Alors 
		$\displaystyle\bigcap_{i∈I} A_i = \overline{ \bigcup _{i∈I} \overline{ A_i } }$. 
		Comme $\forall i \in I$,  $\overline{ A_i }  \in \mathscr{T}_Ω$,  alors par
		réunion dénombrable $\displaystyle\bigcup _{i∈I}\overline{ A_i } \in \mathscr{T}_Ω$,
		 donc par passage à l'événement contraire, 
		$\displaystyle\bigcap  _{i∈I} A_i \in \mathscr{T}_Ω$. 

Attention!  Une tribu est une partie de $𝔓(Ω )$: c'est donc un
ensemble de sous-ensembles de $Ω$ ! On parle de **tribu d'événements**.


Comme ces  parties appartenant à la  tribu sont des sous-ensembles  de résultats
possibles de l'expérience, la tribu  modélise l'information que nous donne cette
expérience.


!!! {{ definition("Espace probabilisable")}}
	Soit ${\mathcal T}_Ω$ une tribu définie sur l'univers $Ω$. On dit que
	$\bigl( Ω  ,{\mathcal T}_Ω \bigr)$  est un espace probabilisable  (ou espace
	d'évènements).
	Les éléments de la tribu sont appelés **événements**.


Considérons par exemple l'expérience aléatoire consistant à lancer un dé cubique
équilibré et notons $Ω$ l'univers constitué des issues donnant les six faces possibles.



- $\bigl\{\emptyset, Ω\bigr\}$ est la *tribu grossière*. On ne pourra
  s'intéresser qu'aux évènements 
« *obtenir un numéro entre 1 et  6* » et « *ne pas obtenir de numéro
entre 1 et 6* » ce qui est un peu sommaire;
- $\bigl\{ ∅,A,\complement_Ω A,Ω\bigr\}$ avec A associé à   «*obtenir un
  6* » permet d'ajouter deux événements: la tribu est plus *fine*.
- $𝔓\bigl(Ω\bigr)$ est ce qui se peut faire de plus fin et sera la tribu par
  défaut.



!!! {{ exercice()}}
  
    === "Question"
        Vérifiez qu'il s'agit bien de tribus.

    === "Aide"
    	Il s'agit de vérifier les stabilités, $Ω$ appartenant à chaque fois aux ensembles.


Considérons  par  exemple une  pièce  de  monnaie  qu'on lance  sans  contrainte
d'arret.  Notons $A_n$  l'événement «*PILE  apparaît  pour la  première fois  au
$n$-ème lancer*»  et soit $A$ l'événement  «*PILE apparaît au moins  une fois au
cours des lancers*».

- $ω∈A$ si, et seulement si, il existe un entier naturel non nul $n$ tel que $ω∈A_n$ donc
  $A=\displaystyle\bigcup_{n∈ℕ^*}A_n$. 
- $(A_n)_{n∈N^*}$ ne constitue pas un système complet d'événements. Les éléments
  de la  famille sont bien deux  `a deux disjoints mais  par exemple l'événement
  «*n'obtenir que des FACE*» n'est pas inclus dans $\displaystyle\bigcup_{n∈ℕ^*}A_n$.


!!! {{definition("tribu engendrée par une famille d'événements")}}
    Soit   $\mathscr{F}\subseteq  \mathfrak{P}(Ω)$   une  famille   d'événements
    (i.e. de parties  de $Ω$). On appelle **tribu  engendrée** par $\mathscr{F}$
    la plus petite tribu contenant $\mathscr{F}$.



!!! {{ exercice()}}

    === "Question"

        Soit $A$ une partie de $Ω$.  Quelle est la tribu engendrée par $A$?

    === "Aide"

        La tribu contient obligatoirement  $Ω$ et son complémentaire $\emptyset$
        puis $A$ et son complémentaire  $\overline{A}$. Or ces quatre événements
        forment une tribu: c'est donc la plus petite tribu contenant $A$. 



!!! {{ exercice()}}

    === "Question"

        Soit $Ω=\{1,2,3,4,5\}$ et $A=\bigl\{\{1,2\},\{3,4\}\bigr\}$ 
		Quelle est la tribu engendrée par $A$?

    === "Solution"

        La tribu contient obligatoirement  $Ω$ et son complémentaire $\emptyset$
        puis les éléments de $A$ et leurs complémentaires:
		
		- $\{1,2\}$ et $\overline{\{1,2\}}=\{3,4,5\}$
		- $\{3,4\}$ et $\overline{\{3,4\}}=\{1,2,5\}$
		- les  réunions non  triviales: $\{1,2\}\cup\{3,4\}=\{1,2,3,4\}$  et son
		  complémantaire $\overline{\{1,2,3,4\}}=\{5\}$.
		  
	    Finalement,   on    vérifie   que   $\left\{Ω,\emptyset,\{1,2\},\{3,4\},
	    \{3,4,5\},\{1,2,5\},\{1,2,3,4\},\{5\} \right\}$ est une tribu: c'est la
	    tribu engendrée par $A$

### Espaces probabilisés

!!! {{ definition("Axiomes de KOLMOGOROV - Espaces probabilisés")}}
	  Soit $\left(Ω ,{\mathscr T} \right)$ un espace probabilisable. On appelle
	  **mesure de probabilité**  (ou probabilité tout court)  une application $ℙ$
	  définie sur ${\mathcal T}$ vérifiant: 
	  
	  - $∀ A  ∈  {\mathscr T},\ ℙ(A) ∈  [0,1]$;
	  - $ℙ(Ω )=1$;
	  -  Quelle  que  soit  la  suite $(A_n)_{n∈ℕ}$  d'événements  *deux  à  deux
	    incompatibles*,
	    
		$$
		ℙ\left(\displaystyle\bigcup_{n=0}^{+\infty}A_n\right)=\displaystyle\sum_{n=0}^{+\infty}ℙ(A_n)
		$$
		
		Cette dernière propriété est appelée **$σ$-additivité** de $ℙ$.
	  
	  On dit alors que $\left(Ω ,{\mathcal T},ℙ\right)$ est un **espace probabilisé**.


Comme  vu dans  le  cas des  univers  finis, on  retiendra  que la  modélisation
probabiliste  consiste à  décrire une  expérience aléatoire  par la  donnée d'un
espace de probabilité.

Pour  un   même  espace  probabilisable,   on  peut  avoir   différents  espaces
probabilisés. Considérez par exemple le jeu de Pile ou Face. Pour une même tribu
définie sur $Ω={P,F}$,  on aura différentes probabilités selon que  la pièce est
éqiolibrée ou non.


Un nouveau problème apparaît par rapport aux probabilités sur des ensembles
finis: on manipule à présent des sommes de série...

On remarquera que la $σ$-additivité assure que la série $\sum ℙ(A_n)$ converge.




!!! warning "Pas d'équiprobabilité sur des univers dénombrables!"
    Soit  $Ω=\bigl\{ω_0,ω_1,\dots,ω_n,\dots\bigr\}$ un  ensemble dénombrable  et
    $(Ω,\mathscr{T}_Ω,ℙ)$ un espace probabilisé et 
    notons $p_n=ℙ\left(\{ω_n\}\right)$ pour tout entier $n$.
    La famille $\bigl(\{ω_n\}\bigr)_{n∈ℕ}$ constitue un système complet d'événements.
    

    -     si     tous     les     $p_n$    sont     égaux     à     0,     alors
    $0=\displaystyle\sum_{n=0}^{+\infty}p_n=ℙ\left(\displaystyle\bigcup_{n=0}^{+\infty}\{ω_n\}\right)=ℙ(Ω)=1$
    ce qui est contradictoire.
    - si  tous les $p_n$  sont égaux à  un réel $p>0$,  alors la série  de terme
    général $p_n$ diverge grossièrement ce qui constitue une contradiction.

    Ainsi il ne peut y avoir équipriobabilité sur un univers dénombrable.


!!! {{ exercice()}}
         On tire aléatoirement un nombre entier de $ℕ^*$. On définit sur $ℕ^*$ la fonction:
         $ℙ: n\mapsto \frac{1}{2^n}$


         1. Vérifier que $(ℕ^*, \mathfrak{P}(ℕ^*), ℙ)$ est un espace probabilisé.
         2. Calculer la probabilité d'obtenir un nombre s'écrivant avec au moins 2
               chiffres.
         3. Calculer la probabilité d'obtenir un nombre multiple de 3.
         4. Calculer la probabilité d'obtenir un nombre dont le reste dans la division
         par 3 est 2.
		 
		 
		 
!!! {{ theoreme("Systèmes complets d'événements")}}
     Soit $(Ω,\mathscr{T}_Ω,ℙ)$ un espace probabilisé, soit $A$ un événement de
    $\mathcal{T}_Ω$ et soit $(E_n)_{n∈ℕ}$ un système
    complet d'événements.
	
	- La série $\displaystyle\sum ℙ(E_n)$ converge et  sa somme vaut 1.
	-  La série $\displaystyle\sum ℙ(A∩E_n)$ converge et  sa somme vaut $ℙ(A)$.


### Limite monotone



!!! {{ theoreme("Limite monotone : cas croissant")}}
    Soit  $(Ω,\mathscr{T}_Ω,ℙ)$ un espace probabilisé  et soit $(A_n)_{n∈ℕ}$ une suite
    croissante  (au  sens  de   l'inclusion)  d'événements  de  $\mathscr{T}_Ω$,
    i.e.       $A_0\subseteq      A_1\subseteq\cdots\subseteq       A_n\subseteq
    A_{n+1}\subseteq\cdots$, alors

    $$
    ℙ\left(\displaystyle\bigcup_{n∈ℕ}A_n\right)=\displaystyle\lim_{n\to+\infty}ℙ(A_n)
    $$


 On admettra ce théorème.

!!! {{ theoreme("Limite monotone : cas décroissant") }}
    Soit 
    $(Ω,\mathscr{T}_Ω,ℙ)$ un espace probabilisé  et soit $(A_n)_{n∈ℕ}$ une suite
    décroissante  (au  sens  de   l'inclusion)  d'événements  de  $\mathscr{T}_Ω$,
    i.e.       $A_0\supseteq      A_1\supseteq\cdots\supseteq       A_n\supseteq
    A_{n+1}\supseteq\cdots$, alors
    
    
    $$
    ℙ\left(\displaystyle\bigcap_{n∈ℕ}A_n\right)=\displaystyle\lim_{n\to+\infty}ℙ(A_n)
    $$

On peut démontrer ce théorème en utilisant  le précédent : on utilise la loi
    de DE MORGAN et on passe au complémentaire.


### Un exemple perturbant

!!! {{ exercice()}}

    On lance un dé une infinité de fois (!?). Quelle est la probabilité que l'on
    ne tombe jamais sur 6?

Voici un énoncé un peu perturbant...

Notons $A_n$ l'événement : «*la face 6 n'est pas apparue au cours des $n$
premiers lancers* ». 

Parmi les  $6^n$ lancers possibles,  $5^n$ excluent la face  6. Comme le  dé est
équilibré, les événements élémentaires sont équiprobables donc 

$$
ℙ(A_n)=\frac{5^n}{6^n}
$$


Soit maintenant $A$ l'événement « *la face 6 n'apparaît jamais* ». Alors $A$ est
réalisé si, et seulement si, tous les $A_n$ sont réalisés. Ainsi

$$
A=\displaystyle\bigcap_{n=1}^{+\infty}A_n
$$


D'autre part, si le 6 est apparu lors  des $n$ premiers lancers, il est apparu a
fortiori dans les $n+1$ premiers lancers. Ains, pour tout entier $n∈ℕ^*$, on a 

$$
A_{n+1} \subseteq A_n
$$

La suite $(A_n)_{n∈ℕ^*}$  est donc décroissante au sens  de l'inclusion. D'après
le théorème de la limite monotone,

$$
ℙ(A)=\displaystyle\lim_{n\to+\infty}ℙ(A_n)=\displaystyle\lim_{n\to+\infty}\left(\frac{5}{6}\right)^n=0
$$


On  obtient un  résultat  assez étonnant:  il existe  de  nombreuses suites  de
lancers où 6 n'apparaît  pas, et pourtant la probabilité de ne  pas tomber sur 6
est nulle. 

Il  faut  remarquer que  l'univers  est  $\{1,2,3,4,5,6\}^{ℕ^*}$ qui  n'est  pas
dénombrable, d'où le trouble.



Il faut adapter notre vocabulaire:


!!! {{ definition("Événement presque sûr/négligeable") }}
    Soit    $(Ω,\mathscr{T}_Ω,ℙ)$     un    espace    probabilisé     et    soit
    $(E_1,E_2)∈\mathcal{T}_Ω^2$.
	
	- Si $ℙ(E_1)=1$, on dit que $E_1$ est un événement **presque sûr**.
	- Si $ℙ(E_2)=0$, on dit que $E_2$ est un événement **négligeable**.
	- Si une proposition est vraie sur événement (ensemble d'éléments de $Ω$) de probabilité 1, on
	  dit qu'elle est  vraie **presque sûrement** et alors  l'ensemble des $ω∈Ω$
	  pour lesquels la propriété est fausse est négligeable.
	  

Pour illustrer ces notions, considérons l'exemple suivant:

!!! {{ exercice()}}

    === "Énoncé"

         Soit   $Ω=\{1,2,3,4,5,6\}$   et  $\mathcal{T}_Ω=\mathfrak{P}(Ω)$.    On
			 définit sur $ \mathcal{T}_Ω$  la probabilité suivante: 
		 $ ∀A ∈ \mathcal{T}_Ω \begin{cases}
		 ℙ(A) = 1 \ \text{ si } 6∈A\\
		 ℙ(A) = 0 \ \text{ si } 6\not\in A
		 \end{cases}$
		 
		 1. Vérifiez que l'on a bien défini une probabilité.
		 2.  Déterminez des événements non vides négligeables et des événements
		 strictement inclus dans $Ω$ vrais presque sûrement. 

	=== "Indications"
		 
		 1. Si deux événements $A$ et $B$ sont incompatibles, que peut-on dire de
		    leurs probabilités respectives?

         2. Quelle est par exemple la probabilité que le nombre tiré soit pair? Impair?




## Variables aléatoires

### Définition


En fait, la définition  donnée lors de l'étude sur des  univers finis est encore
valide.


!!! {{ definition("VAR")}}
    Soit  $(Ω,\mathscr{T}_Ω,ℙ)$ un  espace  probabilisé.   On appelle  *variable
    aléatoire réelle* toute *application* de $Ω $ dans $ℝ$ vérifiant: 
	
	$$
	∀ a∈ ℝ,\ X^{-1}(%[
	]-\infty,a%[
	]) ∈ {\mathscr T}_Ω 
	$$
	
	ou en « bon français », tout intervalle $%[
	]-\infty,a%[
	]$ est l'image d'un évènement
	de la tribu de départ, ou encore 
	
	$$
	\{ω∈Ω\ \mid\ X(ω) ≤ a\} ∈ \mathscr{T}_Ω
	$$


!!! {{ exercice()}}

    === "Problème"

        Comment, avec cette simple définition, s'assurer que les cas suivants sont
        encore dans la tribu?

	    - $[X > a]$
	    - $[a < X ≤ b]$
	    - $[X = a]$

	=== "Solutions"

        -  $[X >  a]  = \overline{[X  ≤  a]}$  appartient  à  la tribu  par
          stabilité du passage au complémentaire.
	    - $[a < X ≤ b] = [X ≤ b]  ∩ [X > a]$ appartient à la tribu par stabilité
	      de l'intersection
	    -     Démontrer     par     double    inclusion     que     $[X=a]     =
	      \displaystyle\bigcap_{n=1}^{+\infty}[a-1/n < X ≤ a]$.
		  Le résultat s'en déduit par stabilité des intersections dénombrables.



!!! {{ theoreme("SCE lié à une VAR")}}
     Soit  $(Ω,\mathscr{T}_Ω,ℙ)$ un  espace probabilisé  et X  une VAR  discrète
     définie sur cet espace. Alors $\bigl([X=x]\bigr)_{x∈X(Ω)}$ est un SCE. 
	 
À vérifier:

- appartenance de chaque événement du SCE à $\mathscr{T}_Ω$.
- incompatibilité deux à deux.
- réunion égale à $Ω$.
- on vérifiera que $ \displaystyle\sum _{ x \in X( \Omega) } ℙ(X = x) = 1 $.


Quand on transporte l'univers  dans $X(Ω)$, on transporte la loi  et bien sûr la
tribu:

!!! {{ definition("Tribu associée à une VAR") }}
    Soit $X$ une VAR définie sur un espace probabilisé $(Ω,\mathscr{T}_Ω,ℙ)$. On
    appelle **tribu associée** à $X$ la tribu engendrée par le SCE
    $\bigl([X=x]\bigr)_{x∈X(Ω)}$. On la note $\mathscr{T}_X$.
   
   
À chaque  événement de la  tribu de départ correspond  un événement de  la tribu
transportée.







### Loi d'une VAR discrète

!!! {{ definition("Loi d'une VAR discrète")}}

    Soit $(Ω,\mathscr{T}_Ω,ℙ)$ un espace probabilisé et $X$ une VAR discrète sur
    $Ω$. 
	
	On pose que $X(Ω)=\{x_1,x_2,\dots,x_n,\dots\}$ avec $x_1<x_2<\cdots$
    et $\displaystyle\lim_{n\to+\infty}x_n=+\infty$.

    On appelle **loi de la VAR** X la suite $(p_n)_{n∈ℕ^*}$ de réels définie par:
   
    $$
    p_n=ℙ\bigl(\{ω∈Ω\ \mid\ X(ω)=x_n\}\bigr)=ℙ([X=x_n])
    $$

Caractériser la loi  d'une VAR discrète $X$, c'est donner  les éléments $x_i$ de
$X(Ω)$ **et** la valeur des $p_i$.


!!! {{ theoreme("Caractéristique d'une loi")}}

    Soit $X$ une VAR définie sur un espace probabilisé $(Ω,\mathscr{T}_Ω,ℙ)$. 
	On pose que $X(Ω)=\{x_1,x_2,\dots,x_n,\dots\}$ avec $x_1<x_2<\cdots$ et
	$\displaystyle\lim_{n\to+\infty}x_n=+\infty$. 

	Alors les réels $p_n=ℙ([X=x_n])$ vérifient:

       1. $(∀n∈ℕ^*)(p_n \geqslant 0)$
       2. $\displaystyle\sum_{n=1}^{+\infty}p_n=1$



!!! {{ theoreme("Caractérisation d'une loi")}}

    Réciproquement, soit $Ω$ un univers dénombrable, soit $(x_n)_{n∈ℕ^*}$ une
	suite strictement croissante 
	tendant vers $+\infty$ et soit $(p_n)_{n∈ℕ^*}$ une suite de réels
	vérifiant les conditions **1.** et **2.** précédentes.

    Il existe alors une VAR $X$ définie sur $Ω$ dont la loi est définie par:

	   * $X(Ω) = \{x_1, x_2,\dots\}$
	   * $(∀ n∈ℕ^*)(ℙ([X=x_n])=p_n)$



!!! {{ exercice()}}

    === "Énoncé"

        Soit $λ>0$. À quelle condition sur $α$ les réels $p_n=α×\dfrac{λ^n}{n!}$ pour $n∈ℕ$
        sont-ils les coefficients d'une loi de probabilité?

	=== "Solution"

        Rappel: $\displaystyle\sum_{n=0}^{+\infty}\dfrac{λ^n}{n!}={\rm e}^λ$.

!!! {{ exercice()}}

    === "Énoncé"

        Démontrer que les  réels $p_n=\dfrac{1}{n(n+1)}, n \geqslant  1$ sont les
        coefficients d'une loi de probabilité.

	=== "Solution"

        Somme télescopique




### Fonction de répartition


Rappelons la définition, toujours valable dans le cas d'un univers dénombrable:


!!! {{ definition("Fonction de répartition") }}
    Soit $X$ une VAR définie sur un espace probabilisé $(Ω,\mathscr{T}_Ω,ℙ)$. 
    On appelle **fonction de répartition** de $X$ la fonction $F_X$ définie sur
    $ℝ$ par:
   
    $$
    (∀x∈ℝ)(F_X(x)=ℙ([X ≤ x]))
    $$


Et les propriétés suivantes:


!!! {{ theoreme("Propriétés de la fonction de répartition")}}

    === "Énoncé"

         Soit  $X$   une  VAR  discrète   définie  sur  un   espace  probabilisé
         $(Ω,\mathscr{T}_Ω,ℙ)$.
		 La fonction de répartition $F_X$ vérifie les propositions suivantes:
		 
		 1. $F_X$ est croissante sur $ℝ$.
	     2.  $(∀x∈ℝ)(F_X(x)=\displaystyle\sum_{\substack{x_k∈X(Ω)\\
	        x_k≤x}}ℙ([X=x_k]))$
         3.  $\displaystyle\lim_{x\to+\infty}F_X(x)=1, \
	        \displaystyle\lim_{x\to-\infty}F_X(x)=0$
         4. $F_X$ est continue à droite en tout point de $ℝ$.
	     5.  $(∀x∈ℝ)(F_X(x) - \displaystyle\lim_{\substack{t\to x\\ t <
	       x}}F_X(t)=ℙ([X=x]))\hspace{2em}$
	       En particulier, $F_X$ est continue en tout point de $ℝ\setminus X(Ω)$.

	=== "Démonstration"
	
	     1. Soit $x$ et $y$  deux réels tels que $ x \leq y $.  Alors $ [ X \leq
	        x ] \subseteq [ X \leq y ] $, donc par croissance de la probabilité, $
	        F_X (x)  \leq F_X (y) $.  La fonction $F_X$ est  donc croissante sur
	        $ℝ$. 
		 2. Déjà vu
		 3. La fonction $F_X$ est croissante sur $ℝ$ et majorée par $1$. Par le
		    théorème de la limite monotone (pour les fonctions), elle admet donc
		    une limite finie en $+\infty$, et on trouve par composée: 
            
			$$
			\displaystyle\lim_{ t \to +\infty }  F_X (t) = \displaystyle\lim_{ n
			\to +\infty } F_X( n) 
			$$
			
			La suite $  ([ X \leq n  ])_{n \in ℕ^*} $ est  une suite croissante
			d'événements, donc d'après  le théorème de la  limite monotone (pour
			les probabilités), 
			
			$$
			\displaystyle\lim_{n \to  +\infty} ℙ( [X  \leq  n])  = ℙ( \bigcup_{n=1}
			^{+\infty} \left[ X \leq n \right]) = ℙ( \Omega )
			$$
			
			Donc $ \displaystyle\lim _{ t \to +\infty } F_X (t)  = ℙ( \Omega ) = 1 $, et $
			\displaystyle\lim _{ t \to +\infty } F_X (t) = 1 $. 
			
			Pour l'étude en $-\infty$, on agit  de même en considérant a suite $
			([  X \leq  -n  ])_{n \in  ℕ^*}  $ qui  est  une suite  décroissante
			d'événements, donc d'après le théorème  de limite monotone (pour les
			probabilités):
			
			$$
			\displaystyle\lim_{n   \to    +\infty}   ℙ(    [X   \leq    -n])   =
			ℙ( \bigcap_{n=1}^{+\infty} \left[ X \leq -n \right]) = ℙ( \emptyset)
			= 0
			$$



On rappelle enfin le lien fondamental entre loi et fonction de répartition:

!!! {{ theoreme("Lien entre loi et fonction de répartition")}}
    Soit   $X$   une   VAR   discrète  définie   sur   un   espace   probabilisé
    $(Ω,\mathscr{T}_Ω,ℙ)$.
	On pose $X(Ω)=\{x_1,x_2,\dots\}$ avec  $(x_n)_{n∈ℕ^*}$ une suite strictement
	croissante tendant vers $+\infty$.
	
	1. $\Bigl(∀x∈ℝ\Bigr)\Bigl(F_X(x)=\displaystyle\sum_{\substack{x_k∈X(Ω)\\
	   x_k≤x}}ℙ([X=x_k])\Bigr)$
	2. $\Bigl(∀x_k∈X(Ω)\Bigr)\Bigl(ℙ([X=x_k])=F_X(x_k)- \displaystyle\lim_{\substack{x\to   x_k\\    x   <
	   x_k}}F_X(x)\Bigr)$


Dans certaines situations, il est en effet plus simple de déterminer la fonction
de répartition plutôt que la loi  (nous avons déjà rencontré une telle situation
dans le  cas fini  : on  tire une boule  dans $n$  urnes, chacune  contenant $N$
boules numérotées  de 1 à $N$.  On a cherché  la loi de  la VAR X égale  au plus
grand des numéros tirés en calculant d'abord sa fonction de répartition).


## Moments d'une VAR discrète

 ![Placeholder](./IMG/Chebyshev.jpeg){ align=right  width=200}
C'est Пафну́тий Льво́вич ЧЕБЫШЁВ (1821-1894) qui introduisit au milieu du XIXe siècle la
notion de moment  pour étudier les variables aléatoires à  partir de ces indices
qui donnent des  éléments d'analyse de la  loi : l'espérance et  la variance que
nous avons rencontrées mais aussi le moment d'ordre 3 qui mesure l'asymétrie et
le moment d'ordre  4 (le *kurtosis*) qui mesure la  concentration à proximité de
la moyenne. 

Aux concours, les moments seront surtout  un pretexte pour étudier des séries et
bientôt des intégrales généralisées.



### Espérance : le moment d'ordre 1

!!! {{ definition("Espérance") }}
    Soit   $X$   une   VAR   discrète  définie   sur   un   espace   probabilisé
    $(Ω,\mathscr{T}_Ω,ℙ)$.
	  
	On   pose   $X(Ω)=\{x_1,x_2,\dots\}$   avec  $(x_n)_{n∈ℕ^*}$   une   suite
	strictement croissante tendant vers $+\infty$.
	  
	On dit que  $X$ **admet une espérance** si  la série $\displaystyle\sum_{n
	≥1}x_n\cdot ℙ([X=x_n])$ est **absolument convergente**.
	  
	On appelle alors *espérance de $X$* le réel:
	  
	$$
	\mathbb{E}(X)=\displaystyle\sum_{n=1}^{+\infty}x_n\cdot
	ℙ([X=x_n])=\displaystyle\sum_{n=1}^{+\infty}x_n\cdot p_n
	$$



!!! warning "Existence de l'espérance"
    Ainsi, sur des espaces dénombrables, l'espérance n'existe pas forcément : il
    faut vérifier  que $\displaystyle\sum_{n≥1} |x_n|\cdot  ℙ([X=x_n])$ converge
    et dans ce cas $\mathbb{E}(X)=\displaystyle\sum_{n≥1} x_n\cdot ℙ([X=x_n])$


!!! {{ exercice()}}

    === "Énoncé"

        Soit    $X$   une    VAR   de    loi   $X(Ω)=ℕ^*$    et   $ℙ([X=k])    =
        \dfrac{1}{k(k+1)}$. Étudier l'éventuelle espérance de $X$.

	=== "Indications"

         La série harmonique diverge
		 
		 
!!! {{ exercice()}}

    === "Énoncé"

        Même question avec $X(Ω)=ℕ$ et $ℙ([X=k])={\rm e}^{-λ}\dfrac{λ^k}{k!}$.

	=== "Indications"

         Faire apparaître la série exponentielle modulo un changement d'indice
		 


### Moments d'ordre quelconque

!!! {{ definition("Moments d'ordre quelconque") }}
    Soit $X$ une VAR discrète définie sur un espace probabilisé
    $(Ω,\mathscr{T}_Ω,ℙ)$.
	  
	On   pose   $X(Ω)=\{x_1,x_2,\dots\}$   avec  $(x_n)_{n∈ℕ^*}$   une   suite
	strictement croissante tendant vers $+\infty$.
	  
	On dit que $X$ **admet un moment d'ordre $s$** si la série $\displaystyle\sum_{n
	≥1}x_n^s\cdot ℙ([X=x_n])$ est **absolument convergente**.
	  
	On appelle alors *moment d'ordre $s$ de $X$* le réel:
	  
	$$
	m_s(X)=\displaystyle\sum_{n=1}^{+\infty}x_n^s\cdot
	ℙ([X=x_n])=\displaystyle\sum_{n=1}^{+\infty}x_n^s\cdot p_n
	$$



!!! {{ exercice()}}

    === "Énoncé"

          Soit    $X$   une    VAR   de    loi   $X(Ω)=ℕ$    et   $ℙ([X=k])={\rm
          e}^{-λ}\dfrac{λ^k}{k!}$.
		  
		  1.  Démontrez que  $X$ admet  un moment  $m_s(X)$ à  tout ordre  $s$,
		     $s∈ℕ^*$.
		  2. Calculez le moment d'odre 2.

	=== "Indications"

         1. $n^s\cdot ℙ([X=n])=\mathscr{o}\left(\dfrac{1}{n^2} \right)$
		 2. $n^2=n(n-1) + n$


Cet exemple nous incite à énoncer le théorème suivant:

!!! {{ theoreme("")}}

    === "Énoncé"

          Soit $X$ une VAR discrète admettant un moment à un ordre $s$, $s∈ℕ^*$, alors
          $X$ admet un moment à tout ordre $r≤s$. 
		  
	=== "Indications"
	
	     Démontrer que pour tout entier  naturel $n$, $|x_n|^r \leqslant 1 +
             |x_n|^s$ en distingant $|x_n|>1$ et $|x_n| \leqslant 1$.

### Moments centrés


!!! {{ definition("Moments centrés d'ordre quelconque") }}
    Soit $X$ une VAR discrète définie sur un espace probabilisé
    $(Ω,\mathscr{T}_Ω,ℙ)$ et **admettant une espérance**.
	  
	On   pose   $X(Ω)=\{x_1,x_2,\dots\}$   avec  $(x_n)_{n∈ℕ^*}$   une   suite
	strictement croissante tendant vers $+\infty$.
	  
	On dit que $X$ **admet un moment centré d'ordre $s$** si la série $\displaystyle\sum_{n
	≥1}\bigl(x_n-\mathbb{E}(X)\bigr)^s\cdot ℙ([X=x_n])$ est **absolument convergente**.
	  
	On appelle alors *moment centré d'ordre $s$ de $X$* le réel:
	  
	$$
	μ_s(X)=\displaystyle\sum_{n=1}^{+\infty}\bigl(x_n-\mathbb{E}(X)\bigr)^s\cdot
	ℙ([X=x_n])=\displaystyle\sum_{n=1}^{+\infty}\bigl(x_n-\mathbb{E}(X)\bigr)^s\cdot p_n
	$$
	

On rappelle que le moment centré d'ordre 2 est appelé **variance** de $X$ et on
le  note $\mathbb{V}(X)$.  Sa racine  carrée est  appelée **écart-type**  de $X$
qu'on note $σ(X)$.


!!! {{ exercice()}}

    === "Énoncé"

          Soit    $X$   une    VAR   de    loi   $X(Ω)=ℕ$    et   $ℙ([X=k])={\rm
          e}^{-λ}\dfrac{λ^k}{k!}$.
		  
		  Calculez $\mathbb{V}(X)$.

	=== "Indications"

         $(k-λ)^2=k(k-1) + (1-2λ)k +λ^2$.
		 
		 
## VAR fonction d'une autre VAR


### Loi d'une VAR du type *f*(X)

!!! {{ theoreme("Fonction d'une VAR")}}

    === "Énoncé"
	
          Soit $X$ une VAR discrète définie sur un espace probabilisé
          $(Ω,\mathscr{T}_Ω,ℙ)$.
	
	      On pose $X(Ω)=\{x_1,x_2,\dots\}$.
	
	      Soit  $f:  \mathcal{D}_f\subseteq  ℝ  ⟶  ℝ$  une  fonction  telle  que
	      $X(Ω)\subseteq \mathcal{D}_f\subseteq ℝ$.
	
	      Alors la fonction $Y=f(X)$ est aussi une VAR définie sur
	      $(Ω,\mathscr{T}_Ω,ℙ)$.
	
	      De plus, $f\bigl(X(Ω)\bigr)=\{f(x_1),f(x_2),\dots\}$, des valeurs $f(x_k)$
	      pouvant être répétées.
	
	      Enfin, pour tout $y_j∈f\bigl(X(Ω)\bigr)$, $ℙ([Y=y_j])=ℙ([f(X)=y_j]) =
	      \displaystyle\sum_{f(x_k)=y_j}^{}ℙ([X=x_k])$ 
	
	=== "Démonstration"

         La fonction  $ Y  = f  \circ X $  est définie  sur $\Omega$  (car $X(Ω)
         \subseteq D_f$) à valeurs réelles. De plus, $ \forall x \in
         ℝ $,
		 
		 $
		 [    Y   \leq    x   ]    =    [   f    \circ    X   \leq    x   ]    =
		 \displaystyle\bigcup_{\substack{y\in Y(\Omega)\\ y \leq x}} [ f(X)
		 =  y  ]  =  \displaystyle\bigcup_{\substack{y \in  Y(\Omega)\\  y  \leq
		 x}}\left(\displaystyle \bigcup_{\substack{z\in X(\Omega)\\ f(z)=y}} [ X
		 = z ] \right)
		 $
		 
		 Or $X$ est une variable aléatoire, donc $[ X = z ] \in \mathcal{T}_Ω$. 
		 Comme $X(\Omega)$  est dénombrable, la  stabilité des tribus  par union
		 dénombrable donne $[ Y \leq x ] \in \mathcal{T}_Ω$.
		 Donc $Y$ est bien une variable aléatoire. 
		 
		 De plus, comme $X(\Omega)$  est dénombrable, $Y(\Omega) = f(X(\Omega))$
		 est dénombrable ou fini car il peut y avaoir des répétitions. 

En             pratique,              on             utilise             souvent
$[Y=y]=\displaystyle\bigcup_{\substack{x∈X(Ω)\\f(x)=y}}[X=x]$.  

Par exemple:

- si $Y=X^2$, $[Y=4] = [X=2]∪[X=-2]$.
- si $Y=\ln X$, $[Y=0] = [X=1]$
- ...

!!! {{ exercice()}}

    === "Énoncé"

         Soit un réel $λ$. On  pose, pour tout $ n \in ℤ \setminus  \{ 0 , 1 \}
         $, $ p_n  = \dfrac λ{n(n-1)} $. Montrer qu'il  existe une unique valeur
         de $λ$  telle que  la suite  $\mathbf{p}$ soit la  loi d'une  variable aléatoire
         réelle discrète $X$. 
		 
		 Déterminer la loi de $ Y = | X | $. 

	=== "Indications"

         $\displaystyle\sum _{ n \in ℤ \setminus \{  0 , 1 \} } \dfrac λ{n(n-1)}
         =    \displaystyle\sum_{n=2}    ^{+\infty}     \dfrac    λ{n(n-1)}    +
         \displaystyle\sum _{n=1} ^{+\infty} 
         \dfrac λ{-n(-n-1)}$
		 
		 $\forall n \geq 2,  \quad [Y=n] = [X=n] \cup [X=-n]  ,\quad \text{ et }
		 [Y = 1] = [ X = -1 ]$
		 
		 

		 
### Théorème de transfert


!!! {{ theoreme("Théorème de transfert")}}

    === "Énoncé"
		Soit $X$ une VAR discrète définie sur un espace probabilisé $(Ω,\mathscr{T}_Ω,ℙ)$.
	
		On pose $X(Ω)=\{x_1,x_2,\dots\}$.
	
		Soit $f: \mathcal{D}_f\subseteq ℝ ⟶ ℝ$ une fonction telle que $X(Ω)\subseteq
		\mathcal{D}_f$.
		

	    La VAR $Y=f(X)$ admet une espérance si, et seulement si, la série $
		\displaystyle\sum_{}^{}f(x_k)ℙ([X=x_k])$ est **absolument convergente**.
		Dans ce cas 
	
		$$
		\mathbb{E}(Y)=\displaystyle\sum_{k=1}^{+\infty}f(x_k)ℙ([X=x_k]) 
		$$

	=== "Démonstration"

        On remarque que $ Y(\Omega) = f(X(\Omega))  $. Soit $ y \in Y(\Omega) $,
        on pose $ F_y = \{ x \in X(\Omega) | f(x) = y \} $. On a alors: 

	    $$ 
		\forall y \in Y(\Omega) , \quad ℙ([Y=y]) = \displaystyle\sum_{x \in F_y} ℙ([X = x]) \quad
		\text{ donc } \quad y× ℙ([Y=y]) = \displaystyle\sum_{x \in F_y} f(x) ×ℙ([X = x]) 
		$$
 
        où  les  sommes  sont  dénombrables et  convergent  par  propriétés  des
		probabilités.  
		
		De plus, $ \displaystyle\bigcup_{y \in Y(\Omega)} F_y = X(\Omega) $
        (union dénombrable), et les $F_y$ sont incompatibles deux à deux.
		
		On
        obtient donc par $\sigma$-additivité des probabilités que $ \displaystyle\sum \left|y×
        ℙ([Y=y])\right|$  converge  si  et   seulement  si  $  \displaystyle\sum
        \left|f(x)× ℙ(X=x)\right| $ converge et que,
        sous réserve de convergence, 

	    $$
		\mathbb{E}(Y)   =   \displaystyle\sum_{y×   \in  Y(\Omega)}   y×ℙ(Y=y)   =
		\displaystyle\sum_{y \in Y(\Omega)} 
		\left( \displaystyle\sum_{x \in F_y} f(x)× ℙ([X=x]) \right) = \displaystyle\sum_{x
		\in X(\Omega)} f(x)× ℙ([X=x]) 
		$$



!!! info "Remarques"

	- Le calcul de  $\mathbb{E}(Y)$ ne nécessite pas de connaître  la loi de $Y$
	  mais seulement celle de $X$.

    - Vous aurez remarqué que $m_s(X)=\mathbb{E}(X^s)$.


!!! {{ exercice()}}

    === "Énoncé"
         
		$%[$ 
        Soit $p∈]0,1[$. On pose $q=1-p$. Soit $X$ une VAR dont la loi est donnée
        par:
		$%]$
		
		$$
		\begin{cases}
		X(Ω) = ℕ^*\\
		ℙ([X=k]) = p\cdot q^{k-1}
		\end{cases}
		$$
		
		1. Vérifiez qu'il s'agit bien d'une loi de probabilité.
		2. Soit $m$ un entier naturel non nul. On pose $Y=\min(X,m)$. 
		     1. Déterminez $Y(Ω)$. Déduisez-en que $Y$ admet une espérance.
		     2. Calculez $\mathbb{E}(Y)$.

	=== "Indications"

         - $Y(Ω)=[\![ 1,m ]\!]$ 
		 -                  $\mathbb{E}(Y)=\displaystyle\sum_{k=1}^{m}k×ℙ([X=k])
		   +\displaystyle\sum_{k=m+1}^{+\infty}m×ℙ([X=k])$ 
		   
		   On  se souviendra  du  calcul  de la  somme  d'une série  géométrique
		   dérivée.
		   
		   

### Moment d'une transformation affine de VAR

!!! {{ theoreme("")}}

    === "Énoncé"
	
		- Soit $X$ une VAR discrète admettant une espérance. Alors $
		   ∀(a,b) ∈ℝ^2, \ \mathbb{E}(aX+b) = a\mathbb{E}(X) + b
		   $
		   
        - Soit $X$ une VAR discrète admettant une variance. Alors
		   $
		   ∀(a,b) ∈ℝ^2, \ \mathbb{V}(aX+b) = a^2\mathbb{V}(X)
		   $
		  
	=== "Indications"
		
		Les  séries   $  \displaystyle   \sum  x_k   ℙ([X  =   x_k])  $   et  $
		 \displaystyle\sum ℙ([X = x_k]) $ convergent 
		 absolument,  donc par  *inégalité triangulaire*  et linéarité  des séries
		 convergentes $ \displaystyle\sum (a x_k + b) ℙ([X = x_k]) $ converge absolument. On en
		 déduit par théorème de transfert que $aX + b$ admet une espérance qu'il
		 reste à calculer par linéarité.
		 

Le  théorème  de  König-Huyghens  est  lui aussi  encore  valable  dans  le  cas
dénombrable.


On se souviendra également des lois centrées, réduites et centrées-réduites.

## Deux nouvelles lois discrètes

### Loi géométrique

#### Situation type

On considère une pièce de monnaie non nécessairement équilibrée: on tombe sur Pile
avec une probabilité $p$, avec $0<p<1$ et Face avec une probabilite $q=1-p$.

On effectue une série infinie de lancers de cette pièce de manière indépendante.

On note $X$ la VAR égale au rang d'apparition du premier Pile.

Est-on sûr que Pile apparaisse? 

Soit $A_n$  l'événement « Pile  n'apparaît toujours pas  après $n$ lancers  » et
soit $A$ l'événement « Pile n'apparaît jamais ».

On vérifie  que la suite  $(A_n)_{n∈ℕ^*}$ est décroissante, que  $ℙ(A_n)=q^n$ et
que $A= \displaystyle\bigcap_{n=1}^{+\infty} A_n$. Alors

$$
ℙ(A)=                                            \displaystyle\lim_{n\to+\infty}
ℙ(A_n)=\displaystyle\lim_{n\to+\infty}q^n=0
$$

car $|q|<1$. Il est donc presque sûr que Pile apparaisse.


Soit $k ∈ℕ^*$. $[X=k]$ est réalisé si les $k-1$ premiers lancers donnent Face et
le $k$^e^ Pile. Donc $ℙ([X=k]) =q^{k-1}×p$.


#### Définition

!!! {{ definition("Loi géométrique") }}
    Soit  $X: Ω⟶ℝ$  une VAR.  On dit  que $X$  suit une  **loi géométrique**  de
    paramètre $p$ si:
	  
	  1. $X(Ω)=[\![1, +\infty[\![ %]]]]$.
	  2. $∀k∈X(Ω),\ ℙ([X=k]) = p×q^{k-1}$ avec $q=1-p$.
	  
	On note $X\leadsto \mathcal{G}(p)$.

#### Moments

!!! {{ theoreme("Moments d'une loi géométrique")}}

    === "Énoncé"

        Soit $X\leadsto \mathcal{G}(p)$. Alors:
		
		1. $\mathbb{E}(X)$ existe et vaut $ \dfrac{1}{p} $
		2. $\mathbb{V}(X)$ existe et vaut $ \dfrac{q}{p^2} $

	=== "Indications"

         1.  $k×ℙ([X=k]) =  p×(k×q^{k-1})$. On  se  souvient alors  de la  série
			 géométrique    dérivée    et     on    trouve    $    \mathbb{E}(X)
			 =\dfrac{p}{(1-q)^2} $
		2.   $   \mathbb{V}(X)   =    \mathbb{E}(X(X-1))   +   \mathbb{E}(X)   -
		   \left( \mathbb{E}(X) \right)^2$ et on se souvient  de la série
			 géométrique dérivée seconde.
			 
			 
#### Scilab

Avec `grand`:

```scilab
--> X=grand(1, 5, "geom", 0.5)
 X  =

   1.   1.   3.   3.   1.

```
		   

Avec uniquement `rand`:


```scilab
--> function X = geo(p) 
       X = 1 
       U = rand(1) 
       while U> p do 
           X = X + 1 
           U = rand(1) 
       end 
    endfunction 

--> N = 10; 
 
--> G = zeros(N, 1);
 
--> p = 0.5; 

--> for k = 1:N
      G(k) = geo(p); 
     end;
 
--> G 
 G  = 

   1. 1. 1. 1. 2. 1. 2. 3. 1. 2.
```


#### Problème du collectionneur



!!! {{ exercice()}}
    Voici une application classique de la loi géométrique: en Syldavie,le
	petit Ivan collectionne les images du  chocolat Wonka. Il y a $n$ images
	différentes.  Lorsqu'il aura  obtenu les  $n$ images,  il aura  droit de
	visiter les  usines Wonka. À chaque  fois qu'il achète  une tablette, la
	probabilité  d'obtenir une  image quelconque  est $1/n$.  Soit  $T_n$ le
	temps d'attente (en  nombre de tablettes achetées) pour  obtenir les $n$
	images. Calculez $ \mathbb{E}(T_n)$.


Soit $X_i$  la variable aléatoire correspondant au  temps d'attente pour
passer     de    $i-1$     images    différentes     à     $i$    images
différentes. 

Quand on est dans l'état $i-1$, on dispose de $i-1$ images
différentes.  Si on tombe  sur l'une  des $i-1$  images dont  on dispose
(avec la probabilité $\dfrac{i-1}{n}$), on reste dans l'état $i-1$, sinon, on passe à l'état $i$ (avec la
probabilité $\dfrac{n-(i-1)}{n})$.  

 Ainsi, $X_i\leadsto \mathcal{G}(p_i)$ avec $p_i=\dfrac{n-(i-1)}{n}$.  

On en déduit que $\mathbb{E}(X_i)=\dfrac{1}{p_i}=\dfrac{n}{n+1-i}$.

Or,                    $T_n=X_1+X_2+\cdots +X_n$,                    donc
$\mathbb{E}(T_n)=\mathbb{E}(X_1)+\mathbb{E}(X_2)+\cdots +\mathbb{E}(X_n)$:



$$\mathbb{E}(X_n)=\displaystyle\sum_{i=1}^{n}\dfrac{1}{p_i}=n
\displaystyle\sum_{i=1}^{n}\dfrac{1}{n+1-i}=n
\left(\dfrac{1}{n}+\dfrac{1}{n-1}+\cdots  + \dfrac{1}{2}+1\right)$$

Par exemple, avec $n=10$:


```scilab
-->k = [1:10];

-->10 * sum(1 ./ k)
  ans  =
 
     29.289683  
```

Il faut en moyenne acheter 30 tablettes pour obtenir les dix images.




### Loi de POISSON


#### Exemple historique

 ![Placeholder](./IMG/blitz.jpg){ align=center  width=700%}


#### Que modélise la loi de POISSON ?


Voici comment S.D. POISSON  introduisit la loi qui porte son nom  à partir de la
loi binomiale.


Considérons une probabilité définie par:

$$
p_j(\pi_n,n)=
\begin{cases}
\binom{n}{j}(\pi_n)^j(1-\pi_n)^{n-j}\quad \text{ si } j \leqslant n\\
0 \quad \text{si } j>n
\end{cases}
$$



Ainsi, cette  suite de  probabilités est une  extension sur  $ℕ$ tout
entier de la loi binomiale $\mathcal{B}(n,\pi_n)$.

*Supposons* que  la suite $(\pi_n)$  tende vers 0 quand  $n$ tend
vers l'infini de telle sorte que $\pi_n\sim \dfrac{λ}{n}$.

Il faut également se souvenir que:

$$∀ t ∈ ℝ,\quad 
\displaystyle\lim_{n\to + ∞ }\left(1+\frac{t}{n}\right)^n=\mathrm{e}^{t}
$$

Alors on montre que (regardez votre merveilleux professeur au tableau):

$$
\displaystyle\lim_{n\to + ∞ }p_j(\pi_n,n)=\frac{λ^j\mathrm{e}^{-λ}}{j!}
$$



Ainsi, la loi de Poisson modélise bien le nombre
d'apparitions de phénomènes rares (le  nombre d'étudiants de l'Externat ẃchouant
aux concours, 
le nombre d'étudiants ne travaillant pas leur cours, etc.) dans une suite infinie
d'évènements  ($\pi_n\sim \frac{λ}{n}$  est  *petit*  et $n$
est *grand*).





#### Définition


!!! {{ definition("loi de POISSON") }}
    Soit $X: Ω⟶ℝ$ une VAR.  On dit que $X$ suit une **loi de POISSON** de
    paramètre $λ$ si:
	  
	  1. $X(Ω)=ℕ$.
	  2. $∀k∈X(Ω),\ ℙ([X=k]) = \dfrac{λ ^{k}{\rm e}^{-λ}}{k!}$. 
	  
	On note $X\leadsto \mathcal{P}(λ)$.


#### Moments d'une loi de POISSON





!!! {{ theoreme("Moments d'une loi de POISSON")}}

    === "Énoncé"

        Soit $X\leadsto \mathcal{P}(λ)$. Alors:
		
		1. $\mathbb{E}(X)$ existe et vaut $ λ $
		2. $\mathbb{V}(X)$ existe et vaut $ λ $

	=== "Indications"

	    Nous avons déjà calculé l'espérance et souvenez-vous que $ \mathbb{V}(X)
	    = \mathbb{E}(X(X-1)) + \mathbb{E}(X) - 
		   \left( \mathbb{E}(X) \right)^2$ 



#### Scilab


Avec `grand`:

```scilab
--> Y = grand(1,10,'poi', 0.7)
  Y = 

  0.   1.   0.   0.   1.   3.   2.   4.   2.   3.
```
