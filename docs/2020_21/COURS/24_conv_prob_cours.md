{{ chapitre(24, "Probabilités : convergences et approximations")}}

!!! exo ""
	![Placeholder](./IMG/wendelin.jpg){ align=right width=390}
	Wendelin WERNER,  né en 1968,  est le premier  probabiliste à avoir  reçu la
	prestigieuse médaille  Fields en  2006, 70  ans après  la création  de cette
	distinction.  Il a  notamment travaillé  sur les  marches aléatoires  et les
	mouvements  browniens,  des  notions  qui  ont  aussi  séduits  les  marchés
	financiers et dont l'abus c(st)upide a notamment conduit à la crise financière
	de 2008. À 14 ans, il a également joué aux côtés de Romy SCHNEIDER et Michel
	PICCOLI dans la *passante du sans-souci* de Jacques ROUFFIO.
	
	En ce qui  concerne notre programme, c'est une introduction,  dans le cas de
	lois discrètes simples, à ce qui seradéveloppée au 3e semestre.
	On  va cependant  découvrir un  pont entre  lois discrètes  et continues  et
	comprendre pourquoi les barres de la  loi binomiales ressemblent à la cloche
	de  GAUSS. On  va  surtout mettre  en  place un  théorème  qui va  justifier
	l'approche heuristique des probabilités que vous avez initiée au collège :
	quand  le  nombre  de  répétitions  tend  vers  l'infini,  la  fréquence  de
	réalisation d'un événement converge vers la probabilité de cet événement. Le
	monde ne serait alors que la  limite d'une accumulation de petits phénomènes
	aléatoires. Pour des ECS, ce résultat  est aussi à l'origine des méthodes de
	Monte-Carlo très en vogue en mathématiques financières.
	
	
	


## Programme


### Convergences et approximations

#### Convergence en probabilité

|Notions|Commentaires|
|----|----|
|Inégalités de Markov et de Bienaymé-Tchebychev pour les variables aléatoires discrètes.|Si $X$ est une variable aléatoire positive admettant une espérance, alors pour tout $\lambda>0$: $P(X\geqslant \lambda)\leqslant \dfrac{\mathbb{E}(X)}{\lambda}$. Pour toute variable $X$ admettant espérance et variance, pour tout $\varepsilon>0$: $ℙ(\vert X - \mathbb{E}(X)\vert \geqslant \varepsilon)\leqslant \dfrac{\mathbb{V}(X)}{\varepsilon^2}$.|
|Convergence en probabilité : si $(X_n)$ et $X$ sont des variables aléatoires définies sur $(\Omega,{\cal A}, ℙ)$, $(X_n)$ converge en probabilité vers $X$ si, pour tout $\varepsilon >0$, $\displaystyle \lim_{n\to\infty}ℙ([\vert X_n-X\vert >\varepsilon]) = 0$| Notation $X_n \xrightarrow{P}X$.|
|Loi faible des grands nombres pour la loi binomiale.|Si $(X_n)$ est une suite de variables aléatoires telle que $X_n\leadsto{\cal B}(n,p)$, alors $(\frac{1}{n}X_n)$ converge en probabilité vers $p$. *La loi faible des grands nombres permet une justification partielle, a posteriori, de la notion de probabilité d'un événement, introduite intuitivement.*|

#### Convergence en loi

|Notions|Commentaires|
|----|----|
|Définition de la convergence en loi d'une suite $(X_n)_{n \in ℕ }$ de variables aléatoires vers $X$.|Notation $X_n \xrightarrow{\mathcal{L}}X$.| 
| Cas où les $X_n$ et $X$ sont à valeurs dans $ℕ$.||
|Si $(np_n)$ tend vers un réel strictement positif $\lambda$, convergence d'une suite de variables aléatoires suivant la loi binomiale $\mathcal{B}(n,p_n)$ vers une variable suivant la loi de Poisson de paramètre $\lambda $.||
|Théorème limite central pour la loi binomiale et pour la loi de Poisson.|Si $(X_n)$ est une suite de variables aléatoires telle que $X_n\leadsto \mathcal{B}(n,p)$ (respectivement $X_n\leadsto \mathcal{P}(n\lambda)$), alors la suite de variables aléatoires centrées réduites $(X_n^*)$ converge en loi vers une variable aléatoire suivant la loi normale centrée réduite.  Théorème admis.||


## Inégalités fameuses

En ECS1, les inégalités que nous allons étudier ne sont valables que dans le cas
où $X(Ω)$ est un ensemble discret (dénombrable ou fini).




### Inégalité de Марков



 Soit $X$ une variable aléatoire **discrète** ne prenant que des valeurs
 **positives** (rangées comme d'habitude dans l'ordre croissant) $x_1,
 x_2,\cdots, x_n$ et **admettant une espérance**.

 Soit $a$ un nombre (qui sera notre seuil d'erreur
 fréquence/probabilité) strictement positif fixé.

 
 Le monde se sépare en deux catégories : les $x_i$ strictement inférieurs à
 $a$ et ceux qui lui sont supérieurs.

 Supposons par exemple que $x_1≤  x_2≤  \cdots x_{k-1}< a≤ 
 x_{k}≤ \cdots≤ x_n$
 
 1. Rappeler la définition de l'espérance $\mathbb{E}(X)$.
 1. Démontrer que $\mathbb{E}(X)≥ a \displaystyle\sum_{i=k}^n ℙ([X=x_i])$.
 1.  En  déduire  que   $  \displaystyle  ℙ([X≥a])  ≤  \dfrac{\mathbb{E}(X)}{a}$
    (inégalité de Марков). 
 1. En déduire la formulation équivalente $ \displaystyle ℙ([X<a]) ≥ 1 - \dfrac{\mathbb{E}(X)}{a}$

Андрей Андреевич  МАРКОВ (1856-1922) proposa  cette formule (dans un  cadre plus
général) afin  de démontrer l'inégalité  énoncée par  BIENAYMÉ en 1853  que nous
allons énoncer dans le paragraphe suivant.

!!! {{ theoreme("inégalité de Марков")}}

    Soit $X$ une variable aléatoire discrète positive admettant une espérance et
    $ a \in ℝ^*_+ $: 
	
	$$
	ℙ([X ≥ a]) ≤ \dfrac{ \mathbb{E}( X ) }{ a }
	$$
	
- Si $a ≤ \mathbb{E}(X)$ cette formule enfonce une porte ouverte (laquelle ?)
-  Si $a$  est  grand  devant $\mathbb{E}(X)$,  alors  l'inégalité signifie  que
   la probabilité que  $X$ prennent de grandes valeurs est  d'autant plus faible
   que $X$ s'éloigne de $\mathbb{E}(X)$ ce qui est frappé au coin du bon sens.


!!! {{ exercice("Cas particulier de la loi binomiale")}}

	On suppose que $X$ suit la loi binomiale $\cal B(n,p)$.
	Donnez une majoration de $ℙ\left(\dfrac{X}{n}≥ a\right)$ et une minoration 
	de $ℙ\left(\dfrac{X}{n}<a\right)$ à l'aide de l'inégalité de MARKOV.


!!! {{ exercice("Démonstration avec une VAR de type quelconque")}}

	On introduit la VAR Y définie sur $Ω$ par :
	
	$$
	ω\mapsto \begin{cases}
	a \text{ si } X(ω) ≥ a\\
	0 \text{ si } X(ω) < a
	\end{cases}
	$$
	
	1. Déterminer $Y(Ω)$.
	1. Déterminer les événements $[Y=a]$ et $[Y=0]$ en fonction de $X$.
	1. Déterminer $\mathbb{E}(Y)$ en fonction de $X$.
	1. Vérifier que $Y ≤ X$.
	1. Conclure.
	
	


### Inégalité de Bienaymé-Чебышёв


![Placeholder](./IMG/bienayme.jpg){ align=left width=100}
Irénée BIENAYMÉ  (1796-1878), inspecteur  des Finances sous  Louis-Philippe puis
sous  la  présidence  de  Louis-Napoléon  Bonaparte,  chercha  à  appliquer  les
probabilités aux calculs  financiers. Il tenta également  de calmer l'engouement
excessif  de ses  contemporains  pour  les statistiques  en  leur appliquant  le
filtre plus rigoureux des  probabilités. Féru de Grec ancien et  de Russe, il se
lit d'amitié avec le mathématicien TCHEBOUYSHOV qui découvre l'inégalité de son ami
français  que  nous allons  étudier  et  la fait  connaître  grâce  à sa  grande
notoriété internationale. Résultat : le nom  de BIENAYMÉ ne sera plus associé au
théorème sauf en France. 



La variance d'une variable aléatoire est la quantité qui mesure la
*dispersion* de  $X$ autour de son  espérance. Elle peut alors  servir de
borne à la mesure de cette dispersion. Ce fut l'idée d'Irénée BIENAYMÉ:


Par exemple en tant que conseiller ou conseillère d'une grande compagnie d'assurances,
vous voulez  savoir si un  événement assuré qui  apparaît comme hors  norme (et
correspond donc à une forte déviation par rapport à l'espérance) se produira. 

!!! {{ theoreme("Inégalité de Bienaymé-Чебышёв")}}

    Soit $X$ une variable aléatoire discrète  admettant un moment d'ordre $2$ et
    $ \epsilon \in ℝ^*_+ $: 
	
	$$
	ℙ(    \left|X   -    \mathbb{E}(   X    )   \right|    ≥   ε    )   ≤
	\dfrac{ \mathbb{V}( X) }{ ε^2 }
	$$

L'ídée de Марков fut d'appliquer son  inégalité à la VAR $(X-\mathbb{E}(X))^2$ :
suivez-la !




!!! {{ exercice()}}

    Majorez alors la probabilité qu'une v.a.r. s'éloigne de $10\sigma(X)
	$ de son espérance.
	
	
!!! {{ exercice()}}

    Les inégalités de nos amis franco-russes permettent d'avoir une
	estimation de certaines probabilités sans qu'on connaisse la loi
	de probabilité. Il faut toutefois être conscient que la
	probabilité exacte peut être assez éloignée de la borne proposée.

	Par exemple, le nombre de caleçons en cotte de mailles fabriqués dans une
	usine de Westeros en une semaine est une variable aléatoire
	d'espérance 50 et de variance 25.

	Estimez, grâce à l'inégalité de Марков, la probabilité que la
	production de la semaine à venir dépasse 75 caleçons en cotte de mailles.
	
	Estimez, grâce à l'inégalité de Bienaymé-Чебышёв, la
	probabilité que la production de la semaine à venir soit
	strictement comprise entre 40 et 60 caleçons en cotte de mailles.	
	

## Convergence en probabilité

### Définition

Une première approche des modes de convergence, nous allons étudier celui qui
paraît  le  plus   intuitif  et  qui  indique  le   rapprochement  de  variables
aléatoires.  Nous  verrons   ensuite  comment  on  peut   également  mesurer  le
rapprochement des lois.

Il  faut noter  qu'il existe  d'autres  modes de  convergences des  VA comme  la
convergence en moyenne  et la convergence presque sûre mais  nous verrons que la
convergence en probabilité:

!!! {{ definition("Convergence en probabilité") }}
    
	Soit $(  X_n )_{ n \in  ℕ } $ et $X$ des  variables aléatoires définies
    sur un même espace probabilisé $ ( \Omega , \cal T_Ω, ℙ ) $. On dit que $ ( X_n
    )_{ n  \in ℕ } $  **converge en probabilité** vers  $X$ lorsque pour
    tout $ε > 0 $, 
	
	$$
	\displaystyle\lim_{ n \to +\infty } ℙ ( [| X_n - X | > ε] ) = 0
	$$
	
	On note $ X_n \xrightarrow{ℙ} X $. 
	
	
!!! {{ exercice()}}

    1. Soit $(X_n)$ une suite de VAR de Bernoulli telles que 
	
		$$
		ℙ([X_n=1])=\dfrac{1}{n} \quad ℙ([X_n=0])=1-\dfrac{1}{n} 
		$$
		
		Étudier la convergence en probabilité de  cette suite et de la suite des
		espérances $(\mathbb{E}(X_n))$.
		
	2. Mêmes questions avec $(Y_n)$ définie par:
		
		$$
		ℙ([Y_n=n^2])=\dfrac{1}{n} \quad ℙ([Y_n=0])=1-\dfrac{1}{n} 
		$$
		
	3. Mêmes questions avec $(Z_n)$ telle que $Z_n\leadsto \mathcal{E}(n)$




### Les inégalités classiques et la convergence en probabilité


!!! {{ exercice()}}

	1.  Soit $(X_n)$  une suite  de VAR  admettant une  espérance et  X une  VAR
	   admettant une espérance. Démontrer que $ \displaystyle\lim_{n\to+\infty}\mathbb{E}\left(|X_n-X|\right) = 0 
	\quad\Longrightarrow\quad
	 X_n\overset{ℙ}{\longrightarrow} X$ .

	2. Soit $(X_n)$ une suite de VAR admettant une variance et X une VAR
	   admettant  une  variance. Démontrer  que  la  formule  de B-T  donne  une
	   condition suffisante de convergence en probabilité de $X_n$ vers $X$.
	3. Démontrer sous les mêmes conditions que $\mathbb E\left(\left(X-X_n\right)^2\right)\underset{n\to+\infty}{\longrightarrow}
	\quad\Longrightarrow\quad
	 X_n\overset{ℙ}{\longrightarrow} X$

### Loi faible des grands nombres pour la loi binomiale

L'introduction de ce théorème en ECS1 est très limité (à la loi binomiale). Vous
étudierez le cas général, plus intéressant, avec Mme Denmat.  

Vous ferez alors
également le lien entre la LfGN et les simulations informatiques.

Vous ferez  attention à l'emploi  trompeur du mot *loi*  ici à ne  pas confondre
avec une loi de probabilité.

!!! {{ theoreme("Loi faible des grands nombres")}}

    === "Énoncé"

         Soit $ ( X_n ) _{n\in ℕ^*} $ une suite de variables aléatoires telles que
		 $ \forall  n \in ℕ^*  $, $ X_n \leadsto  \mathcal B(n,p) $,  alors $
		 \dfrac{1}{n} X_n \xrightarrow{ℙ} p $.

	=== "Indications"

         Pensez à notre couple Bienaymé-Чебышёв


!!! note "Aparté"
    
	Pourquoi faible (et encore on a de la chance, les Espagnols parlent de la *ley
	débil*) ? En fait, la loi (faible) dit de manière générale que :

	$$
	\displaystyle\lim_{n\to+\infty}ℙ\left(  \left\vert \dfrac{X_1+X_2+\cdots+X_n}{n}
	-\dfrac{\mathbb{E}(X_1)+\mathbb{E}(X_2)+\cdots+\mathbb{E}(X_n)}{n} \right\vert >
	ε\right) = 0
	$$

	i.e. la probabilité qu'il y ait une  déviation non négligeable de la moyenne des
	VA par rapport à l'espérance tend vers 0.

	La loi forte  stipule elle que le  fait que la moyenne soit  égale à l'espérance
	est un événement quasi certain.
	
	

	
## Convergence en loi

### Définition

!!! {{ definition("Convergence en loi") }}
    
	Soit $ ( X_n  )_{n\inℕ} $ une suite de  variables aléatoires réelles. On
    dit que $ (  X_n )_{n\inℕ} $ **converge en  loi** vers une variable
    aléatoire réelle $X$ lorsque pour tout $x\inℝ$ **où $F_X$ est continue**, 
	
	$$
    \displaystyle\lim _{n \to +\infty} F_{X_n}( x ) = F_{X}( x)
    $$

	On note alors $ X_n \xrightarrow{\cal L} X $.


Il faudra faire attention à distinguer les cas discret/continu car la continuité
de $F_X$ est nécessaire en $x$. 


Mais il faut  surtout remarquer que la nature discrète/continue  des $X_N$ et de
$X$ peut être différente.

!!! {{ exercice()}}

	 1. Considérons par exemple une suite $(X_n)$ de VAR à densité dont les fonctions de
	    répartition sont définies pour tout $n∈ℕ^*$ par:

	    $$
	    F_n:x\mapsto 
	    \begin{cases}
	    \dfrac{1}{(1-x)^n} \text{ si } x ≤ 0\\
	    1 \text{ si } x> 0
	    \end{cases}
	    $$
	
	    Démontrer que la suite $(X_n)$ converge en loi vers une VAR X dont on
	    précisera la loi.
	
	 2. Même question avec la suite $(Y_n)$ telle que $Y_n\leadsto \cal
	    U\left(\left[-\dfrac{1}{n} , \dfrac{1}{n} \right]\right)$


### Cas où $X_n(Ω)\subseteq ℕ$ et $X(Ω)\subseteq ℕ$

!!! {{ theoreme("Convergence en loi de VAR discrètes à valeurs dans $ℕ$")}}

    === "Énoncé"
        Soit $( X_n)_{n\inℕ} $ une suite de variables aléatoires réelles
        et $X$ une variable aléatoire réelle. On suppose que $ X_n( \Omega)
        $  et  $  X(  \Omega )  $ sont  inclus  dans  $ℕ$.  Alors  
        
	    $$
		  X_n \xrightarrow{\cal L} X ⟺ ∀k∈ℕ, \displaystyle \lim_{n\to +\infty}
		  P ([ X_n = k] ) = P ( [X = k] ) 
	    $$

	=== "Indications"

         *⟸* Introduisez la fonction de répartition 
		 $ F_{X_k}( x )  = \displaystyle\sum_{ i= 0 }^{ \lfloor x\rfloor } ℙ ( [X_k = i])$

	     *⟹* On pense naturellement à écrire $ℙ( [X_k = i]) = F_{X_k}( i) - F_{X_k}(i-1)$
		 mais il y a un petit problème...
		 
		 Que pensez-vous  de $ℙ(  [X_k =  i]) = F_{X_k}  \left( i  + \frac{1}{2}
		 \right) - F_{X_k} \left( i - \frac{1}{2} \right)$ ?
			 



### Convergence d'une loi binomiale vers une loi de Poisson


!!! {{ theoreme("Convergence d'une loi binomiale vers une loi de Poisson")}}

    === "Énoncé"

        Soit $ (π_n) \in ]0,1[ ^{ℕ} $ une suite de réels telle que $ ( n π_n ) $
        converge vers un réel strictement positif $\lambda$. 
		
		Soit $  ( X_n )_{n\inℕ}  $ une suite  de variables aléatoires  telle que
		pour tout $ n \in ℕ $, $ X_n \leadsto \mathcal{B}( n , π_n ) $. 
		
		Alors $ X_n \xrightarrow{\cal L} Y $, où $ Y \leadsto \mathcal{P}( \lambda ) $.

	=== "Indications"

         Nous   avons    déjà   démontré   ce   résultat    dans   un   chapitre
         précédent...Pensez à utiliser le théorème précédent.

### Théorème limite central



!!! {{ theoreme("TLC cas de la loi binomiale")}}


	Soit  $ (  X_n )_{n\inℕ} $  une suite  de variables  aléatoires réelles
	discrètes  telle  que  pour  tout  $  n \in  ℕ  $,  $  X_n  \leadsto
	\mathcal{B}( n,p ) $. Alors 
	
	$$
	X_n^* = \dfrac{ X_n - n p }{ \sqrt{ n p (1-p) } } \xrightarrow{\cal L} Y 
	$$
	
	où  $ Y \leadsto \mathcal{N}( 0 , 1) $.




!!! {{ theoreme("TLC cas de la loi de Poisson")}}

	Soient $  \lambda > 0 $  et $ ( X_n  ) _{n\inℕ} $ une  suite de variables
	aléatoires  réelles   discrètes  telle  que   pour  tout  $n\inℕ$,   $  X_n
	\leadsto \mathcal{P}( n \lambda ) $. Alors 

	$$
	X_n^* = \dfrac{ X_n - n \lambda }{ \sqrt{n \lambda} } \xrightarrow{\cal L} Y 
	$$

	où $ Y \leadsto \mathcal{N}( 0 , 1) $.

