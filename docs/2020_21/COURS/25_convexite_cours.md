
{{ chapitre(25, "Extrema - Convexité")}}

!!! exo ""
	![Placeholder](./IMG/jensen.jpeg){ align=right width=250}
	J.L.W.V. JENSEN  publie en 1905  *Om konvexe funktioner og  uligheder mellem
	midelvaerdier*  est semble-t-il  le  premier mathématicien  à introduire  le
	terme de *fonction  convexe* et les inégalités qui  l'accompagnent. Le terme
	*convexe* a  été introduit en français  vers 1370 pour désigner  une surface
	« bombée  » et vient  du latin  *convexus* qui signifie  arrondi, circulaire
	lui-même issu  de *conveho*  qui signifie convoyer.  Nous lui  opposerons le
	terme  *concave* apparu  en français  à la  même époque  et qui  désigne une
	surface « en creux ».
	
	Cependant,  cette étude de ce  que nous appelons maintenant  la *convexité*
	est un  problème qui a intéressé  les mathématiciens dès le  début du calcul
	différentiel. Dès 1665 NEWTON s'est inéressé au rapport entre le signe de la dérivée
	seconde et l'orientation des courbes (« courbée  vers le haut » ou « vers le
	bas »).
	
	Nous devons rappeler quelques résultats  sur les extrema pour nous replonger
	dans le  bain qui sera  bien profond l'an  prochain au moment  d'étudier les
	extrema de fonctions de plusieurs variables. 
	
	Mais laissons Sir Isaac nous rafraîchir la mémoire :
	
	*When a quantity is the greatest or the  least that it can be, at the moment
	it neither flows backward or forward. For if it flows forward, or increases,
	that  proves that  it  was less,  and  will presentely  be  greater than  it
	is...Wherefore find its Fluxion [...] and suppose it to be nothing.*

## Programme

### Extremum

*Pour préparer l'introduction des notions de topologie du programme de deuxième année, on insistera sur la différence entre la recherche d'extremum sur un segment et la recherche d'extremum sur un intervalle ouvert.*


|Notions|Commentaires|
|-------|------------|
|Toute fonction continue sur un segment admet des extrema globaux sur ce segment.||
|Dans le cas d'une fonction de classe $C^1$ : condition nécessaire d'existence d'un extremum local sur un intervalle ouvert.
Définition d'un point critique.|On pourra montrer que le résultat tombe en défaut lorsque l'intervalle de définition n'est pas ouvert.|
|Condition suffisante d'existence d'un extremum local en un point critique pour
une fonction de classe $C^2$ sur un intervalle ouvert.|Ce résultat sera démontré grâce au développement limité à l'ordre 2.|


### Fonctions convexes

*Tous les résultats de cette section seront admis*


|Notions|Commentaires|
|----|----|
|Définition des fonctions convexes, fonctions concaves. Point d'inflexion.|Une fonction est convexe sur un intervalle $I$ si $\forall (x_1,x_2)\in I^2, \forall (t_1,t_2)\in [0 , 1]^2$ tels que $t_1+t_2=1$, $f(t_1x_1+t_2x_2)\leqslant t_1f(x_1)+t_2f(x_2)$. Interprétation géométrique.|
|Généralisation de l'inégalité de convexité.||
|Caractérisation des fonctions convexes de classe $C^1$.|Les étudiants devront savoir que si $f$ est de classe $C^1$, alors $f$ est convexe si et seulement si l'une des deux propositions est vérifiée : $f'$ est croissante ou $C_f$ est au-dessus des tangentes.||
Caractérisation des fonctions convexes et concaves de classe $C^2$.||



## Extrema

### Rappels

Voici quelques résultats vus en début d'année.


#### Que dire de la dérivée en un extremum local ?


Intuitivement, si une fonction $f$ dérivable sur un intervalle admet sur cet intervalle un minimum en
$x_0$, la  fonction va décroître  vers $f(x_0)$ puis croître  ensuite et
donc il semble que $f'(x_0)$ va être nul.


Observons quelques cas avec Scilab :

![Placeholder](./IMG/extremum1.jpg){ align=right width=290}
```scilab
a = get("current_axes");
a.x_location = "origin";
x = (-1:0.125:4)';
plot2d(x, (x - 2)^2 - 1)
``` 

Cela semble fonctionner. Mais observons ceci :

![Placeholder](./IMG/extremum2.jpg){ align=left width=290}
```scilab
a = get("current_axes");
a.x_location = "origin";
x = (3:0.125:4)';
plot2d(x, (x - 2)^2 - 1)
```


Le  minimum est  atteint en  3 car  $f$ est  strictement  croissante sur
$[3\,,\,4]$ et pourtant $f'(3)\neq 0$.


Il  faut  donc  distinguer  le   cas  où  $x_0$  est  une  extrémité  de
l'intervalle des autres cas.

Supposons donc que $x_0$ soit un  point intérieur à un intervalle I, que
$f$ soit dérivable en $x_0$ et que,
par exemple, $f$ admette un minimum local en $x_0$.

Cela     veut     dire    qu'il     existe     $\alpha>0$    tel     que
$[x_0-\alpha\,,\,x_0+\alpha]\subset I$ et 

$$(\forall x\in[x_0-\alpha\,,\,x_0+\alpha])( f(x)\geqslant f(x_0))$$

Le taux d'accroissement $\dfrac{f(x)-f(x_0)}{x-x_0}$ est donc négatif sur $[x_0-\alpha\,,\,x_0]$
 et positif sur $[x_0\,,\,x_0+\alpha]$.

Il en est donc de même de ses  limites à gauche et à droite en $x_0$. Or
la fonction $f$ étant dérivable en $x_0$, ces deux limites sont égales à $f'(x_0)$
et donc on a $f'(x_0)\leqslant  0$ et $f'(x_0)\geqslant 0$. On en déduit que $f'(x_0)=0$.
 
 
!!! {{ theoreme("Condition nécessaire d'existence d'un extremum")}}

	=== "Énoncé"
		Soit I un intervalle OUVERT et soit $x_0$ un élément de I (qui n'est donc pas
        une extrémité de I). 
		Soit $f$ une fonction définie sur I et dérivable en $x_0$.

	    SI $f$ admet un extremum local en $x_0$, ALORS $f'(x_0)=0$.
		
	=== "Indications"
		Suivons les instructions de NEWTON. 
		Supposons que $f$ possède un maximum  local en $x_0$. Il existe alors un
		réel $\alpha > 0$ tel  que $ [ x_0 - \alpha, x_0 +  \alpha ] \subset I $
		et $  \forall x \in [  x_0 - \alpha, x_0  + \alpha ]$, $  f(x) \leqslant
		f(x_0) $.
		
		Donc, pour tout $ x \in ] x_0, x_0 + \alpha ] $,
		
		$$
		\dfrac{ f(x) - f(x_0) }{ x - x_0 } \leqslant 0
		$$
		
	    Or $f$ est  dérivable en $x_0$ par  hypothèse. On peut donc  passer à la
	    limite dans cette inégalité et on trouve $ f'(x_0) \leqslant 0 $ 
		De même, pour tout $ x \in [ x_0 - \alpha , x_0 [ $, 
	
		$$
		\dfrac{ f(x) - f(x_0) }{ x - x_0 } \geqslant0
		$$
		
		ce qui donne $ f'(x_0) \geqslant0 $. Donc $f'(x_0) = 0 $.
	 

!!! danger
	On ne le  répétera jamais assez: attention  aux conditions d'utilisation
        de ce théorème! 
		
!!! danger
	 ATTENTION! La réciproque  est fausse ! Il se peut  que $f'(x_0)=0$ sans que
	 $f$ n'admette d'extremum en $x_0$. 
	 
!!! note "point critique"
	Dans le cas d'une fonction de classe $\cal C^1$ sur un intervalle ouvert, un
	élément $c$ de cet intervalle est un **point critique** si $f'(c)=0$.
	

!!! note "où sont les extrema ?"
	  
	  Les  résultats précédents  nous  indiquent donc  qu'il  faut chercher  les
	  extrema d'une  fonction de classe  $\cal C^1$ éventuellement  par morceaux
	  parmi :
	  
	  - ses points critiques
	  -  les  bornes  des morceaux  et  les  points  où  la fonction  n'est  pas
	    dérivable.
		
### Nouveauté

Si la  fonction est de  classe $\cal  C^2$, le cours  de cette anée  nous permet
d'obtenir un résultat liant extrema et dérivée seconde :

!!! {{ theoreme("Extrema et dérivée seconde")}}

    === "Énoncé"

         Soit $f$ une  fonction de classe $C^2$ sur un  intervalle ouvert $I$ et
         soit $c \in I$ un point critique de $f$. Alors, 
		 
		 - Si $f^{(2)}(c)>0$, alors $f$ possède un minimum local en $c$.
		 - Si $f^{(2)}(c)<0$, alors $f$ possède un maximum local en $c$.
		 - Si  $f^{(2)}(c)=0$, c'est le mystère.
		   

	=== "Indications"

         Effectuer un $DL_2(c)$ : $f(c+h)\underset{h\to 0}{=}...$


!!! {{ exercice()}}

    Déterminer les  extrema locaux  et une alllure  de la  courbe représentative
    des fonctions :
	
	1. $f : x\mapsto x^3-x^2-3x$ sur $[-2,3]$
	2. $g : x\mapsto \dfrac{x}{1+x^2} $ sur $ℝ$

	 
	 
## Fonctions convexes



!!! {{ definition("Fonctions convexes, concaves") }}
    
	 Soit $f$ une fonction définie sur un intervalle $I$ non vide et non réduit à
     un point.
	
	 - On  dit que  la fonction $f$  est **convexe** sur  $I$ lorsque  $ \forall
	   (x_1, x_2) \in I^2 $, $ \forall λ \in [0,1] $, 
	  
        $$
	    f ( λ x_1 + (1-λ) x_2 ) \leq λ f(x_1) + (1-λ) f(x_2)
	    $$
	  
	- On dit que la fonction $f$ est **concave** sur $I$ lorsque $ \forall
	   (x_1, x_2) \in I^2 $, $ \forall λ \in [0,1] $, 
	  
        $$
	    f ( λ x_1 + (1-λ) x_2 ) \geq λ f(x_1) + (1-λ) f(x_2)
	    $$


!!! {{ exercice("Interprétation géométrique")}}

	Comment interpréter géometriquement ces inégalités ?
	
	


!!! {{ theoreme("con-cave/vexe")}}

	Une fonction $f$ est  concave sur un intervalle $I$ si  et seulement si $-f$
	est convexe sur $I$.
	
!!! {{ exercice()}}

	Que  pensez-vous de  la  convexité  d'une fonction  affine,  de la  fonction
	$x\mapsto x^2$ ? De la fonction $x\mapsto |x|$?
	

!!! {{ exercice()}}

	Soit  I et  J  deux intervalles,  $f:  I→J$  et $g:  J->  ℝ$ deux  fonctions
	convexes. Est-ce que $g\circ f$ est convexe ?


!!! {{ theoreme("Généralisation de l'inégalité de convexité")}}

    === "Énoncé"

        Soit $n \in ℕ^*$ et $f$ une fonction définie sur un intervalle $I$
        non vide et non réduit à un point.  Si $f$ est convexe sur $I$, alors $
        \forall ( x_1 , x_2 , \dots , x_n ) \in I^n $, $ \forall ( t_1 , t_2 ,
        \dots , t_n ) \in [0,1]^n $ tels que $ \displaystyle\sum_{k=1}^{n} t_k = 1 $, 
		
		$$
		f \left( \displaystyle\sum_{k=1}^{n} t_k x_k \right) \leq \sum_{k=1}^{n} t_k f(x_k)
        $$

	=== "Indications"

         Récurrence
		 

!!! {{ exercice()}}
	
	Démontrer par exemple  que pour une fonction convexe sur  un intervalle, l'image
    de la moyenne est inférieure à la moyenne des images. 



!!! {{ exercice()}}
	
	Soit $(x_1,x_2,\dots,x_n)∈(ℝ_+^*)^n$. Démontrer que
	
	$$
	\left(\displaystyle\prod_{i=1}^nx_i\right)^{\frac{1}{n}} ≤ \dfrac{1}{n} \displaystyle\sum_{i=1}^nx_i
	$$



!!! {{ theoreme("Convexité d'une fonction $\cal C^1")}}
	Soit $f$ une fonction de classe $\cal C^1$ sur un intervalle $I$. Les propriétés
	suivantes sont équivalentes: 

	1. $f$ est convexe sur $I$
	1. $f'$ est croissante sur $I$
	1. En tout point de $I$, $\mathcal{C}_f$ est au dessus de ses tangentes
	1. Pour tout $(x,a)∈I^2$, $f(x) ≥ f(a) + (x-a)f'(x)$

!!! {{ exercice()}}

    Étudier la convexité des fonctions exp, ln, sin, cos.


	
On peut ainsi démontrer plus rapidement des résultats bien connus :

!!! {{ exercice()}}
	
	À l'aide de résultats de convexité, démontrer que:
	
	- pour tout $ x \in ℝ $, $ {\rm e}^x \geq x + 1 $.
	- pour tout $ x \in ] -1, + \infty [ $, $ x \geq \ln(x + 1) $.





!!! {{theoreme("Convexité d'une fonction $\cal C^2$")}}

	Soit $f$ une fonction de classe $\cal C^2$ sur un intervalle $I$. 
	
	Alors $f$ est
	convexe sur $I$ **si, et seulement si,** pour tout $ x \in I $, $f''(x) \geq
	0$. 
	
	

	

	
	
!!! {{ definition("Point d'inflexion") }}
    
	Soit $f$ une fonction  deux fois dérivable sur un intervalle  $I$, et $c \in
	I$. On dit que $c$ est  un **point d'inflexion** de la courbe représentative
	de $f$ lorsque $f''$ s'annule ET change de signe en ce point. 

Que pensez-vous de le concavité de la courbe autour d'un point d'inflexion ?


!!! {{ exercice()}}
	
	Soit $f : x∈ℝ  \mapsto ax^3+bx^2+cx+d$ avec $(a,(b,c,d))∈ℝ^*⊗ℝ^3$. Démontrer
	que la courbe  représentative de $f$ admet un point  d'inflexion et préciser
	son abscisse.


!!! {{ exercice()}}
	
	Étudier la concavité  et les points d'inflexion de  la courbe représentative
	des fonctions suivantes :
	
	1. $f : x∈ℝ\mapsto \operatorname{e}^{-x^2}$
	2. $f : x∈ℝ\mapsto \sin(x) - \cos(x)$.
