{{ chapitre(22, "Intégrales impropres")}}


!!! exo ""
	![Placeholder](./IMG/Ermakov.jpeg){ align=right width=300}
	Notre exploration de l'analyse continue.  Pour l'instant, nous avons répondu
	à ces questions:
	
	- Qu'est-ce qu'une dérivée ? Une *limite*.
	- Qu'est-ce qu'une série ? Une *limite*.
	- Qu'est-ce qu'une intégrale ? Une *limite* (pensez aux sommes de Riemann)
	- Qu'est-ce qu'une limite ? Un *nombre*.
	
	Suite aux  choix des  concepteurs du  programme d'ECS,  nous avons  éludé la
	définition de l'intégrale en tant que limite. Ce chapitre va permettre
	de créer un lien plus concret entre limite et intégrale.
	Il restera  une grande question en  suspens : qu'est-ce qu'un  nombre ? Mais
	cette question est hors  de notre portée en ECS comme  elle l'était de celle
	du grand Cauchy. Il faudrait étudier les travaux de Weiersrass et Heine mais
	ceci est une autre histoire.
	
	Les séries  sont apparues pour nous  comme un outil fondamental  d'étude des
	probabilités discrètes qui  ont un rôle primordial dans  la compréhension du
	monde.
	Les   intégrales  impropres   le  seront   pour  l'étude   des  probabilités
	continues.     Si     l'on    a     pu     donner     un    sens     à     $
	\displaystyle\sum_{n=0}^{+\infty}u_n$,  on  en  donnera   tout  autant  à  $
	\displaystyle\int_0^{+\infty} f(x) {\rm d}x$ qui est aussi une limite. 
	
    J'ai  choisi d'honorer  dans  ce chapitre  le mathématicien  **biélorusse**
    [Василий  Петрович  Ермаков](https://ru.wikisource.org/wiki/%D0%AD%D0%A1%D0%91%D0%95/%D0%95%D1%80%D0%BC%D0%B0%D0%BA%D0%BE%D0%B2,_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D0%B8%D0%B9_%D0%9F%D0%B5%D1%82%D1%80%D0%BE%D0%B2%D0%B8%D1%87)  né  à  Терюха en  1845  (enfin,  il  aurait  été
    biélorusse  aujourd'hui  mais il  est  décédé  ukrainien  en 1922).  Il  est
    l'auteur  en particulier  d'un élégant  théorème donnant  un critère  fin de
    convergence pour les intégrales. 
	
	
	
## Qu'est-ce qu'une intégrale impropre ?

D'après notre cours, l'existence d'une intégrale $\int_a^bf(x){\rm d}x$ est basée sur deux hypothèses:
	
- $[a,b]$ est un intervalle fermé fini;
- $f(x)$ est continue sur cet intervalle.

Une intégrale impropre sera une intégrale qui ne vérifie pas au moins une de
ces hypothèses. Nous allons étudier ce genre de problème dans ce chapitre.

!!! {{ definition("intégrale impropre") }}
    Soit $(a,b)∈ℝ^2$ avec  $a<b$. On appelle intégrale  impropre toute intégrale
    d'un des huit types suivants:
	
	1. $ \displaystyle\int_a^bf(x){\rm  d}x$ où $f$ est continue  sur $[a,b[$ et
	   non définie en $b$.
	2. $ \displaystyle\int_a^bf(x){\rm  d}x$ où $f$ est continue  sur $]a,b]$ et
	   non définie en $a$.
	3. $ \displaystyle\int_a^bf(x){\rm  d}x$ où $f$ est continue  sur $]a,b[$ et
	   non définie en $a$ et $b$.
    4. $ \displaystyle\int_a^{+\infty}f(x){\rm  d}x$  où $f$  est continue  sur
       $[a,+\infty[$.
	4.  $ \displaystyle\int_{-\infty}^bf(x){\rm  d}x$  où $f$  est continue  sur
       $]-\infty,b]$.
	4. $ \displaystyle\int_a^{+\infty}f(x){\rm  d}x$  où $f$  est continue  sur
       $]a,+\infty[$ et non définie en $a$.
	4. $ \displaystyle\int_{-\infty}^bf(x){\rm  d}x$  où $f$  est continue  sur
       $]-\infty,b[$ et non définie en b.
	4. $ \displaystyle\int_{-\infty}^{+\infty}f(x){\rm  d}x$  où $f$  est continue  sur
       $ℝ$.


En fait on va séparer ces cas en trois catégories : 

- la droite pose porblème;
- la gauche pose problème;
- les deux côtés posent problème.

Par exemple :

- $ \displaystyle\int_0^1 \dfrac{1}{\sqrt{1-x}} {\rm d}x$ est du type 1 (droitiste).
-  $   \displaystyle\int_0^1  \dfrac{1}{x(1-x)}   {\rm  d}x$   est  du   type  3
  (ambidextre).
-  $  \displaystyle\int_1^{+\infty}  \dfrac{1}{x^2}  {\rm d}x$  est  du  type  4
  (droitiste).
-  $  \displaystyle\int_0^{1}  \dfrac{1}{x^2}  {\rm d}x$  est  du  type  2
  (gauchiste).
-  $  \displaystyle\int_0^{+\infty}  \dfrac{1}{x^2}  {\rm d}x$  est  du  type  6
  (ambidextre).
- $ \displaystyle\int_{-\infty}^{+\infty} {\rm e}^{t^2}{\rm d}x$ est du type 8
  (ambidextre).


Dans la suite nous noterons:

- $\overline{ℝ}=ℝ∪\{-\infty, +\infty\}$
- $\overrightarrow{ℝ}=ℝ∪\{+\infty\}$
- $\overleftarrow{ℝ}=ℝ∪\{-\infty\}$

## Convergence/divergence

La  première chose  à  faire est  de  repérer à  laquelle  des trois  catégories
sus-citées appartient l'intégrale à étudier : **où est le problème ?**


### Droitistes

!!! {{ definition("Convergence à droite") }}
    
	Soit   $f:  [a,b[\to   ℝ$,  continue   sur   $[a,b[$  avec   $a∈ℝ$  et   $b∈
    \overrightarrow{ℝ}$.
	
	On dit que $ \displaystyle\int_a^bf(t){\rm d}t$ **converge** si 
	
	$$ 
	F\colon 
	\begin{array}{rll}
 	[a,b[ & \to & ℝ \\
    x & \mapsto & \displaystyle\int_a^xf(t){\rm d}t
    \end{array}
	$$
	
	admet une limite finie en $b$. Dans ce cas :
	
	$$
	\displaystyle\int_a^bf(t){\rm        d}t        =       \lim_{x\to        b}
	\displaystyle\int_a^xf(t){\rm d}t
	$$
	
	Dans le cas contraire, on dit que l'intégrale **diverge**.
	
	
Ainsi $f$ est continue  sur tout intervalle $[a,x]$ avec $x<b$  et admet sur cet
intervalle une primitive F. L'intégrale
impropre converge donc si, et seulement si F admet une limite en $x$. Voyons des exemples.
	

1. Étudions par exemple  l'intégrale $ \displaystyle\int_0^1 \dfrac{1}{1-t} {\rm
   d}t$. La fonction est continue sur $[0,1[$ mais n'est pas définie en 1. 

	Pour   tout   $x∈[0,1[$,   $   \displaystyle\int_0^x   \dfrac{1}{1-t}   {\rm
	d}t=-\ln(1-x)\underset{x\to 1}{\longrightarrow}+\infty$.
	Ainsi $  \displaystyle\int_0^1 \dfrac{1}{1-t} {\rm d}t$  diverge: l'aire
	ci-dessous est infinie.



	<div class="chart-container" style="position: relative; width:70%">
	<canvas id="myChart1"  style="background-color: rgba(0,0,0,0.2)"></canvas>
	</div>


2. Étudions maintenant  l'intégrale $ \displaystyle\int_1^{+\infty} \dfrac{1}{t^2} {\rm
   d}t$. La fonction est continue sur $[1,+\infty[$ mais la borne de droite est infinie. 

	Pour   tout   $x∈[1,+\infty[$,   $   \displaystyle\int_1^x   \dfrac{1}{t^2}   {\rm
	d}t=1-\dfrac{1}{x} \underset{x\to +\infty}{\longrightarrow}1$.
	Ainsi $ \displaystyle\int_1^{+\infty} \dfrac{1}{t^2}  {\rm d}t$ converge vers
	1 : l'aire ci-dessous vaut 1.



	<div class="chart-container" style="position: relative; width:70%">
	<canvas id="myChart2"  style="background-color: rgba(0,0,0,0.1)"></canvas>
	</div>


### Gauchistes

!!! {{ definition("Convergence à gauche") }}
    
	Soit $f: ]a,b]\to ℝ$, continue sur $]a,b]$ avec $a∈ \overleftarrow{ℝ}$ et $b∈ℝ$.
	
	On dit que $ \displaystyle\int_a^bf(t){\rm d}t$ **converge** si 
	
	$$ 
	F\colon 
	\begin{array}{rll}
 	]a,b] & \to & ℝ \\
    x & \mapsto & \displaystyle\int_x^bf(t){\rm d}t
    \end{array}
	$$
	
	admet une limite finie en $a$. Dans ce cas :
	
	$$
	\displaystyle\int_a^bf(t){\rm        d}t        =       \lim_{x\to        a}
	\displaystyle\int_x^bf(t){\rm d}t
	$$
	
	Dans le cas contraire, on dit que l'intégrale **diverge**.







 Voyons des exemples.
	

1. Étudions par exemple  l'intégrale $ \displaystyle\int_0^1 \dfrac{1}{t} {\rm
   d}t$. La fonction est continue sur $]0,1]$ mais n'est pas définie en 0. 

	Pour   tout   $x∈]0,1]$,   $   \displaystyle\int_x^1   \dfrac{1}{t}   {\rm
	d}t=-\ln(x)\underset{x\to 0}{\longrightarrow}+\infty$.
	Ainsi $  \displaystyle\int_0^1 \dfrac{1}{t} {\rm d}t$  diverge: l'aire
	ci-dessous est infinie.



	<div class="chart-container" style="position: relative; width:70%">
	<canvas id="myChart3"  style="background-color: rgba(0,0,0,0.2)"></canvas>
	</div>


2. Étudions maintenant  l'intégrale $ \displaystyle\int_{-\infty}^{-1} \dfrac{1}{t^2} {\rm
   d}t$. La fonction est continue sur $]-\infty,-1]$ mais la borne de gauche est infinie. 

	Pour   tout   $x∈]-\infty,-1]$,   $   \displaystyle\int_x^{-1}   \dfrac{1}{t^2}   {\rm
	d}t=\dfrac{1}{x}+1 \underset{x\to -\infty}{\longrightarrow}1$.
	Ainsi $ \displaystyle\int_{-\infty}^{-1} \dfrac{1}{t^2}  {\rm d}t$ converge vers
	1 : l'aire ci-dessous vaut 1.



	<div class="chart-container" style="position: relative; width:70%">
	<canvas id="myChart4"  style="background-color: rgba(0,0,0,0.1)"></canvas>
	</div>






### Ambidextristes


!!! {{ definition("Convergence des deux côtés") }}
    
	Soit $f:  ]a,b[\to ℝ$, continue  sur $]a,b[$ avec $a∈  \overleftarrow{ℝ}$ et
	$b∈ \overrightarrow{ℝ}$.
	
	On dit  que $ \displaystyle\int_a^bf(t){\rm  d}t$ **converge** si  pour tout
	$c∈]a,b[$  les   intégrales  $   \displaystyle\int_a^cf(t){\rm  d}t$   ET  $
	\displaystyle\int_c^bf(t){\rm d}t$ convergent (respectivement  à gauche et à
	droite).
	
	
	Dans ce cas, pour tout $c∈]a,b[$ :
	
	$$
	\displaystyle\int_a^bf(t){\rm  d}t  =  \displaystyle\int_a^cf(t){\rm  d}t  +
	\displaystyle\int_c^bf(t){\rm d}t 
 	$$
	
	Dans le cas contraire, on dit que l'intégrale **diverge**.
	
- En pratique on choisit n'importe quel  $c$ dans $]a,b[$ donc en particulier un
  $c$ qui pourrait faciliter nos calculs.
- Ainsi l'intégrale diverge s'il existe un $c∈]a,b[$ tel que
$\displaystyle\int_a^cf(t){\rm  d}t$  diverge  ou  $\displaystyle\int_c^bf(t){\rm
d}t$ diverge.




!!! {{ exercice()}}

	1. Étudier la convergence  de $ \displaystyle\int_0^1 \dfrac{1}{t(1-t)} {\rm
	   d}t$
	2.  Étudier la convergence  de $ \displaystyle\int_{-\infty}^{+\infty} t {\rm
	   d}t$
	3. Calculer $ \displaystyle\lim{x\to +\infty} \displaystyle\int_{-x}^{x} t {\rm
	   d}t$ : qu'en pensez-vous ?
	   

!!! note "Remarque"

	Dans  la suite,  on  considèrera des  intégrales droitistes  pour  ne pas  à
	traiter  tous  les cas  à  chaque  théorème. En  effet,  les  autres cas  se
	déduisent de la connaissance de celui-ci.




### Premières règles de calcul sur les intégrales convergentes


!!! {{ theoreme("Linéarité")}}

    === "Énoncé"

		Soient $f$  et $g$ deux fonctions  continues sur $[a,b[$ avec  $a∈ℝ$ et
		$b∈   \overrightarrow{ℝ}$   telles   que    les   deux   intégrales   $
		\displaystyle\int_a^bf(t){\rm  d}t$ et  $ \displaystyle\int_a^bg(t){\rm
		d}t$ convergent.

		Alors      pour       tout      réel      $λ$,       l'intégrale      $
		\displaystyle\int_a^b\bigl(f(t)+λg(t)\bigr){\rm d}t$ converge et 

		$$
		\displaystyle\int_a^b\bigl(f(t)+λg(t)\bigr){\rm d}t
		=\displaystyle\int_a^bf(t){\rm d}t+
		λ\displaystyle\int_a^bg(t){\rm d}t
		$$

	=== "Indications"

         Travaillez sur $[a,x]$ et passez à la limite.



!!! {{exercice()}}

	Que se passe-t-il si l'une des intégrales converge et pas l'autre ?
	
!!! {{exercice()}}
	
	Démontrez de même la croissance de l'intégration. 
	
	
!!! {{ theoreme("Relation de Chasles")}}

    === "Énoncé"

		Soit  $f$  une fonction  continue  sur  un  intervalle $[a,c[$  avec  $a∈ℝ$,
		$c∈\overrightarrow{ℝ}$ et soit $ b \in [a,c[$. 

		L'intégrale $  \displaystyle\int_b^cf(t){\rm d}t$ converge si,  et seulement
		si, l'intégrale $ \displaystyle\int_a^cf(t){\rm d}t$ converge et on a alors:

		$$
		\displaystyle\int_a^cf(t){\rm  d}t  =  \displaystyle\int_a^bf(t){\rm  d}t  +
		\displaystyle\int_b^cf(t){\rm d} 
		$$

	=== "Indications"

         Travaillez sur un **segment** où $f$ est continue et passez à la limite.



#### Changement de variable et IPP

Les théorèmes spécifiques  des intégrales impropres ne sont pas  au programme en
ECS1 donc on passe systématiquement aux intégrales  sur un segment et on passe à
la limite.

### Intégrales faussement impropres

Certaines  fonctions continues  sur  des intervalles  semi-ouverts $[a,b[$  avec
$b∈ℝ$ peuvent
être prolongées  par continuité  en $b$  et sont alors  intégrables au  sens des
intégrales définies des fonctions continues.

Par  exemple  $ \displaystyle\int_0^1  \dfrac{1-\cos(t)}{t^2}  {\rm  d}t$ ou  la
fameuse $ \displaystyle\int_0^1 \dfrac{\sin(t)}{t} {\rm d}t$: pourquoi ?



## Intégrale de Riemann

Bernhard Riemann  est l'auteur d'une œuvre  fondamentale dont son étude  sur les
fonctions $ζ:  s\mapsto \displaystyle\sum  \dfrac{1}{n^s}$. En son  honneur, les
séries  $\displaystyle\sum \dfrac{1}{n^s}$  sont  appelées  *séries de  Riemann*
comme cela a déjà été évoqué dans un chapitre précédent. On parle donc d'*intégrale de
Riemann* en l'honneur du grand Bernhard pour désigner les intégrales de la forme
$ \displaystyle\int_a^b \dfrac{1}{t^α} {\rm d}t$.


!!! {{ theoreme("convergence des intégrales de Riemann")}}

    === "Énoncé"

         Soit $α∈ℝ$.
		 
		 - L'intégrale $  \displaystyle\int_1^{+\infty} \dfrac{1}{t^\alpha} {\rm
		   d}t$  converge si,  et  seulement si,  $\alpha>1$, et  on  a alors  $
		   \displaystyle\int_1^{+\infty} \dfrac{1}{t^\alpha} {\rm d}t = \dfrac{1}{\alpha - 1}$.

	     -  L'intégrale $  \displaystyle\int  _0^1  \dfrac1{t^\alpha} {\rm  d}t$
		    converge si, et seulement si, $\alpha<1$, et on a alors $ \displaystyle \int _0^1
	        \dfrac1{t^\alpha} {\rm d}t = \dfrac{1}{1-\alpha}$. 

	=== "Indications"

         Introduisez la borne $x$ et pensez à distinguer le cas $α=1$.
		 

!!! {{ exercice()}}

    Quelle est la nature de $ \displaystyle\int_0^{+\infty} \dfrac{1}{t^α} {\rm d}t$?








## Intégration des fonctions positives

On peut facilement étudier la nature  d'une intégrale impropre si on connaît une
primitive de  la fonction à  intégrer. Mais  la vie (des  préparationnaires) est
injuste et souvent il est difficile  voire impossible d'exhiber une primitive de
$f$.    L'idée   st    alors   de    comparer   $f$    à   des    fonctions   de
référence. Malheureusement encore, comme pour  les séries, cela ne sera possible
que pour des fonctions à valeurs positives.




### Positivité

!!! {{ theoreme("Positivité")}}

    Soit $f$ une fonction continue et positive sur un intervalle $[a,b[$ avec $a∈ℝ$, $b∈
    \overrightarrow{ℝ}$ et $a<b$.
	
	- Si l'intégrale $ \displaystyle\int_a^b f(t) {\rm d}t$ converge ET vaut 0,
	alors la fonction $f$ est identiquement nulle sur $[a,b[$.
	- Si l'intégrale $ \displaystyle\int_a^b f(t)  {\rm d}t$ converge EΤ $f$ est
	  différente de l'application nulle  alors $ \displaystyle\int_a^b f(t) {\rm
	  d}t>0$.
	  
	  
On fera bien attention à vérifier toutes les hypothèses:
 
1. $a<b$
1. $f$ est continue et positive sur $[a,b[$
2. l'intégrale converge

Le deuxième point est la contraposé du premier point.


### Convergence par majoration d'une primitive.


!!! {{ theoreme("Convergence par majoration")}}

    === "Énoncé"
		Soit $f$ une fonction continue et positive sur un intervalle $[a,b[$ avec $a∈ℝ$, $b∈
		\overrightarrow{ℝ}$ et $a<b$.

		Soit $F$ définie sur $[a,b[$ par $x  \mapsto  \displaystyle\int_a^x f(t) {\rm d}t$. Alors:

		$$
		\displaystyle\int_a^b  f(t) {\rm  d}t\text{ converge  } ⟺  F \text{  est
		majorée sur } [a,b[
		$$

    === "Indications"
		Vérifiez que $F$ est croissante et pensez au théorème de la limite monotone.


!!! {{ exercice()}}

	Étuidiez la convergence de $ \displaystyle\int_0^1 \left|\sin\left(\dfrac{1}{t} \right)\right| {\rm d}t$


### Critères de comparaison pour les fonctions positives


Dans cette  section, $f$ et  $g$ sont des fonctions  continues sur $[a,b[$  et à
valeurs positives avec $a∈ℝ$, $a<b$ et $b∈\overrightarrow{ℝ}$.


#### Cas où $f ≤ g$ au voisinage de $b$


!!! {{theoreme("$f ≤ g")}}

    === "Énoncé"
		On suppose qu'au voisinage de $b$ on a $0 ≤ f(t) ≤ g(t)$. Alors

		$$
		\displaystyle\int_a^b    g(t)   {\rm    d}t    \text{    converge   }    ⟹
		\displaystyle\int_a^b f(t) {\rm d}t \text{ converge }
		$$

		$$
		\displaystyle\int_a^b    f(t)   {\rm    d}t    \text{    diverge   }    ⟹
		\displaystyle\int_a^b g(t) {\rm d}t \text{ diverge }
		$$

    === "Indication"
		Si  $\displaystyle\int_a^bg(t){\rm  d}t$  converge  alors  $G:  x\mapsto
		\displaystyle\int_a^x g(t) {\rm d}t$ est majorée donc F....donc...


!!! {{ exercice()}}

	Convergence de $ \displaystyle\int_1^{+\infty} \dfrac{1}{t^3+1} {\rm d}t$

#### Cas où $ f\underset{x \to b}{=} {\Large \scr o}\left(g\right)$


!!! {{theoreme("f est négligeable devant g")}}

    === "Énoncé"
		On suppose qu'au voisinage de $b$ on a $0 ≤ f(t) = {\Large \mathscr o} g(t)$. Alors

		$$
		\displaystyle\int_a^b    g(t)   {\rm    d}t    \text{    converge   }    ⟹
		\displaystyle\int_a^b f(t) {\rm d}t \text{ converge }
		$$

	
    === "Indication"
		On revient au cas précédent en remarquant que pour tout $ε>0$, il existe
		$A∈]a,b[$ tel  que pour  tout $t∈]A,b[$,  $f(t) <  εg(t)$. Il  suffit de
		prendre...
		
		
!!! {{exercice()}}

	Convergence de $ \displaystyle\int_1^{+\infty} \operatorname{e}^{-t^2} {\rm d}t$


#### Cas où $ f(x) \underset{x \to b}{\sim} g(x)$


!!! {{theoreme("f ~ g")}}

    === "Énoncé"
		On suppose qu'au voisinage de $b$ on a  $ f(x) \underset{x \to b}{\sim} g(x)$. Alors

		$$
		\displaystyle\int_a^b    g(t)   {\rm    d}t    \text{   et   } 
		\displaystyle\int_a^b f(t) {\rm d}t \text{ sont de même nature}
		$$
		
	=== "Indication"
		^^


!!! {{exercice()}}

	Convergence de $ \displaystyle\int_0^{1} \dfrac{\operatorname{e}^{-t}}{t} {\rm d}t$



### Hors programme (en 1ère année) : comparaison série / intégrale

Voici un théorème  hors programme mais qu'on retrouvera souvent  dans les épreuves
de concours. On ne pourra bien sûr pas l'utiliser directement cette année mais connaître la
démonstration sera bien utile pour inspirer la résolution de certains exercices. 

Il porte le nom de critère intégral de Cauchy bien que son papa
soit plutôt l'écossais McLaurin.

Il énonce que si  $f$ est continue, positive et décoirssante  sur $ℝ_+$ alors la
série $  \sum f(n)$ et  l'intégrale $\int_0^{+\infty}f(t){\rm d}t$ sont  de même
nature. 

Le nerf de la guerre est: 

$$
\displaystyle\int_k^{k+1} f(t) {\rm d}t ≤ f(k) ≤ \displaystyle\int_{k-1}^k f(t) {\rm d}t
$$

Faites un dessin et développer la démonstration.






### Règle du $t^αf(t)$


Voici   deux  théorèmes   qui  vous   permettront  de   vous  sortir   de  situations
embarrassantes...si vous les utilisez correctement.

!!! {{theoreme("Règle du $t^αf(t)$ en $+\infty$")}}

On suppose que $f$ est **positive** au voisinage de $+\infty$. 
- s'il  existe $α>1$  tel que $  \displaystyle\lim_{t\to+\infty}t^αf(t)=0$ alors
  $f$ est d'intégrale convergente au voisinage de $+\infty$.
- s'il  existe $α≤1$  tel que $  \displaystyle\lim_{t\to+\infty}t^αf(t)=+\infty$ alors
  $f$ est d'intégrale divergente au voisinage de $+\infty$.


À démontrer...

!!! {{exercice()}}

	Convergence de $ \displaystyle\int_1^{+\infty} \dfrac{\sin(t) {\rm e}^{-t}}{t^5} {\rm d}t$


!!! {{theoreme("Règle du $t^αf(t)$ en 0")}}

On suppose que $f$ est **positive** au voisinage de 0. 
- s'il  existe $α<1$  tel que $  \displaystyle\lim_{t\to0}t^αf(t)=0$ alors
  $f$ est d'intégrale convergente au voisinage de 0.
- s'il  existe $α≥1$  tel que $  \displaystyle\lim_{t\to0}t^αf(t)=+\infty$ alors
  $f$ est d'intégrale divergente au voisinage de 0.

À démontrer...



!!! {{exercice()}}

	Convergence de $ \displaystyle\int_0^1 t^n\ln(t) {\rm d}t$ pour tout $n∈ℕ$


### Convergence absolue

!!! {{ definition("Convergence absolue") }}
    Soit   $f$   une   fonction   continue   sur   $[a,b[$   avec   $a∈ℝ$,   $a∈
    \overrightarrow{ℝ}$.
	
	On dit que  $ \displaystyle\int_a^b f(t) {\rm d}t$  *converge absolument* ou
	est  *absolument convergente$  lorsque $  \displaystyle\int_a^b |f(t)|  {\rm
	d}t$ converge
	
	
L'intérêt de cette définition réside dans le théorème suivant:

!!! {{ theoreme("Convergence absolue implique convergence")}}

    === "Énoncé"

        $    \displaystyle\int_a^b     |f(t)|    {\rm    d}t$     converge    $⟹
        \displaystyle\int_a^b f(t) {\rm d}t$ converge.

	=== "Indications"

         $f(t) = |f(t)| - \bigl(|f(t)|-f(t)\bigr)$...


!!! {{exercice()}}

	Convergence de $ \displaystyle\int_0^{+\infty} \dfrac{\sin(t)}{t^{3/2}}  {\rm d}t$ 




## Plan d'attaque des intégrales impropres

- On commence par déﬁnir la fonction à intégrer. On précise soigneusement ses ensembles
de déﬁnition et de continuité.
- Identiﬁer la (resp. les) borne(s) en laquelle (resp. lesquelles) l’intégrale est impropre. Dans
le cas d’une intégrale impropre aux deux bornes, couper l’intégrale en deux et faire deux
études.
- Dans le cas d’une borne ﬁnie : à l’aide d’un calcul de limite, déterminer si la fonction se
prolonge par continuité (fausse impropreté).
- Si la fonction change de signe, considérer la valeur absolue de cette fonction.
- Chercher un équivalent simple.
- Si cela ne sufﬁt pas, tester la règle du $t^α f(t )$.
- Dans les cas plus complexes, modiﬁer l’intégrale à étudier à l’aide d’une intégration par
parties ou d’un changement de variables.
- Dans de rares cas, on sait déterminer une primitive de la fonction à intégrer, il sufﬁt alors
de calculer la limite des intégrales partielles .




!!! {{exercice("Intégrale de Dirichlet - épisode 1")}}

	Convergence de $ \displaystyle\int_0^{+\infty} \dfrac{\sin(t)}{t}  {\rm d}t$ 





<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.2.0/chart.js" integrity="sha512-opXrgVcTHsEVdBUZqTPlW9S8+99hNbaHmXtAdXXc61OUU6gOII5ku/PzZFqexHXc3hnK8IrJKHo+T7O4GRIJcw==" crossorigin="anonymous"></script>

<script src="../courbes.js"></script>

